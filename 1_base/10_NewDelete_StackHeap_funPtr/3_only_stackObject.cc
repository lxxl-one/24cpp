#include <iostream>
#include <string.h>

using std::cout;
using std::endl;


class Student{
public:
    Student(const char *name,int id)
    : _name(new char[strlen(name) + 1 ]())
    , _id(id)
    {
        cout<<"构造函数"<<endl;
        strcpy(_name,name);
    }
    
private:
    static void *operator new(size_t sz){
        cout<<"void *operator new(size_t)"<<endl;
        //用malloc申请未初始化空间
        void *pret=malloc(sz);

        return pret;
    }
    
    static void operator delete(void *ptr){
        cout<<"void operator delete(void * ptr)"<<endl;
        free(ptr);
    }

public:
    void print() const{
        if(_name){
            cout<<_name<<endl;
            cout<<_id<<endl;
        }
    }

    ~Student(){
        if(_name){
            delete []_name;
            _name=nullptr;
        }
        cout<<"已经释放"<<endl;
    }

private:
    char *_name;
    int _id;

};

void test(){
    
    //创建栈对象需要的条件：
    //构造函数、析构函数不能私有
    Student stu=Student("xiaowang",4200);
    stu.print();
    cout<<endl;

/*    Student *pstu=new Student("xiaohong",4201);
    pstu->print();
    cout<<endl;

    delete pstu;
    pstu=nullptr;
*/
}

int main()
{   
    test();
    return 0;
}

