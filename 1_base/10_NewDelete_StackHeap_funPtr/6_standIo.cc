#include <iostream>
#include <string>
#include <limits>

using std::cout;
using std::endl;
using std::cin;
using std::string;

void printStreamStatus(){

    cout<<cin.good()<<endl;
    cout<<cin.fail()<<endl;
    cout<<cin.eof()<<endl;
    cout<<cin.bad()<<endl;
}
void test(){
    
    int number =0;
    printStreamStatus();
    cin >> number;
    printStreamStatus();
    cout<<"number="<<number<<endl;

    cin.clear();//重置流的状态，但没有清空缓冲区,会自动赋给之后的cin
    //cin.ignore(1024,'\n');
    cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    //std::numeric_limits<std::streamsize>::max() 表示要忽略的最大字符数，
    //'\n' 表示遇到换行符时停止忽略。

    string s1;
    cin>>s1;
    cout<<"str = "<<s1<<endl;
}

void test2(){
    int number=0;
    
    //ctrl + d 退出
    while(cin>>number, !cin.eof()){//逗号表达式，最后一个逗号后面才是表达式
        if(cin.bad()){
            cout<<"stream is bad"<<endl;
        }
        else if(cin.fail()){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout<<"请输入整数"<<endl;
        }
        else{
            cout<<"number = "<<number<<endl;
        }
    }
}

int main()
{   
    test();
    return 0;
}

