#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;
using std::flush;
using std::ends;

//缓冲区刷新是指将缓冲区中的数据立即写入到其对应的目的地
void test(){
    for(size_t idx=0;idx<1024;++idx){
        cout<<'a';
    }
    
    operator<<(cout,'b').operator<<(endl);
    sleep(1);
    //operator<<(cout,'b');
    //cout.operator<<(endl);
    //cout<<'b'<<endl;        //刷新，换行
    cout<<"hello"<<flush;   //只刷新，不换行
    cout<<"world"<<ends;    //不刷新也不换行
    sleep(2);   //相等于上一行输入的world还停留在缓冲区里，等2秒程序结束后才打出来
}

int main()
{   
    test();
    return 0;
}

