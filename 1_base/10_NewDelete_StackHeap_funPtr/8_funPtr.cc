#include <iostream>

using std::cout;
using std::endl;

void func0(){
    cout<<"无参数的函数指针"<<endl;
}

int func1(int x){
    cout<<"int func1(int)"<<endl;
    return x;
}

int func2(int y){
    cout<<"int func2(int)"<<endl;
    return 2*y;
}

void func3(int (*pt)(int )){
   cout<<"调用有参数的函数指针"<<endl;
   cout<< pt(300)<<endl;
}

void func4(void (*pt)()){
   cout<<"调用无参数的函数指针"<<endl;
    pt();
}

void test(){
    int (*pf)(int);
    pf=&func1;
    cout<<pf(100)<<endl;
    
    cout<<endl;
    pf=func2;
    cout<<pf(100)<<endl;

    cout<<endl;
    func3(pf);
    
    cout<<endl;
    void (*pt)()=func0;
    func4(pt);
/*
__ostream_type &operator<<(__ostream_type& (*__pf)(__ostream_type&))
      {
         return __pf(*this);
      }


inline 
basic_ostream &endl(basic_ostream & __os)
    { 
       return flush(__os.put(__os.widen('\n'))); 
    }


inline 
basic_ostream &flush(basic_ostream & __os)
    { 
return __os.flush(); 
}
*/
}

int main()
{   
    test();
    return 0;
}

