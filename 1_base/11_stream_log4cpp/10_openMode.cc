#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;

void test(){

    //ate，打开时文件指针在末位
    ifstream ifs("wd_xie.txt", std::ios::in | std::ios::ate);
    if(!ifs){
        cout<<"异常"<<endl;
        return;
    }

    cout<<ifs.tellg()<<endl;
    ifs.seekg(0);

    string line;
    while(getline(ifs,line)){
        cout<<line<<endl;
    }

    cout<<ifs.tellg()<<endl;

    ifs.close();
}

void test2(){

    //app，打开时文件指针在末位
    ofstream ofs("wd_duxie.txt", std::ios::out | std::ios::app);
    if(!ofs){
        cout<<"异常"<<endl;
        return;
    }

    cout<<ofs.tellp()<<endl;
    ofs<<"hello"<<endl;
    ofs<<"world"<<endl;
    cout<<ofs.tellp()<<endl;


    ofs.close();
}

int main()
{   
    test();
    test2();
    return 0;
}

