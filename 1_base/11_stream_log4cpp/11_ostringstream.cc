#include <iostream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::ostringstream;

string func(int &value){
    ostringstream oss;
    oss<<value;
    return oss.str();
}

void test(){

    int number=10;
    string tmp =func(number);
    cout<<tmp<<endl;
    //oss是在内存，不在屏幕，所以屏幕上打不出来
}

int main()
{   
    test();
    return 0;
}

