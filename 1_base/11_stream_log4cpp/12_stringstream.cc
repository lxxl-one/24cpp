#include <iostream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;

void test(){

    int number=10;
    int number2=20;
    stringstream ss;

    //字符串的输出功能
    ss<<"number= "<<number<<" ,number2= "<<number2<<endl;
    cout<<ss.str();

    string key;
    int value;
    //字符串的输入功能
    //空格作为分隔符
    while(ss >> key >> value){
        cout<<key<<"---->"<<value<<endl;
    }
}

int main()
{   
    test();
    return 0;
}

