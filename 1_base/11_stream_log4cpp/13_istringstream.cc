#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::istringstream;

void readConf(const string &fileName){

    ifstream ifs(fileName);
    if(!ifs){
        cout<<"读文件异常"<<endl;
    }
    
    string line;
    istringstream iss;    //初始化串输入流，line作为参数，它在内存，读写快
    //while(ifs>>value>>line)   文件在磁盘，相当于要6次磁盘io
    while(getline(ifs,line)){   //只需要3次io
        iss.clear();
        iss.str(line);    //设置串输入流，line作为参数
        string key,value;
        iss>>key>>value;
        cout<<"key="<<key<<",value="<<value<<endl;
    }
    //更快的是一次读完全部文件，这样只用1次io

}

void test(){

    readConf("conf.data");

}

int main()
{   
    test();
    return 0;
}

