#include <iostream>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test(){

    //日志的格式
    BasicLayout *pbl=new BasicLayout();

    //日志的目的地
    OstreamAppender *poa = new OstreamAppender("OstreamAppender",&cout);
    poa->setLayout(pbl);

    //日志记录器
    Category &root=Category::getRoot();
    root.addAppender(poa);

    //日志过滤器
    root.setPriority(Priority::ERROR);

    root.emerg("this is an emerg message");
    root.fatal("this is an fatal message");
    root.alert("this is an alert message");
    root.crit("this is an crit message");
    root.error("this is an error message");
    root.notice("this is an notice message");
    root.debug("this is an debug message");

    Category::shutdown();
}

int main()
{   
    test();
    return 0;
}

