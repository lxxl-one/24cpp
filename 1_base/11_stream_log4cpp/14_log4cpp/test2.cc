#include <iostream>
#include <log4cpp/SimpleLayout.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test(){

    //日志的格式
    SimpleLayout *psl=new SimpleLayout();

    //日志的目的地
    FileAppender *pfl = new FileAppender("1","wd.txt");
    //"1" 是 FileAppender 对象的名称，用于在日志配置中标识该对象。
    //如果有多个 FileAppender 对象，可以使用不同的名称来区分它们。
    //"wd.txt"：表示将日志写入到名为 "wd.txt" 的文件中。
    //这里的txt是追加写
    pfl->setLayout(psl);

    //日志记录器
    Category &root=Category::getRoot();
    root.addAppender(pfl);

    //日志过滤器
    root.setPriority(Priority::ERROR);

    root.emerg("this is an emerg message");
    root.fatal("this is an fatal message");
    root.alert("this is an alert message");
    root.crit("this is an crit message");
    root.error("this is an error message");
    root.notice("this is an notice message");
    root.debug("this is an debug message");

    Category::shutdown();
}

int main()
{   
    test();
    return 0;
}

