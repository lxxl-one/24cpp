#include <iostream>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test(){

    //日志的格式
    PatternLayout *ppl1 = new PatternLayout();
    ppl1->setConversionPattern("%d [%p] %c %m%n");

    PatternLayout *ppl2 = new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c %m%n");


    //日志的目的地
    OstreamAppender *poa = new OstreamAppender("2", &cout);
    poa->setLayout(ppl1);

    FileAppender *pfl=new FileAppender("file","wd.txt");
    pfl->setLayout(ppl2);
    

    //日志记录器
    //Category &root=Category::getRoot();
    Category &root=Category::getRoot().getInstance("cat");
    //可以对比上面两行的区别，一个是根Category，另一个是子Category————"cat"
    root.addAppender(poa);
    root.addAppender(pfl);

    //日志过滤器
    root.setPriority(Priority::ERROR);

    root.emerg("this is an emerg message");
    root.fatal("this is an fatal message");
    root.alert("this is an alert message");
    root.crit("this is an crit message");
    root.error("this is an error message");
    root.notice("this is an notice message");
    root.debug("this is an debug message");
/*
 root.emerg("this is an emerg message") 表示记录一条紧急级别的日志消息，内容为 "this is an emerg message"。
 root.debug("this is an debug message") 则表示记录一条调试级别的日志消息，内容为 "this is an debug message"。
*/ 
    Category::shutdown();

}

int main()
{   
    test();
    return 0;
}

