#include <iostream>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;


void test(){

    //日志的格式
    PatternLayout *ppl1 = new PatternLayout();
    ppl1->setConversionPattern("%d [%p] %c %m%n");

    PatternLayout *ppl2 = new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c %m%n");

    //日志的目的地
    OstreamAppender *poa = new OstreamAppender("2", &cout);
    poa->setLayout(ppl1);

    RollingFileAppender *prfl=new RollingFileAppender("file","wd.txt", 5 * 1024, 3);
    prfl->setLayout(ppl2);
    //生成3个备份
    //先是wd，如果wd满了，则wd改名wd1，新的记录到新wd
    //如果新wd满了，则wd1变为wd2，新wd变为wd1，新新的记录到新新wd（wd总是最新的），依此类推
    //如果总大小超出了分块总和，则最老的直接抛弃
    //滚动文件
    

    //日志记录器
    Category &root=Category::getRoot().getInstance("cat");
    root.addAppender(poa);
    root.addAppender(prfl);

    //日志过滤器
    root.setPriority(Priority::ERROR);

    size_t idx=40;
    while(idx > 0){
        root.emerg("this is an emerg message");
        root.fatal("this is an fatal message");
        root.alert("this is an alert message");
        root.crit("this is an crit message");
        root.error("this is an error message");
        root.notice("this is an notice message");
        root.debug("this is an debug message");

        idx--;
    }
/*
 root.emerg("this is an emerg message") 表示记录一条紧急级别的日志消息，内容为 "this is an emerg message"。
 root.debug("this is an debug message") 则表示记录一条调试级别的日志消息，内容为 "this is an debug message"。
*/ 
    Category::shutdown();

}
int main()
{   
    test();
    return 0;
}

