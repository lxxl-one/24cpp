#include <iostream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;


void str_func(){
    stringstream ss;
    ss << "Hello!";
    ss << "Hello!";
    cout<<ss.str()<<endl;   // Hello!Hello!
    ss.str("New content"); // 设置新的字符串内容
    cout << ss.str()<<endl; // 输出：New content

    ss << "Hello, World!";
    string content = ss.str(); // 获取字符串内容
    cout << content << endl; // 输出：Hello, World!
    //cout<<ss.str()<<endl;   // 输出：Hello, World! 

    ss.str("content"); // 设置新的字符串内容
    cout<<ss.str()<<endl;   //content

    ss << "He";             //hentent
    cout<<ss.str()<<endl;   
    
    ss << "He";             //heheent
    cout<<ss.str()<<endl;

    //由此可见，当使用str(" ")后，ss会被置为其中内容，
    //之后再使用<<，会在其初始化内容上覆盖写
    
    //如果只是多次 <<，追加写
    //如果只是 .str()，则获取字符串内容

}

int main()
{   
    str_func();
    return 0;
}

