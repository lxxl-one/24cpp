#include <iostream>

using std::cout;
using std::endl;

class Point{
public:
    explicit Point(int ix=0, int iy=0)   //防止隐式转换
    : _ix(ix)
    , _iy(iy)
    {

    }

    ~Point(){

    }

    void print() const{
        cout<<_ix<<','<<_iy<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){

    //Point pt=10;    // 10--->Point(10,0)，隐式转换
    Point pt= Point(10,1);
    pt.print();
}

int main()
{   
    test();
    return 0;
}

