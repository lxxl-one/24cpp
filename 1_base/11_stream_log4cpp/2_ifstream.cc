#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test(){

    //对于文件输入流，当文件不存在时，就打开失败
    ifstream ifs("wd.txt");
    if(!ifs.good()){
        cerr<<"ifstream is not good"<<endl;
        return;
    }
    string word;

    //对于文件输入流，以空格为分隔
    while(ifs >> word){
        cout<<word<<endl;
    }

}

int main()
{   
    test();
    return 0;
}

