#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test(){

    //对于文件输入流，当文件不存在时，就打开失败
    ifstream ifs("wd.txt");
    if(!ifs.good()){
        cerr<<"ifstream is not good"<<endl;
        return;
    }

    string line[100];   //如果超过100行呢？
    size_t idx=0;
    //以行为基本单位读
    while(getline(ifs,line[idx])){
        //cout<<line[idx]<<endl;
        idx++;
    }
    
    //读指定的某一行
    cout<<line[36]<<endl;

    ifs.close();
}

int main()
{   
    test();
    return 0;
}

