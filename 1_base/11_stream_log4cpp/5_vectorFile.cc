#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;
using std::vector;

void test(){

    ifstream ifs("wd.txt");
    if(!ifs.good()){
        cerr<<"ifstream is not good"<<endl;
        return;
    }

    vector<string> vec;
    string line;   
    //以行为基本单位读
    while(getline(ifs,line)){
        vec.push_back(line);
    }

    for(size_t idx=0; idx!=vec.size(); ++idx){
        cout<<vec[idx]<<endl;
    }

    cout<<vec[36]<<endl;
    ifs.close();
}

int main()
{   
    test();
    return 0;
}

