#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test(){
    vector<int> number;
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;

    cout<<endl;
    number.push_back(1);
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;

    cout<<endl;
    number.push_back(2);
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;

    cout<<endl;
    number.push_back(3);
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;


    cout<<endl;
    number.push_back(1);    //元素可以重复
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;


    cout<<endl;
    number.push_back(20);
    cout<<number.size()<<endl;
    cout<<number.capacity()<<endl;

    //当size等于capacity时（即容器满），再有元素进来，扩容，size+1，capacity*2
    //原来的东西会一起放到新空间，老空间被释放掉
}

int main()
{   
    test();
    return 0;
}

