#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::ofstream;
using std::string;

void test(){

    //当文件不存在，则创建
    //存在时，则覆盖写
    ofstream ofs("test.txt");
    if(!ofs.good()){
        cerr<<"ofstream is not good"<<endl;
        return;
    }

}
void test2(){

    ifstream ifs("wd.txt");
    if(!ifs.good()){
        cerr<<"ifstream is not good"<<endl;
        return;
    }

    ofstream ofs("wd_xie.txt");
    if(!ofs.good()){
        cerr<<"ofstream is not good"<<endl;
        return;
    }

    string line;
    while(getline(ifs,line)){
        ofs<<line<<endl;
    }

    ifs.close();
    ofs.close();
}

int main()
{   
    test2();
    return 0;
}

