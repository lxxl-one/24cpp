#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::fstream;
using std::string;

void test(){

    //当文件不存在时，直接显示创建失败
    string fileName("wd_duxie.txt");
    fstream fs(fileName);
    if(!fs.good()){
        cout<<"fs is not good"<<endl;
        return;
    }

    string line;
    while(getline(fs,line)){
        cout<<line<<endl;
    }
}

int main()
{   
    test();
    return 0;
}

