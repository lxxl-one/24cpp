#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::fstream;
using std::string;

void test(){

    string fileName("duxie.txt");
    fstream fs(fileName);
    if(!fs.good()){
        cout<<"fs is not good"<<endl;
        return;
    }

    //可以直接读
/*    string line;
    while(getline(fs,line)){
        cout<<line<<endl;
    }
*/

    int number;
    for(int idx=0;idx<5;idx++){
        cin >> number;
        fs<<number<<" ";
    }
    //此时文件指针已经走到最后，一会输出时，只会重复输出最后一个数字
    //读取时，仍以空白格做为分隔
    

    //查看文件长度，tellp/tellg
    //p=put,g=get
    //文件指针，seekp/seekg
    size_t pos = fs.tellg();    
    cout<<pos<<endl;
    
    fs.seekp(0);    //文件流对象的偏移
    //fs.seekp(0, std::ios::beg);
    //fs.seekp(~pos, std::ios::end);
    for(int idx=0;idx<4;idx++){
        fs >> number;
        cout << number << " ";
    }
    fs.close();
}

int main()
{   
    test();
    return 0;
}

