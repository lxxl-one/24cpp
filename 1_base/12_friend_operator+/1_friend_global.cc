#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

class Point{

        //友元不受访问权限控制
        friend double distance(const Point &lsh, const Point &rhs);
public:

    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }


private:
    int _ix;
    int _iy;
};

//友元的第一种形式：全局函数(普通函数、自由函数)
double distance(const Point &lhs, const Point &rhs){
    return hypot(lhs._ix-rhs._ix, lhs._iy-rhs._iy);
}

void test(){
    Point pt1(1,2);
    Point pt2(4,6);
    cout<<distance(pt1,pt2)<<endl;
}

int main()
{   
    test();
    return 0;
}

