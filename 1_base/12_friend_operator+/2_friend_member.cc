#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

class Point;//类的前向声明
class Line{
public:
    double distance(const Point &lsh, const Point &rhs);
    double distance(const Point &lsh);

};


class Point{
public:

    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }

        friend double Line::distance(const Point &lsh, const Point &rhs);
        //friend double Line::distance(const Point &lsh);

private:
    int _ix;
    int _iy;
};


//友元的第二种形式：单独挑类中的某一个函数
double Line::distance(const Point &lhs, const Point &rhs){
    return hypot(lhs._ix-rhs._ix, lhs._iy-rhs._iy);
}
/*
重载函数是不同的，友元函数得分开设置
double Line::distance(const Point &lhs){
    return hypot(lhs._ix, lhs._iy);
}
*/

void test(){
    Point pt1(1,2);
    Point pt2(4,6);
    //cout<<Line::distance(pt1,pt2)<<endl;
    cout<<Line().distance(pt1,pt2)<<endl;
}

int main()
{   
    test();
    return 0;
}

