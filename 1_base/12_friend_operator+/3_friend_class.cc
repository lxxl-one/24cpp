#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

class Point;//类的前向声明
class Line{
public:
    double distance(const Point &lsh, const Point &rhs);
    void setPoint(Point &pt,int ix,int iy);
private:
    int _iz;
};


class Point{
public:

    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }

/*
        friend double Line::distance(const Point &lsh, const Point &rhs);
        friend void Line::setPoint(Point &pt,int ix,int iy);
*/
        //还是麻烦，可以把这个类里的所有函数都作为友元
        //第三种形式，友元类
        friend class Line;
        //友元是单向的，比如Point不能访问Line的_iz
        //没有传递性、继承性
        //破坏了封装性，尽量少用
        
private:
    int _ix;
    int _iy;
};


double Line::distance(const Point &lhs, const Point &rhs){
    return hypot(lhs._ix-rhs._ix, lhs._iy-rhs._iy);
}

void Line::setPoint(Point &pt,int ix,int iy){
    pt._ix=ix;
    pt._iy=iy;
}

void test(){
    Point pt1(1,2);
    Point pt2(4,6);

    Line().setPoint(pt2,10,20);//匿名对象
    //cout<<Line::distance(pt1,pt2)<<endl;
    cout<<Line().distance(pt1,pt2)<<endl;
}

int main()
{   
    test();
    return 0;
}

