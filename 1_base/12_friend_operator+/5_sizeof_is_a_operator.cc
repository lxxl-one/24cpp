#include <iostream>

using std::cout;
using std::endl;

void test(){

    int num=10;
    cout<<"sizeof(num) = "<<sizeof(num)<<endl;
    cout<<"sizeof num = "<<sizeof num<<endl;
    //说明sizeof是运算符，不是函数
}

int main()
{   
    test();
    return 0;
}

