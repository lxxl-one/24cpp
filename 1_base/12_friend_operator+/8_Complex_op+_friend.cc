#include <iostream>

using std::cout;
using std::endl;

//对于 + 的重载，推荐使用友元
//这样可以保证了两个参数(写在类里只有1个)，
//同时不用再写get函数才能访问private(写在类外就不能直接访问private)
class Complex{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"构造函数"<<endl;
    }

    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"析构函数"<<endl;

    }


    void print() const{
        cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
    }

private:
    double _dreal;
    double _dimag;
};

Complex operator+(const Complex &lhs, const Complex &rhs){
    return Complex(lhs._dreal+rhs._dreal,lhs._dimag+rhs._dimag);
}

/*
Complex operator+(const Complex &lhs, const Complex &rhs){
    Complex com(lhs.getReal()+rhs.getReal(),lhs.getImag()+rhs.getImag());
    return com;
}
*/

void test(){
    Complex c1(1,3);
    c1.print();

    Complex c2(2,6);
    c2.print();

    cout<<endl;
    Complex c3 = c1 + c2;
    c3.print();
}


int main()
{   
    test();
    return 0;
}

