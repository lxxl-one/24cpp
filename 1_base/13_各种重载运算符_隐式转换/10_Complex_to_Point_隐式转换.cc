#include <iostream>
#include <limits>

using std::cout;
using std::endl;
using std::ostream;
using std::cin;

//其他类型----->自定义类型，隐式转换
//这里Complex是其他类型，Point是自定义类型

class Complex{
    friend class Point;
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"Complex(double, double)"<<endl;
    }


    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"~Complex()"<<endl;

    }

        friend std::ostream &operator<<(std::ostream &os,const Complex &rhs);
        friend std::istream &operator>>(std::istream &is,Complex &rhs);
        //因为是往里面写，const删掉
    

    void print() const{
        if(_dreal==0 && 0==_dimag)
            cout<<0<<endl;
        else if (0==_dreal){
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
        else if (0 ==_dimag)
            cout<<_dreal<<endl;
        else{
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
    }

private:
    double _dreal;
    double _dimag;
};


std::ostream &operator<<(std::ostream &os,const Complex &rhs){
    cout<<"重载输出流函数"<<endl;
    if(rhs._dreal==0 && 0==rhs._dimag)
        os<<0<<endl;
    else if (0==rhs._dreal){
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }
    else if (0 ==rhs._dimag)
        os<<rhs._dreal<<endl;
    else{
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }

    return os;
}

void readDouble(std::istream &is, double &number){
    while(is >> number, !is.eof()){
        if(is.bad()){
            cout<<"stream is bad"<<endl;
            return;
        }
        else if(is.fail()){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout<<"pls input double data"<<endl;
        }
        else
            break;
    }
}

std::istream &operator>>(std::istream &is,Complex &rhs){
    cout<<"重载输入流函数"<<endl;
    readDouble(is,rhs._dreal);
    readDouble(is,rhs._dimag);
    //is >> rhs._dreal >> rhs._dimag;
    //这样如果输入hello，则会有错误，不够健全

    return is;
}

class Point{
public:
    Point(int ix=0,int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point(int, int)"<<endl;
    }

    Point(const Complex &rhs)
    : _ix(rhs._dreal)
    , _iy(rhs._dimag)
    {
        cout<<"Point(const Complex)"<<endl;
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);

    ~Point(){
        cout<<"~Point()"<<endl;
    }
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os,const Point &rhs){
    os<<"("<<rhs._ix<<','<<rhs._iy<<")";
    return os;
}

void test(){

    Point pt(1,2);
    cout<<"pt = "<< pt <<endl;
    cout<<endl;

    //从其他类型向自定义类型转换
    //隐式转换的本质，是先根据对应的重载的构造函数，创建临时对象
    //然后用临时对象给要创建的对象进行拷贝构造
    //用 -fno-elide-constructors 即可看到
    Point pt2 = 100;
    cout<<"pt2 = "<< pt2 <<endl;
    cout<<endl;

    //Complex转为Point，重载构造函数即可
    //即隐式转换都依赖构造函数
    Complex c2(2,3);
    //Point pt3 = c2;
    //相当于先 Point 临时对象(c2)
    //然后 Point pt3 = 临时对象
    
    Point pt3(c2);
    cout<<"pt3 = "<< pt3 <<endl;
    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

