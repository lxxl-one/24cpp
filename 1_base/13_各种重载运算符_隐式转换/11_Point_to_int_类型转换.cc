#include <iostream>
#include <limits>

using std::cout;
using std::endl;
using std::ostream;
using std::cin;

class Complex{
    friend class Point;
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"构造函数"<<endl;
    }


    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"析构函数"<<endl;

    }

        friend std::ostream &operator<<(std::ostream &os,const Complex &rhs);
        friend std::istream &operator>>(std::istream &is,Complex &rhs);
        //因为是往里面写，const删掉
    

    void print() const{
        if(_dreal==0 && 0==_dimag)
            cout<<0<<endl;
        else if (0==_dreal){
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
        else if (0 ==_dimag)
            cout<<_dreal<<endl;
        else{
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
    }

private:
    double _dreal;
    double _dimag;
};


std::ostream &operator<<(std::ostream &os,const Complex &rhs){
    cout<<"重载输出流函数"<<endl;
    if(rhs._dreal==0 && 0==rhs._dimag)
        os<<0<<endl;
    else if (0==rhs._dreal){
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }
    else if (0 ==rhs._dimag)
        os<<rhs._dreal<<endl;
    else{
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }

    return os;
}

void readDouble(std::istream &is, double &number){
    while(is >> number, !is.eof()){
        if(is.bad()){
            cout<<"stream is bad"<<endl;
            return;
        }
        else if(is.fail()){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout<<"pls input double data"<<endl;
        }
        else
            break;
    }
}

std::istream &operator>>(std::istream &is,Complex &rhs){
    cout<<"重载输入流函数"<<endl;
    readDouble(is,rhs._dreal);
    readDouble(is,rhs._dimag);
    //is >> rhs._dreal >> rhs._dimag;
    //这样如果输入hello，则会有错误，不够健全

    return is;
}

class Point{
public:
    Point(int ix=0,int iy=0)
    : _ix(ix)
    , _iy(iy)
    {

    }

    Point(const Complex &rhs)
    : _ix(rhs._dreal)
    , _iy(rhs._dimag)
    {

    }

    //自定义类型转换为其他类型：类型转换函数
    //没有参数、返回类型
    //必须是是成员函数
    //返回一个目标类型的变量
    operator int(){
        cout<<"int"<<endl;
        return _ix+_iy;
    }

    operator double(){
        cout<<"double"<<endl;
        if(_iy==0)
            return 0.0;
        else
            return static_cast<double>(_ix)/_iy;
    }

    operator Complex(){
        cout<<"Complex"<<endl;
        return Complex(_ix,_iy);
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);

    ~Point(){

    }
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os,const Point &rhs){
    os<<"("<<rhs._ix<<','<<rhs._iy<<")";
    return os;
}

void test(){

    Point pt(2,3);

    //自定义类型转换为其他类型
    int ix=pt;
    cout<<ix<<endl;

    cout<<endl;
    double dy=pt;
    cout<<dy<<endl;

    cout<<endl;
    Complex c3=pt;
    cout<<c3<<endl;
}

int main()
{   
    test();
    return 0;
}

