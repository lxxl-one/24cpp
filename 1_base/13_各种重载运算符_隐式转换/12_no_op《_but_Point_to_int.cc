#include <iostream>
#include <limits>

using std::cout;
using std::endl;
using std::ostream;


class Point{
public:
    Point(int ix=0,int iy=0)
    : _ix(ix)
    , _iy(iy)
    {

    }

    operator int(){
        cout<<"int"<<endl;
        return _ix+_iy;
    }


    ~Point(){

    }
private:
    int _ix;
    int _iy;
};


void test(){

    Point pt(2,3);
    cout<<pt<<endl;
    //只能存在一个，比如上面写了int，就不能再写double
    //有输出流运算符函数就走输出流，没有才走类型转换函数
    //比较少用
}

int main()
{   
    test();
    return 0;
}

