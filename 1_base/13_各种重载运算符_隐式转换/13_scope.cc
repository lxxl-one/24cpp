#include <iostream>

using std::cout;
using std::endl;

int number=10;

namespace wd
{
int number =20;

class Example{
public:
    Example(int value)
    : number(value)
    {

    }

    void print(int number){
        cout<<"形参number="<<number<<endl;
        cout<<"数据成员number="<<this->number<<endl;
        cout<<"数据成员number="<<Example::number<<endl;
        cout<<"命名空间number="<<wd::number<<endl;
        cout<<"全局变量number="<< ::number<<endl;
    }
private:
    int number;
};

}//end of namespace


void test(){
    int ix=4000;
    wd::Example ex(300);
    ex.print(ix);
}

int main()
{   
    test();
    return 0;
}

