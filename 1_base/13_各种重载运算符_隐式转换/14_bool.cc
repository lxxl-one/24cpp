#include <iostream>

using std::cout;
using std::endl;

class My{
public:
    My(int x)
    : _x(x)
    {

    }

    //explicit operator bool() const{
    operator bool() const{
        if(_x>0)
            return true;
        else
            return false;
        //return isValid();
        //或者只写这一句，然后在isValid里实现判断逻辑
    }

private:
    bool isValid() const{
        if(_x>0)
            return true;
        else
            return false;
    }

    int _x;
};

void test(){
    My obj(5);
    if(obj){
        cout<<"obj._x > 0"<<endl;
    }
    else
        cout<<"obj._x <= 0"<<endl;
}

int main()
{   
    test();
    return 0;
}

