#include <iostream>

using std::cout;
using std::endl;

class Complex{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"构造函数"<<endl;
    }

    //对象本身会发生变化，则写成成员函数形式
    //比如 赋值运算符函数
    Complex &operator+=(const Complex &rhs){
        _dreal += rhs._dreal;
        _dimag += rhs._dimag;

        return *this;
    }

    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"析构函数"<<endl;

    }


    void print() const{
        cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
    }

private:
    double _dreal;
    double _dimag;
};

Complex operator+(const Complex &lhs, const Complex &rhs){
    return Complex(lhs._dreal+rhs._dreal,lhs._dimag+rhs._dimag);
}


void test(){
    Complex c1(1,3);
    c1.print();

    Complex c2(2,6);
    c2.print();

    cout<<endl;
    Complex c3 = c1 + c2;
    c3.print();

    cout<<endl;
    c3 += c1;
    //c3.operator+=(c1);
    c3.print();
}


int main()
{   
    test();
    return 0;
}

