#include <iostream>

using std::cout;
using std::endl;

class Complex{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"构造函数"<<endl;
    }

    Complex &operator++(){
        ++_dreal;
        ++_dimag;

        return *this;
    }

    Complex operator++(int){   //int 只起标志作用，没有传参含义，表示后置++
        Complex tmp(*this);
        ++_dreal;
        ++_dimag;

        return tmp;     //tmp是局部对象，不能访问局部对象的引用，所以去掉&
    }

    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"析构函数"<<endl;

    }


    void print() const{
        cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
    }

private:
    double _dreal;
    double _dimag;
};

Complex operator+(const Complex &lhs, const Complex &rhs){
    return Complex(lhs._dreal+rhs._dreal,lhs._dimag+rhs._dimag);
}


void test(){
    Complex c1(1,3);
    c1.print();

    Complex c2(2,6);
    c2.print();

    cout<<endl;
    Complex c3 = c1 + c2;
    c3.print();

    cout<<endl;
    (++c3).print();
    c3.print();

    cout<<endl;
    (c3++).print();
    c3.print();

    // &(++c3)可以
    // &(c3++)不行
    // 前置++效率高，且返回的是左值
    // --同理
}


int main()
{   
    test();
    return 0;
}

