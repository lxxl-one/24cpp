#include <iostream>

using std::cout;
using std::endl;

class Complex{
public:
    Complex(double dreal=0.0,double dimag=0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout<<"构造函数"<<endl;
    }


    Complex(const Complex &rhs)
    : _dreal(rhs._dreal)
    , _dimag(rhs._dimag)
    {
        cout<<"拷贝构造"<<endl;
    }

    ~Complex(){
        cout<<"析构函数"<<endl;

    }

        //std::ostream operator<<(std::ostream os,const Complex &rhs);
        //首先不能改变参数个数，这里隐含了this指针，3>2
        //如果直接去掉rhs，则改变了参数顺序
        //所以要么静态、要么友元
        //友元采用全局函数的方式
        
        //对于流对象，已经删除了拷贝构造函数，
        //这里形参实参结合、返回流对象，都会调用拷贝构造函数
        //所以这里第一个参数、返回类型只能写成 &引用的形式
        friend std::ostream &operator<<(std::ostream &os,const Complex &rhs);
    

    void print() const{
        if(_dreal==0 && 0==_dimag)
            cout<<0<<endl;
        else if (0==_dreal){
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
        else if (0 ==_dimag)
            cout<<_dreal<<endl;
        else{
            if(_dimag<0)
                cout<<_dreal<<"+"<<'('<<_dimag<<')'<<"i"<<endl;
            else
                cout<<_dreal<<"+"<<_dimag<<"i"<<endl;
        }
    }

private:
    double _dreal;
    double _dimag;
};


std::ostream &operator<<(std::ostream &os,const Complex &rhs){
    if(rhs._dreal==0 && 0==rhs._dimag)
        os<<0<<endl;
    else if (0==rhs._dreal){
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }
    else if (0 ==rhs._dimag)
        os<<rhs._dreal<<endl;
    else{
        if(rhs._dimag<0)
            os<<rhs._dreal<<"+"<<'('<<rhs._dimag<<')'<<"i"<<endl;
        else
            os<<rhs._dreal<<"+"<<rhs._dimag<<"i"<<endl;
    }

    return os;
}


void test2(){

    Complex c(2,3);
    cout<<"c="<<c<<endl;

    operator<<(cout,"c=");
    //operator<<(cout,c);
    // operator<<(cout,endl); 
    // 自己写的重载的函数，要求rhs是Complex类，而endl是ostream的成员函数，不是一个东西

    cout.operator<<(endl);
    //cout.operator<<(endl);调用的是std::ostream类的成员函数operator<<，
    //而不是在66行重载的operator<<函数。
    //在C++中，endl是用于输出换行并刷新缓冲区的操纵符，
    //它是std::ostream类的一个成员函数，
    
    operator<<(operator<<(cout,"c="),c).operator<<(endl);

    cout<<endl;
    cout<<"c="<<c<<"hello"<<1<<c<<endl;
}

int main()
{   
    test2();
    return 0;
}

