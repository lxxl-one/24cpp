#include <iostream>

using std::cout;
using std::endl;

//如果一个类中重载了函数调用运算符，那么该类的对象就是函数对象
//因为这个对象可以像函数一样使用
class Func{
public:
    int operator()(int x, int y){
        return x+y;
    }

    int operator()(int x, int y,int z){
        return x*y*z;
    }
    //函数调用运算符只能写在类里，以成员函数形式重载
};

void test(){

    int a=3,b=4,c=5;
    Func fo;
    cout<<fo(a,b)<<endl;
    cout<<fo(a,b,c)<<endl;
    //fo.operator()(a,b)
}

int main()
{   
    test();
    return 0;
}

