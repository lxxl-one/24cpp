#include <iostream>

using std::cout;
using std::endl;

class Data{
public:
    Data(int data=0)
    : _data(data)
    {
        cout<<"构造1"<<endl;
    }


    int getData(){
        return _data;
    }

    ~Data(){
        cout<<"析构1"<<endl;
    }
    
private:
    int _data;
};

class Second{
public:
    Second(Data *pdata)
    : _pdata(pdata)
    {
        cout<<"构造2"<<endl;
    }

    Data *operator->(){
        return _pdata;
    }

    Data &operator*(){
        return *_pdata;
    }
    //如果不加&，则会多调用一次 Data的拷贝构造函数、析构函数

    ~Second(){
        cout<<"析构2"<<endl;
        if(_pdata){
            delete _pdata;
            _pdata=nullptr;
        }
    }
private:
    Data *_pdata;
};

class Third{
public:
    Third(Second *psl)
    : _psl(psl)
    {
        cout<<"构造3"<<endl;
    }

    Second &operator->(){
        return *_psl;
    }

    Second &operator*(){
        return *_psl;
    }

    ~Third(){
        cout<<"析构3"<<endl;
        if(_psl){
            delete _psl;
            _psl=nullptr;
        }
    }

private:
    Second *_psl;
};

void test(){
    Second s2(new Data(10));
    cout<<(*s2).getData()<<endl;
    // s2.operator*().getdata();
}

void test2(){
    Third s3(new Second(new Data(100)));
    cout<<(*s3)->getData()<<endl;

    cout<<(*s3).operator*().getData()<<endl;
    //同样无法跨层
}

int main()
{   
    test2();
    return 0;
}

