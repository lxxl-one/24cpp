#include <iostream>

using std::cout;
using std::endl;


class Line{

//内部类，一般只对内部可见
private:
    class Point{
    public:
        Point(int ix,int iy)
        : _ix(ix)
        , _iy(iy)
        {
            cout<<"构造点"<<endl;
        }
    private:
        int _ix;
        int _iy;
    };
public:
    Line(int x1, int y1,int x2, int y2)
    :_pt1(x1,y1)
    ,_pt2(x2,y2)
    {

    }
private:
    Point _pt1;
    Point _pt2;
};

void test(){

}

int main()
{   
    test();
    return 0;
}

