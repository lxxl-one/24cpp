#include <iostream>

using std::cout;
using std::endl;

//将destory放到另一个友元类里，创建一个友元类的栈对象
class Singleton{
    friend class AutoRelease;
public:
    static Singleton *getInstance(){
        if(_pInstance==nullptr)
            _pInstance=new Singleton();
        return _pInstance;
    }

private:
    Singleton(){
        cout<<"单例模式"<<endl;
    }
    ~Singleton(){
        cout<<"手动释放"<<endl;
    }
    static Singleton *_pInstance;
};

Singleton *Singleton::_pInstance=nullptr;

class AutoRelease{
public:
    AutoRelease(){

    }

    ~AutoRelease(){
        if(Singleton::_pInstance){
            delete Singleton::_pInstance;
            Singleton::_pInstance=nullptr;
        }
        else
            cout<<"不要释放两次"<<endl;
    }
};
void test(){
    Singleton *s1= Singleton::getInstance();

    AutoRelease ar;//创建一个栈对象
}

int main()
{   
    test();
    return 0;
}

