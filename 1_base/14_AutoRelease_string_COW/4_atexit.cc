#include <iostream>
#include <stdlib.h>

using std::cout;
using std::endl;

void func(){
    cout<<"func"<<endl;
}
void test(){

    //对于注册的func，会在进程正常结束之后被调用
    //而不是调用时被调用
    atexit(func);
    atexit(func);
    atexit(func);
    atexit(func);
}

int main()
{   
    cout<<1<<endl;
    test();
    cout<<2<<endl;
    return 0;
}

