#include <iostream>
#include <stdlib.h>

using std::cout;
using std::endl;

//在创建时就布置了destory，饿汉模式
class Singleton{
public:
    static Singleton *getInstance(){
        if(_pInstance==nullptr){
            _pInstance=new Singleton();
            atexit(destory);
        }
        return _pInstance;
    }

    static void destory(){
        if(_pInstance){
            delete _pInstance;
            _pInstance=nullptr;
        }
    }
private:
    Singleton(){
        cout<<"单例模式"<<endl;
        
    }
    ~Singleton(){
        cout<<"手动释放"<<endl;
    }
    static Singleton *_pInstance;
};

//Singleton *Singleton::_pInstance=nullptr;
//可能会在多线程环境下出现竞态条件，
//因为在多个线程同时调用getInstance时，可能会导致多次初始化实例。

//饿汉模式
Singleton *Singleton::_pInstance=getInstance();
//在程序启动时就会创建_pInstance实例，之后在调用getInstance时不会再进行初始化。
//这样可以保证在多线程环境下不会出现竞态条件，
//因为初始化是在程序启动时进行的，并且只会进行一次。


void test(){
    Singleton *s1= Singleton::getInstance();

}

int main()
{   
    test();
    return 0;
}

