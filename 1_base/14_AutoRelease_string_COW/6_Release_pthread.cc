#include <iostream>
#include <stdlib.h>
#include <pthread.h>

using std::cout;
using std::endl;

//第四种方法
//pthread + atexit
//once是唯一的参数，此时传入的init只会被执行一次
//pthread_once接受两个参数，
//第一个参数是指向pthread_once_t类型的指针，
//第二个参数是一个函数指针，用于指定在第一次调用时需要执行的函数。
//pthread_once会保证在多线程环境下，只有一个线程会调用指定的函数，而其他线程会等待该函数执行完成。
//这就确保了指定的函数只会被执行一次。
//哪怕进来100个线程，申请空间也只执行一次
//但pthread只能在linux下面使用
//g++ 6 -lpthread
class Singleton{
public:
    static Singleton *getInstance(){
        pthread_once(&_once,init);
        return _pInstance;
    }

    static void init(){
        _pInstance=new Singleton();
        atexit(destory);
    }

    static void destory(){
        if(_pInstance){
            delete _pInstance;
            _pInstance=nullptr;
        }
    }
private:
    Singleton(){
        cout<<"单例模式"<<endl;
        
    }
    ~Singleton(){
        cout<<"手动释放"<<endl;
    }
    static Singleton *_pInstance;
    static pthread_once_t _once;
};

//饿汉模式
Singleton *Singleton::_pInstance=getInstance();

pthread_once_t Singleton::_once=PTHREAD_ONCE_INIT;

void test(){
    Singleton *s1= Singleton::getInstance();

}

int main()
{   
    test();
    return 0;
}

