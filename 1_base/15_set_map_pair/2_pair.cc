#include <iostream>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::pair;
using std::string;

void test(){
    //pair<int, string> pa(1,"hello");
    pair<int, string> pa = {1,"hello"};
    cout<<pa.first<<"  "<<pa.second<<endl;
    //pair 将两个值组合成一个单元，它的设计就是专门用于存储两个值的。
    //只有两个元素
    //可以直接获取第一个、第二个元素

}

int main()
{   
    test();
    return 0;
}

