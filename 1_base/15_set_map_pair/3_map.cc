#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::make_pair;

void test(){
    //1、存放的是pair类型
    //2、key值唯一，value随意
    //3、默认情况下对key进行升序排序
    //4、可以使用count、find、insert
    //5、可以通过下标访问，查找插入修改，number[key]，
    //   如果key存在，返回value。如果不存在，返回空。且将key与空插入map
    map<int, string> number= {
        pair<int,string>(1,"hello"),
        pair<int,string>(2,"world"),
        pair<int,string>(2,"world"),
        //把小括号换成大括号
        {6,"wuhan"},
        {7,"beijing"},
        make_pair(4,"hello"),
        make_pair(10,"world"),
        make_pair(6,"world")        //不会覆盖，还是wuhan
        //返回的是pair类型
    };

    //map<int,string>::iterator it;
    for(auto it=number.begin();it!=number.end();++it){
        cout<<it->first<<"  "<<it->second<<endl;
    }

    //count
    cout<<endl;
    cout<<number.count(5)<<endl;
    cout<<number.count(6)<<endl;

    //find
    cout<<endl;
    map<int,string>::iterator it=number.find(7);
    if(it == number.end()){
        cout<<"不在map"<<endl;
    }
    else
        cout<<"在map"<<"  "<<it->first<<"  "<<it->second<<endl;

    //insert
    cout<<endl;
    //auto ret = 
    pair<map<int,string>::iterator,bool>ret = 
        number.insert(pair<int,string>(3,"wangdao"));
        //number.insert(make_pair(3,"wangdao"));
        //number.insert({3,"wangdao"});
    if(ret.second){
        cout<<"插入成功"<<"  ";
        cout<<ret.first->first<<"  "<<ret.first->second<<endl;
    }
    else
        cout<<"失败，因为key值已存在"<<endl;

    //map<int,string>::iterator it;
    cout<<endl;
    for(auto it=number.begin();it!=number.end();++it){
        cout<<it->first<<"  "<<it->second<<endl;
    }

    //下标访问，能查找、插入、修改，遥遥领先
    cout<<endl<<endl;
    cout<<"number[7]="<<number[7]<<endl;
    number[5]="nanning";
    number[1]="hELLO";
    
    cout<<endl;
    for(auto it=number.begin();it!=number.end();++it){
        cout<<it->first<<"  "<<it->second<<endl;
        }
}

int main()
{   
    test();
    return 0;
}

