#include <iostream>

using std::cout;
using std::endl;

class A{
public:
    A(long ix=0) : _ix(ix) {
        cout<<"A(ix=0)"<<endl;
    }

    void print(){
        cout<<_ix<<endl;
    }

    void setx(int ix){
        _ix=ix;
    }

    ~A(){
        cout<<"~A()"<<endl;
    }

public:
    long _ix;
};


class B
: virtual public A
{
public:
    B(){
        cout<<"B()"<<endl;
    }
    ~B(){
        cout<<"~B()"<<endl;
    }
};

class C
: virtual public A
{
public:
    C(){
        cout<<"C()"<<endl;
    }
    ~C(){
        cout<<"~C()"<<endl;
    }
};

class D
: public B  
, public C  
{
public:
    D(){
        cout<<"D()"<<endl;
    }
    ~D(){
        cout<<"~D()"<<endl;
    }

};

void test(){
    cout<<sizeof(A)<<endl;//8
    cout<<sizeof(B)<<endl;//16=8+指针1
    cout<<sizeof(C)<<endl;//16=8+指针2
    cout<<sizeof(D)<<endl;//24=8+指针1+指针2

    cout<<endl;
    D d;

    cout<<endl;
    d.B::setx(10);
    d.B::print();

    cout<<endl;
    d.C::setx(5);
    d.C::print();
    d.B::print();

    cout<<endl;
    d.setx(100);
    d.print();
    d.B::print();

    cout<<endl;
    cout<<&d._ix<<endl;
    cout<<&(d.A::_ix)<<endl;    //d是怎么知道有A的？
    cout<<&(d.B::_ix)<<endl;
    cout<<&(d.C::_ix)<<endl;
    cout<<d._ix<<endl;
    cout<<(d.A::_ix)<<endl;    //d是怎么知道有A的？
    cout<<(d.B::_ix)<<endl;
    cout<<(d.C::_ix)<<endl;

}

int main()
{   
    test();
    return 0;
}

