#include <iostream>

using std::cout;
using std::endl;

/*
 为了让拷贝和赋值报错，有以下方法：

 1、把他俩设为private
 2、参考流，删去这两个函数
    Example(const Example &rhs)=delete;
    Example &operator=(const Example &rhs)=delete;
 3、今天学到的，如果派生类没有拷贝、赋值，那么会自动调用基类的拷贝、赋值
    那么创建一个Not类，删去它的这两个函数，或者把Not的这两个函数也设为private
    然后让Example继承它，并在Example中不写拷贝赋值函数，那么就不能实现拷贝赋值
*/

class Example{
public:
    Example(){
        cout<<"Example()"<<endl;
    }
    Example(const Example &rhs){
        cout<<"Example(const Example &)"<<endl;
    }
    Example &operator=(const Example &rhs){
        cout<<"Example &operator=(const Example &)"<<endl;
        return *this;
    }
    ~Example(){
        cout<<"~Example()"<<endl;
    }
};

void test(){
    Example ex1;
    Example ex2=ex1;
    Example ex3;
    ex3 = ex1;

}

int main()
{   
    test();
    return 0;
}

