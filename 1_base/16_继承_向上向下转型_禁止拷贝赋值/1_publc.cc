#include <iostream>

using std::cout;
using std::endl;

//public下的
//数据成员可以全部继承，但成员函数不一定
//子类不能继承父类重载的new、delete
//因为数据成员不同，父类new出来的空间大小与子类不同，放不下

//父类的protected只留给子类和孙子类，且依然是protected（即子类类体里面可以访问）
//protected + public/protected = protected，无论经过几次继承
//protected + private = private，只要有一次私有继承，则所有成员都变成private
//子类的对象、父类的对象都不可以访问
//对于子类对象而言，只能访问公有继承的公有成员
class Point{
public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point构造"<<endl;
    }
    ~Point(){
        cout<<"Point析构"<<endl;
    }
    
    void print(){
        cout<<"_ix="<<_ix<<","<<"_iy="<<_iy<<endl;
    }

    int gety(){
        return _iy;
    }

protected:  //留给儿子访问的
    int _ix;

private:    //只能在该类的内部进行访问
    int _iy;
};

class Point3d 
: public Point  //派生类列表
{

public:
    Point3d(int ix=0,int iy=0, int iz=0)
    : Point(ix,iy)  //不能自己初始化，要继承
    , _iz(iz)
    {
        cout<<"Point3d构造"<<endl;
    }
    ~Point3d(){
        cout<<"Point3d析构"<<endl;
    }

    void print(){
        cout<<"_ix="<<_ix<<endl;     //protected
        cout<<"_iy不能访问"<<endl;   //不能访问
        cout<<"_iy="<<gety()<<endl;  //public
        cout<<"_iz="<<_iz<<endl;
    }

private:
    int _iz;
};


void test(){
    Point a(1,2);
    Point3d b(3,4,5);

    //cout<<a._ix<<endl;

    a.print();
    cout<<endl;
    b.print();
    
    cout<<endl;
    cout<<sizeof(Point)<<endl;
    cout<<sizeof(Point3d)<<endl;
}

int main()
{   
    test();
    return 0;
}

