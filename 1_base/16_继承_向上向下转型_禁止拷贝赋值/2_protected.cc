#include <iostream>

using std::cout;
using std::endl;

//保护继承的类，其中的非私有成员，可以在后续中无限地保护继承下去，
//注意保护继承下已经没有了public成员，所以后续的成员要么是protected，要么是private
//默认继承方式是私有继承
class Point{
public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point构造"<<endl;
    }
    ~Point(){
        cout<<"Point析构"<<endl;
    }
    
    void print(){
        cout<<"_ix="<<_ix<<","<<"_iy="<<_iy<<endl;
    }

    int gety(){
        return _iy;
    }

protected:  //留给子孙访问的，而且这些子孙是公有继承或保护继承
    int _ix;

private:    //只能在类的内部进行访问
    int _iy;
};

class Point3d 
: protected Point  //派生类列表
{

public:
    Point3d(int ix=0,int iy=0, int iz=0)
    : Point(ix,iy)  //不能自己初始化，要继承
    , _iz(iz)
    {
        cout<<"Point3d构造"<<endl;
    }
    ~Point3d(){
        cout<<"Point3d析构"<<endl;
    }

    void print(){
        cout<<"_ix="<<_ix<<endl;    //protected
        cout<<"_iy不能访问"<<endl;  //不能访问
        cout<<"_iy="<<gety()<<endl; //protected
        cout<<"_iz="<<_iz<<endl;
    }

private:
    int _iz;
};


void test(){
    Point a(1,2);
    Point3d b(3,4,5);

    cout<<endl;
    a.print();
    cout<<endl;
    b.print();
    
    cout<<endl;
    cout<<sizeof(Point)<<endl;
    cout<<sizeof(Point3d)<<endl;
}

int main()
{   
    test();
    return 0;
}

