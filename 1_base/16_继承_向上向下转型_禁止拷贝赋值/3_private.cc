#include <iostream>

using std::cout;
using std::endl;

//私有继承的类，其中的所有成员，都不能在后续中被访问了，相当于只爽这一次
//默认继承方式是私有继承
class Point{
public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point构造"<<endl;
    }
    ~Point(){
        cout<<"Point析构"<<endl;
    }
    
    void print(){
        cout<<"_ix="<<_ix<<","<<"_iy="<<_iy<<endl;
    }

    int gety(){
        return _iy;
    }

protected:  
    int _ix;

private:    //只能在类的内部进行访问
    int _iy;
};

class Point3d 
: private Point  //派生类列表
{

public:
    Point3d(int ix=0,int iy=0, int iz=0)
    : Point(ix,iy)  //不能自己初始化，要继承
    , _iz(iz)
    {
        cout<<"Point3d构造"<<endl;
    }
    ~Point3d(){
        cout<<"Point3d析构"<<endl;
    }

    void print(){
        cout<<"_ix="<<_ix<<endl;    //private
        cout<<"_iy不能访问"<<endl;  //不能访问
        cout<<"_iy="<<gety()<<endl; //private
        cout<<"_iz="<<_iz<<endl;
    }

private:
    int _iz;
};

class Point4D
: public Point3d
{
    void print4d(){
        //cout<<_ix<<endl;  都已经在3d中变成了private了
        //cout<<_iy<<endl;  不能访问
    }

};

void test(){
    Point a(1,2);
    Point3d b(3,4,5);

    a.print();
    cout<<endl;
    b.print();
    
    cout<<endl;
    cout<<sizeof(Point)<<endl;
    cout<<sizeof(Point3d)<<endl;
}

int main()
{   
    test();
    return 0;
}

