#include <iostream>

using std::cout;
using std::endl;

class Base{
public:
    Base()
    {
        cout<<"Base()"<<endl;
    }
    ~Base()
    {
        cout<<"~Base()"<<endl;
    }
};

class Deriver : public Base{

public:
    Deriver(long deriver)
    //不管基类有没有写，这里都会"调用""基类的构造函数，准确来说是 借用
    : _deriver(deriver)         
    {
        cout<<"Deriver(long)"<<endl;
    }

private:
    long _deriver;
};

void test(){
    Deriver deriver(10);
}

int main()
{   
    test();
    return 0;
}

