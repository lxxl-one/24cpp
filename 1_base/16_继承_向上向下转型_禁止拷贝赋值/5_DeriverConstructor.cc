#include <iostream>

using std::cout;
using std::endl;

class Base{
public:
/*
    Base(){
        cout<<"Base()"<<endl;
    }
*/
    //Base(long base = 0)
    Base(long base)
    : _base(base)
    {
        cout<<"Base(long )"<<endl;
    }

private:
    long _base;
};

class Deriver 
: public Base
{
public:
    Deriver()
    {
        cout<<"如果Base有带参数的构造函数，但没有默认构造函数，"
            "那么在Derived的构造函数中仍然需要通过初始化列表显式地调用Base的构造函数，"
            "并传递相应的参数。"
            "但如果该有参构造函数的参数有默认值，则不需要显式写出"<<endl;
        cout<<"Deriver()"<<endl;
    }

};

void test(){
    //Deriver deriver(10); 
    //报错，因为自己的构造函数里没有写任何参数，不知道传给谁
    //哪怕基类有 有参构造函数
    Deriver der2;
}

int main()
{   
    test();
    return 0;
}

