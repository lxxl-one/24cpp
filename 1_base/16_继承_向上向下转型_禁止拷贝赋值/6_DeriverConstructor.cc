#include <iostream>

using std::cout;
using std::endl;

class Base{
public:
    Base(){
        cout<<"基类写出了无参构造函数"<<endl;
    }
    Base(long base)
    : _base(base)
    {
        cout<<"Base(long)"<<endl;
    }
private:
    long _base;
};

class Deriver
: public Base
{
public:

};

void test(){
    Deriver der2;   //如果基类没有写出无参构造函数，则报错
}

int main()
{   
    test();
    return 0;
}

