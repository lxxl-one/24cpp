#include <iostream>

using std::cout;
using std::endl;

//初始化顺序：      基类-3member-派生类？
//构造函数调用顺序：基类-对象类型的数据成员-派生类
//析构函数调用顺序：正好相反
class Tst{
public:
    Tst(){
        cout<<"Tst()"<<endl;
    }

    Tst(long ix)
    : _ix(ix)
    {
        cout<<"Tst(long )"<<endl;
    }

    ~Tst(){
        cout<<"~Tst()"<<endl;
    }

    long _ix;
};

class Base{
public:
    Base(){
        cout<<"Base()"<<endl;
    }

    Base(long base)
    : _base(base)
    {
        cout<<"Base(long)"<<endl;
    }

    ~Base(){
        cout<<"~Base()"<<endl;
    }
private:
    long _base;
};

class Deriver
: public Base
{
public:
    Deriver(long deriver)
    : Base(deriver)     //基类有参无参都有，如果这里调用有参，必须显式写出来
    , _deriver(deriver)
    , _tst(deriver)
    {
        
        //Tst _tst(deriver); //不能在这里才初始化
        cout<<"Deriver(long)"<<endl;
    }

    ~Deriver(){
        cout<<"~Deriver()"<<endl;
    }

    long _deriver;
    Tst _tst;   //对象类型的数据成员，
                //如果有那3类数据成员（引用、const、类对象），
                //则必须在初始化列表中完成初始化
};

void test(){
    Deriver der2(10);
    
    cout<<endl;
    cout<<der2._tst._ix<<endl;
}

int main()
{   
    test();
    return 0;
}

