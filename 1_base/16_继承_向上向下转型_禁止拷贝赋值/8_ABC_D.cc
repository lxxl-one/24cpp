#include <iostream>
#include <stdio.h>

using std::cout;
using std::endl;

class A{
public:
    A(int ix=0): _ix(ix){
        cout<<"A(int ix)"<<endl;
    }
    ~A(){
        cout<<"~A()"<<endl;
    }
    void address(){
        cout<<&(_ix)<<endl;
    }
    void printa(){
        cout<<"A::_ix="<<_ix<<endl;
    }

public:
    int _ix;
};


class B{
public:
    B(){
        cout<<"B()"<<endl;
    }
    ~B(){
        cout<<"~B()"<<endl;
    }
    void printb(){
        cout<<"b"<<endl;
    }
};

class C{
public:
    C(){
        cout<<"C()"<<endl;
    }
    ~C(){
        cout<<"~C()"<<endl;
    }
    void printc(){
        cout<<"c"<<endl;
    }
};

class D
: public A
, public B  //派生类的构造函数顺序与继承顺序有关
, public C  //继承方式不共用，不写就默认private
{
public:
    D(int x)
    : A(3)
    , _ix(x)
    {
        cout<<"D()"<<endl;
    }
    ~D(){

    }

    void setd(int ix){
        _ix=ix;
    }

public:
    int _ix;
};

void test(){
    A a;
    D d(6);


/*
多继承下，成员函数的访问冲突
使用作用域限定符
    d.A::print();
    d.B::print();
    d.C::print();
*/
    cout<<endl;
    cout<<d._ix<<endl;
    d.printa(); //A::_ix=3
    cout<<d.A::_ix<<endl; //3

    cout<<endl;
    d.setd(5);
    d.printa(); //A::_ix=3
    cout<<d.A::_ix<<endl;//3
    cout<<d._ix<<endl;//5

    cout<<endl;
    cout<<a._ix<<endl;  //0
    a.address();    //3c
    printf("%p\n",&(a._ix));    //3c
    d.address(); //40
    //说明d中确实有一个A::_ix，
    //void address(){
      //  cout<<&(_ix)<<endl;
    //}
    //address是从A继承来的，没有重写，只能看见A范围，寻找的仍是A::_ix，而不是d._ix
    //如果D中重写了address，还可以找到A的address：d.A::address()
    printf("%p\n",&(d.A::_ix)); //40
    printf("%p\n",&(d._ix));    //44

}

int main()
{   
    test();
    return 0;
}

