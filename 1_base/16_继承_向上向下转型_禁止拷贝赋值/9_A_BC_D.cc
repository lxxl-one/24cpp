#include <iostream>
#include <stdio.h>

using std::cout;
using std::endl;

//数据成员可以重复，成员函数可以重载
class A{
public:
    A(long ix=0) : _ix(ix) {
        cout<<"A(ix=0)"<<endl;
    }

    void print(){
        cout<<"a._ix="<<_ix<<endl;
    }

    void setx(int ix){
        _ix=ix;
    }

    ~A(){
        cout<<"~A()"<<endl;
    }

private:
    long _ix;
};


class B
: public A
{
public:
    B(){
        cout<<"B()"<<endl;
    }
    ~B(){
        cout<<"~B()"<<endl;
    }
};

class C
: public A
{
public:
    C()
    : _ix(1)
    {
        cout<<"C()"<<endl;
    }
    ~C(){
        cout<<"~C()"<<endl;
    }

    void setx(int ix){
        _ix=ix;
    }

    int _ix;
};

class D
: public B  
, public C  
{
public:
    D(){
        cout<<"D()"<<endl;
    }
    ~D(){
        cout<<"~D()"<<endl;
    }

    void print(){
        cout<<"我是d自己的print"<<endl;
    }

    int _ix;
    int _iy;

};

void test(){
    cout<<sizeof(A)<<endl;  //8
    cout<<sizeof(B)<<endl;  //8
    cout<<sizeof(C)<<endl;  //8+4=16
    cout<<sizeof(D)<<endl;  //8+16+4+4=32

    cout<<endl;
    D d;

    cout<<endl;
    d.B::setx(10);
    d.B::print();   //  10

    cout<<endl;
    cout<<d.C::_ix<<endl;   //1
    d.C::setx(5);   
    d.C::print();   //0，c的print放在a中，print打印的是此时a中的ix，为0
    cout<<d.C::_ix<<endl;   //5，这时c自己的ix，已被c自己的setx修改为5
    d.B::print();   //10,b里面的ix不受c影响

    cout<<endl;
    d.B::setx(100);
    d.C::print();   //0，同上，不关我事
    d.B::print();   //100
    d.C::setx(1000);//c自己的
    cout<<d.C::_ix<<endl;   //1000，这时c自己的ix，已被c自己的setx修改为1000

    cout<<endl;
    cout<<d._ix<<endl;
    cout<<d._iy<<endl;
    d.print();

    cout<<endl;
    printf("%p\n",&(d.C::_ix));
    printf("%p\n",&(d._ix));

}

int main()
{   
    test();
    return 0;
}

