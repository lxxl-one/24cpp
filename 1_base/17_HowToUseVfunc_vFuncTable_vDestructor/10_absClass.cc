#include <iostream>

using std::cout;
using std::endl;

//抽象类的第二种写法
//构造函数是protected的，也是抽象类，但deriver可以创建对象
//如果是private，则都不行

class Base{
protected:
    Base()
    {
        cout<<"Base()"<<endl;
    }
};

class Deriver
: public Base
{
public:
    Deriver()
    : Base()
    {
        cout<<"Deriver()"<<endl;
    }
};

void test(){
    //Base base;    报错
    Deriver deriver;
}

int main()
{   
    test();
    return 0;
}

