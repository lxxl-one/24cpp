#include <iostream>

using std::cout;
using std::endl;

class Base{
public:
    Base(long number =0)
    : _number(number)
    {

    }

    void print(){
        cout<<"Base::print()"<<endl;
        cout<<"Base::_number = "<<_number<<endl;
    }

    ~Base(){

    }

protected:
    long _number;
};

class Deriver
: public Base
{
public:
    Deriver(long number=0,long number2=0)
    : Base(number)
    , _number(number2)
    {

    }

    ~Deriver(){

    }

    void print(int x){
        cout<<"x="<<x<<endl;
        cout<<"void Deriver::print(int x)"<<endl;
        cout<<"Base::_number="<<Base::_number<<endl;
        cout<<"_number="<<_number<<endl;
    }

protected:
    long _number;
};

void test(){

    Deriver d(111,222);
    d.print(333);
    //因为自己的print（int x）把基类继承过来的print()隐藏了
    //就算把print()写成virtual，依然错。因为它们不形成重写
    
    cout<<endl;
    d.Base::print();    //只是隐藏了，不是删除
}

int main()
{   
    test();
    return 0;
}

