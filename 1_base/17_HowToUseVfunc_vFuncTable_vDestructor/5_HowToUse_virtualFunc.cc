#include <iostream>

using std::cout;
using std::endl;

//访问虚函数，不等同于体现动态多态
//1、指针
//2、引用
//3、对象直接调用，不行

class Base{
public:
    Base(long base=0)
    : _base(base)
    {
        cout<<"Base()"<<endl;
    }
    ~Base(){
        cout<<"~Base()"<<endl;
    }

    virtual
    void print(){
        cout<<Base::_base<<endl;
    }

private:
    long _base;
};

class Deriver
: public Base
{
public:
    void print(){
        cout<<_deriver<<endl;
    }
    Deriver(long base=0,long deriver=0)
    : Base(base)
    , _deriver(deriver)
    {
        cout<<"Deriver()"<<endl;
    }
    ~Deriver(){
        cout<<"~Deriver()"<<endl;
    }

private:
    long _deriver;
};

void func1(Base *base){
    base->print();      //通过指针调用
}
void func2(Base &base){
    base.print();      //通过引用调用
}

void test(){
    cout<<sizeof(Base)<<endl;
    cout<<sizeof(Deriver)<<endl;

    Base base(10);
    Deriver deriver(111,222);
    base.print();
    deriver.print();
    deriver.Base::print();
    //直接使用对象调用，不是多态，因为在编译时就已经知道是要用那个函数了
    //也没有用基类指针指向派生类对象，不满足第四条

    cout<<endl;
    func1(&base);
    func2(deriver);
}

int main()
{   
    test();
    return 0;
}

