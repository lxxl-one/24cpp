#include <iostream>

using std::cout;
using std::endl;

//4、在其他成员函数中调用虚函数，是否能体现动态多态
//有时能体现有时不能，具体看是否满足5个条件
class Base{
public:
    virtual
    void print(){
        cout<<"Base::_base="<<_base<<endl;
    }
    Base(long base=0)
    : _base(base)
    {
        cout<<"Base()"<<endl;
    }
    ~Base(){
        cout<<"~Base()"<<endl;
    }

    void func1(){
        print();
    }
    void func2(){
        Base::print();
    }

private:
    long _base;
};


class Deriver : public Base
{
public:
    void print(){
        cout<<"Deriver::_deriver"<<_deriver<<endl;
    }
    Deriver(long base=0,long deriver=0)
    : Base(base)
    , _deriver(deriver)
    {
        cout<<"Deriver()"<<endl;
    }
    ~Deriver(){
        cout<<"~Deriver()"<<endl;
    }

    void func1(){
        print();
        //this->print();  //this指针指向pbase，pbase指向deriver，所以满足第5条
    }
    void func2(){
        this->Base::print();    //已经明确表明是Base类中的print，编译时就知道
    }
    //甚至func1、func2都不用写出来，也是一样的效果

private:
    long _deriver;
};

void test(){
    cout<<sizeof(Base)<<endl;
    cout<<sizeof(Deriver)<<endl;

    Base base(10);
    Deriver deriver(111,222);

    cout<<endl;
    Base *pbase1=&base;
    pbase1->func1();
    pbase1->func2();

    cout<<endl;
    Base *pbase2=&deriver;
    pbase2->func1(); //222，体现了多态
    pbase2->func2();//111，因为没有用基类指针调用调用该虚函数
    
    //使用对象直接调用，和多态没什么关系了，因为在编译时就已经知道是调用哪个函数了
    //deriver.func1();
    //deriver.func2();
}

int main()
{   
    test();
    return 0;
}

