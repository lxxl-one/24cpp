#include <iostream>

using std::cout;
using std::endl;

//5、在构造函数与析构函数中访问虚函数，不能体现动态多态
//因为派生类对象都没创建完，或者派生类对象先没了
class Grandpa
{
public:
    Grandpa(){
        cout<<"Grandpa()"<<endl;
    }
    ~Grandpa(){
        cout<<"~Grandpa()"<<endl;
    }

    virtual
        void func1(){
            cout<<"void Grandpa::func1()"<<endl;
        }
    virtual
        void func2(){
            cout<<"void Grandpa::func2()"<<endl;
        }
};

class Father
: public Grandpa
{
public:
    Father()
    : Grandpa()
    {   //初始化列表完成，对象创建完毕。
        //此时Father对象已经有了虚表，所以func1是father的
        //这里就算是this->func1，但是不满足第四条，不是动态多态
        cout<<"Father()"<<endl;
        func1();
    }
    ~Father(){
        //当走到这里时，son已经被销毁，所以son没有虚表，func2还是father虚表里的func2
        cout<<"~Father()"<<endl;
        func2();
    }

    virtual
        void func1(){
            cout<<"void Father::func1()"<<endl;
        }
    virtual
        void func2(){
            cout<<"void Father::func2()"<<endl;
        }
};

class Son
: public Father
{
public:
    Son(){
        cout<<"Son()"<<endl;
        func1();
    }
    ~Son(){
        cout<<"~Son()"<<endl;
        func2();
    }

    virtual
        void func1(){
            cout<<"void Son::func1()"<<endl;
        }
    virtual
        void func2(){
            cout<<"void Son::func2()"<<endl;
        }
};

void test(){

}

int main()
{   
    Son son;
    cout<<endl;
    return 0;
}

