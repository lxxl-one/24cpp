#include <iostream>

using std::cout;
using std::endl;

//抽象类的第一种写法：纯虚函数
//纯虚函数/抽象类 的好处：提供了统一的接口，留给各自派生类去各自实现
//例子见9_figure
class Base{
public:
    virtual void show() const = 0;
    //纯虚函数，留给儿子去实现
    
    virtual void print() const =0;
};

class Deriver
: public Base
{
public:
    void show() const override
    {
        cout<<"void Deriver::show() const"<<endl;
    }
    
    //继承了Base的print没有实现，所以Deriver也是抽象类

};

class Deriver2
: public Deriver
{
public:
    virtual void print() const{
        cout<<"void Deriver2::print() const"<<endl;
    }
};

void test(){
    //Deriver deriver;  //不能创建
    Deriver2 deriver2;
    deriver2.show();    //虚函数通过父亲继承给儿子，儿子没有重写
    deriver2.print();

/*
    Base base;
    base.show();
    声明了纯虚函数的类是抽象类，抽象类不能创建对象
    因为不知道要分配多大的内存空间

    抽象类的派生类中，如果也有纯虚函数没有实现(不管是自带的还是继承来的)
    那么这个派生类也会变成抽象类
*/
    //但可以创建这种类型的指针or引用,因为都是分配8字节空间
    cout<<endl;
    Base *pbase=&deriver2;
    pbase->show();
    pbase->print();
    Deriver &pderiver=deriver2;
    pderiver.print();
}

int main()
{   
    test();
    return 0;
}

