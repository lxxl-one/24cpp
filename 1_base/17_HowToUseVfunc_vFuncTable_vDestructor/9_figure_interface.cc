#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

class Figure{
public:
    virtual void display() const =0;
    virtual double area() const =0;
};

class Circle
: public Figure
{
public:
    Circle(double radius)
    : _radius(radius)
    {

    }
    
    void display() const {
        cout<<"Circle";
    }
    double area() const {
        return 3.14159*_radius*_radius;
    }
private:
    double _radius;
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length, double width)
    : _length(length)
    , _width(width)
    {

    }
    
    void display() const {
        cout<<"Rectangle";
    }
    double area() const {
        return _length * _width;
    }
private:
    double _length;
    double _width;
};

class Triangle
: public Figure
{
public:
    Triangle(double a, double b,double c)
    : _a(a)
    , _b(b)
    , _c(c)
    {

    }
    
    void display() const {
        cout<<"Triangle";
    }
    double area() const {
        double p=(_a + _b + _c)/2;
        return  sqrt(p*(p-_a)*(p-_b)*(p-_c));
    }
private:
    double _a;
    double _b;
    double _c;
};


void func(const Figure &fig){
    fig.display();
    cout<<"的面积："<<fig.area()<<endl;
}

void test(){
    Circle cir(5.5);
    Rectangle rec(10,20);
    Triangle tri(3,4,6);


    func(cir);
    func(rec);
    func(tri);
}

int main()
{   
    test();
    return 0;
}

