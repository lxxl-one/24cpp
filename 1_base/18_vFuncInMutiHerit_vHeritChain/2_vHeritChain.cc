#include <iostream>

using std::cout;
using std::endl;

//当继承链上存在虚继承的基类，则最底层的子类要负责完成该基类的成员的构造
//即B的构造函数中调用A构造函数不起作用，要在C中调用
//如果在最底层不写，则默认调用无参构造函数
//如果没有无参构造函数，则会在继承链上被多次初始化，即B和C上都初始化，报错
//
//怎么理解虚拟继承？
//我猜的：1、不再划分区域，从上到下每个数据成员都只有一个
//2、最底层要调用所有的构造函数，完成链上所有类的初始化

class A{
public:
    A(){
        cout<<"A()"<<endl;
    }

    A(int ia): _ia(ia){
        cout<<"A(int)"<<endl;
    }

    ~A(){
        cout<<"~A()"<<endl;
    }
    
    int _ia;
};

class B
: virtual public A
{
public:
    B(){
        cout<<"B()"<<endl;
    }

    B(int ia,int ib)
    : A(ia) //没用
    , _ib(ib)
    {
        cout<<"B(int, int)"<<endl;
    }

    ~B(){
        cout<<"~B()"<<endl;
    }

    int _ib;
};

class C
: public B
{
public:
    C(int ia,int ib, int ic)
    : A(ia) //最底层要写出
    ,  B(1000,ib)
      //这里写多少，_ia都是ia，调用B的构造函数只会让B的_ib为ib，
      //对ia只有A的构造函数起作用
    , _ic(ic)
    {
        cout<<"C(int, int ,int)"<<endl;
    }

    ~C(){
        cout<<"~C()"<<endl;
    }

    void print(){
        cout<<"_ia="<<_ia<<endl;
        cout<<"_ib="<<_ib<<endl;
        cout<<"_ic="<<_ic<<endl;
    }

    int _ic;
};

void test(){
    C c(10,20,30);
    c.print();
    cout<<c._ia<<endl;
    cout<<c._ib<<endl;
    cout<<c.A::_ia<<endl;
    cout<<c.B::_ia<<endl;
    cout<<c.B::_ib<<endl;

    cout<<endl;
    B b(11,22);
    cout<<b._ia<<endl;
    cout<<b._ib<<endl;
}

int main()
{   
    test();
    return 0;
}

