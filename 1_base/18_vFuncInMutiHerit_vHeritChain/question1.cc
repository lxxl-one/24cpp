#include<iostream>

using std::endl;
using std::cout;
class Base
{
public:
    Base(int j)
    : i(j) 
    {
        
    }
    virtual  ~Base() 
    {
        
    }
    
    void func1() 
    {
        i *= 10;
        func2();    //这里用的是deriver的func2
        //如果写成Base::fun2()，才是Base中的func2
    }
    
    int getValue()
    {
        return  i;
    }
    
protected:
    virtual void func2()
    {
        i++;
    }
    
protected:
    int i;
};

class Child
: public Base
{
public:
    Child(int j)
    : Base(j) 
    {
        
    }
    void func1()
    {
        i *= 100;
        func2();
    }
    
protected:
    void func2()
    {
        i = i+2;
    }
};

int main() 
{
    Base * pb = new Child(1);
    pb->func1();
    cout << pb->getValue() << endl; 
    //结果是12
    //先调用Base的func1，这个没问题
    //问题是，在func1中的调用的是deriver中的func2
	delete pb; 
    
	return 0;
} 
