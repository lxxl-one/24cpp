#include <iostream>

using std::cout;
using std::endl;

class A{
public:
    virtual
    void func(int val=1){
        cout<<"A->"<<val<<endl;
    }
    virtual
    void test(){
        func();
    }

private:    
    long _a;
};

class B : public A 
{
public:
    virtual
    void func(int val=10){
        cout<<"B->"<<val<<endl;
    }
private:
    long _b;
};


int main()
{   
    B b;
    A *p1=&b;
    B *p2=&b;
    p1->func();
    //为什么是B->1
    //因为在编译时就能看见参数列表。但这时只知道是A类型指针的func，所以传入的参数为1
    //运行时，满足动态多态，执行的是B的虚表里的func
    //就好比如果写成p1->func(100)，没有任何疑惑
    p2->func();
    return 0;
}

