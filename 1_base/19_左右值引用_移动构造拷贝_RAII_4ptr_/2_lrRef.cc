#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void test(){
    int number=5;
    //int &ref2=10;     
    //10是右值，不能绑定到非const左值，说明非const左值引用不能识别右值

    const int &ref3=10;
    &ref3;  
    const int &ref4=number;
    &ref4;
    //const左值可以绑定到右值，且ref3仍是左值
    //也可以绑定到左值，说明const左值引用不能区分左值右值

    int &&rref=10;  //右值引用，可以识别并绑定到右值，
    //int &&rref1=number;    不能绑定到左值
    
    &rref;  //右值引用可以是左值，
            //一般有名字的情况下是左值，或者在const string &&rref参数列表里也是左值

}

int &&func(){
    return 10;
}

int main()
{   
    test();
    //  &func();    右值引用也可以是右值
    return 0;
}

