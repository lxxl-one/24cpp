#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

/*
    在C语言中，FILE是一个结构体类型，用于表示文件流。
    它提供了对文件的输入和输出操作。
    FILE结构体通常由C标准库中的函数所使用，用于打开、关闭、读取和写入文件。

    在标准C库中，FILE结构体的定义通常是不透明的，因此程序员无法直接访问其内部成员。
    通常我们只能通过标准C库提供的函数来操作FILE结构体，
    比如fopen用于打开文件，fclose用于关闭文件，fread和fwrite用于读写文件等。

    在C++中，FILE结构体通常被认为是C风格的文件操作方式，
    而更推荐使用C++标准库中的fstream类来进行文件操作。
    fstream类提供了更加面向对象的文件操作方式，并且使用起来更加方便和安全。 

    什么是裸指针？
    裸指针是指不被封装在智能指针或其他数据结构中的指针，它直接指向内存中的某个地址。
    在C++中，裸指针通常指的是使用原生指针来管理内存，需要手动管理内存的分配和释放。
*/   

//利用构造函数初始化资源，利用析构函数释放资源，避免忘记
//即利用栈对象的生命周期管理资源
//RAII
class SafeFile{
public:
    SafeFile(FILE *fp)
    : _fp(fp)
    {
        cout<<"SafeFile(FILE *)"<<endl;
        if(nullptr == fp)
            cout<<"_fp == nullptr"<<endl;
    }

    void write(const string &msg){
        fwrite(msg.c_str(), msg.size(),1,_fp);
    }

    ~SafeFile(){
        cout<<"~SafeFile()"<<endl;
        if(_fp){
            fclose(_fp);
            cout<<"fclose(_fp)会把指针释放掉"<<endl;
        }
    }

private:
    FILE *_fp;  //裸指针
};

void test(){
    string msg="hello,world\n";
    SafeFile sf(fopen("4.txt","a+"));  //sf是栈对象
    sf.write(msg);
}

int main()
{   
    test();
    return 0;
}

