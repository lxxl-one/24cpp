#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::auto_ptr;

void test(){
    int *pint = new int(10);    //裸指针
    auto_ptr<int> ap(pint); //智能指针
    cout<<"*pint="<<*pint<<endl;
    cout<<"*ap="<<*ap<<endl;    //ap解引用，得到ap指针的指向，和pint相同
    cout<<"pint="<<pint<<endl;
    cout<<"ap.get()="<<ap.get()<<endl;  //将智能指针降为裸指针

    cout<<endl;
    auto_ptr<int> ap2=ap;   //拷贝构造函数
    cout<<&ap<<" "<<&ap2<<endl;
    cout<<"*ap2="<<*ap2<<endl;
    cout<<"ap2.get()="<<ap2.get()<<endl;
/*
    cout<<"*ap="<<*ap<<endl;
    本来不加这一句，没问题
    加了这一句报错，说明可能是拷贝构造函数时出了问题

    查看源码，auto_ptr的拷贝构造函数，是_pdata(pdata.release())
    而release在执行时，已经把pdata即ap置为nullptr（相当于移动拷贝）
    那么之后再打印*ap，对nullptr解引用，肯定报错

    auto_ptr的缺陷是，在执行拷贝构造函数时，已经发生了所有权转移，所以不再使用
*/
}

int main()
{   
    test();
    return 0;
}

