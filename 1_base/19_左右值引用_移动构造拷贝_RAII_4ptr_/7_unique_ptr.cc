#include <iostream>
#include <memory>
#include <vector>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::vector;

//独享所有权
//不允许复制和赋值(对象语义)，直接把那俩函数delete
//作为容器元素时，只能传入右值

class Point{
public:
    Point(int ix, int iy): _ix(ix),_iy(iy)
    {
        cout<<"Point(int, int)"<<endl;
    }
    ~Point(){
        cout<<"~Point()"<<endl;
    }

    void print() const{
        cout<<_ix<<','<<_iy<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    unique_ptr<Point> up(new Point(111,222)); //智能指针
    up->print();
    cout<<"up.get()="<<up.get()<<endl;

    cout<<endl;
    //unique_ptr<Point> up2=up;   //不允许复制
    
    cout<<endl;
    unique_ptr<Point> up3(new Point(333,444));
    //up3=up; //不允许赋值
    
    vector<unique_ptr<Point>> vec;
    //vec.push_back(up);
    //vec.push_back(up3);
    //报错，push_back传入参数时调用拷贝构造函数
    
    //而unique_ptr有移动语义，所以传入右值即可
    vec.push_back(std::move(up));
    vec.push_back(std::move(up3));
    vec.push_back(unique_ptr<Point> (new Point(555,666)));  //直接传入临时对象
}

int main()
{   
    test();
    return 0;
}

