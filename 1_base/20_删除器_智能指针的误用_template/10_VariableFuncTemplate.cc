#include <iostream>

using std::cout;
using std::endl;

//template<typename T1,typename T2, ...>    有很多个T，全部打包给Args，模板参数包
//void print(T1 t1,T2 t2, ...)  函数参数包，且是最后一个参数
template <typename ...Args>
void print(Args ...args){
    cout<<sizeof...(Args)<<endl;
    cout<<sizeof...(args)<<endl;
}

void test(){
    //测试参数包有多大
    //在编译的时候，会自动推导出来各自T1、T2、T3 ... 是什么
    print();        //0 0
    print(1,"hello");   //2 2
    print(1,"hello",true,3.3);  //4 4
}

int main()
{   
    test();
    return 0;
}

