#include <iostream>

using std::cout;
using std::endl;

/*
    可变模板参数是C++11引入的特性，允许函数模板接受数量可变的参数。
    可变模板参数有以下几个特点：

    可变参数模板（Variadic Templates）：
    允许函数模板或类模板接受数量可变的参数，可以接受任意数量的参数。
        
    使用递归展开（Expansion by recursion）：
    在处理可变参数模板时，通常会使用递归展开的方式来处理每个参数，直到处理完所有参数。

    使用递归终止条件（Termination condition）：
    在递归展开的过程中，需要定义终止条件，以防止无限递归。
    通常会使用重载或特化来定义终止条件。

    参数包展开（Parameter pack expansion）：
    可以使用展开操作符...来展开参数包，将参数包中的参数逐个处理。

    灵活处理不定数量的参数：
    可变模板参数使得模板可以更加灵活地处理不确定数量的参数，从而实现更加通用和灵活的模板设计。
*/

void print(){
    //没有参数的情况，单独写出来
    cout<<endl;
}

template <typename T,typename ...Args>
void print(T t1,Args ...args){
    //cout<<T<<" ";   不能打出类型
    
    cout<<t1<<" ";
    print(args...);
    //递归。一次拆出一个T
    //注意最后一次调用的是上面那个print()，所以会换行
    // ...args打包，args...解包
}

/*
如果写在这里，递归会报错
void print(){
    //没有参数的情况，单独写出来
    cout<<endl;
}
*/

void test(){
    print();
    print(1,"hello");
    print(1,"hello",true,3.3);
    print(1,"hello",false,"world",5.5);
}

int main()
{   
    test();
    return 0;
}

