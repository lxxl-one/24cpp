#include <iostream>

using std::cout;
using std::endl;

//用可变参数模板计算1+2+...+100的值
int sum(){
    return 0;
}

template <typename T,typename ...Args>
int sum(T x,Args ...args){
    //cout<<x<<endl;
    return x+sum(args...);
}

void test(){
    cout<<"sum(1,2, ...10)="<<sum(1,2,3,4,5,6,7,8,9,10)<<endl;
    //缺点就是要把函数参数全部写出，不能sum(1, 2, ...10)
}

int main()
{   
    test();
    return 0;
}

