#include <iostream>

using std::cout;
using std::endl;

template<typename T>
class Test{
public:
    template<typename K>
    virtual K add(K x,K y){
            return x+y;
        }
    //成员模板不能设为虚函数
    //模板发生的时机在编译，虚函数在运行时
    
    //虚函数的调用发生在运行时，而模板成员函数的实例化发生在编译时。
    //在编译时，编译器无法确定所有可能的模板实例化类型，
    //因此无法为每个可能的实例化生成对应的虚函数表。
    
    //有虚函数就会有虚表，而所有虚函数的入口地址都会存于虚表
    //但是现在是模板形式，不能确定到底推导出多少个虚函数，也就不能推导出虚表大小
private:
    T _ix;
};

void test(){

}

int main()
{   
    test();
    return 0;
}

