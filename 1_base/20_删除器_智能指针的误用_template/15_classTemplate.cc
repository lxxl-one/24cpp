#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

//template <typename T> 一样的
template <class T>
class MyContainer {
private:
    T _element;

public:
    MyContainer(T element)
    : _element(element) 
    {

    }

    T getValue() {
        return _element;
    }
};

void test(){
    //MyContainer intContainer(42); 报错
    MyContainer<int> intContainer(42);
    MyContainer<string> stringContainer("Hello");

    cout << intContainer.getValue() << endl;  // 输出 42
    cout << stringContainer.getValue() << endl;  // 输出 Hello
}

int main()
{   
    test();
    return 0;
}

