#include <iostream>
#include <memory>
#include <string>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::unique_ptr;
using std::string;

//对于unique_ptr，
//初始化、reset都会导致二次释放
//没有赋值or复制

class Point{
public:
    Point(int ix,int iy)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point(int,int)"<<endl;
    }

    void print(){
        cout<<_ix<<","<<_iy<<endl;
    }

    ~Point(){
        cout<<"~Point()"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point *pint=new Point(1,2);
    unique_ptr<Point> up(pint);
    unique_ptr<Point> up2(pint);
    //二次释放：使用了不同的智能指针托管了同一堆空间
}

void test1(){
    unique_ptr<Point> up(new Point(1,2));
    up->print();
    unique_ptr<Point> up2(new Point(3,4));
    up2->print();

    cout<<endl;
    up.reset(up2.get());
    //先释放1,2，然后up指向up2
    //二次释放：使用了不同的智能指针托管了同一堆空间
    up->print();
    up2->print();
}

int main()
{   
    test1();
    return 0;
}

