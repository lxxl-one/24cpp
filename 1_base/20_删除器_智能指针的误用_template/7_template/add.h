#ifndef __ADD_H__
#define __ADD_H__

//模板不能写在头文件、实现文件，即声明和实现分开，
//编译时不会出错，但链接时会出错
template<typename T>
T add(T x,T y);

#include "add.cc"
//加上这句话，可以直接g++ test.cc
//因为test里包含了头文件，头文件包含了实现文件

#endif
