#include <iostream>

using std::cout;
using std::endl;

template<typename T =int, short ksize=10>   
//T是类型参数，ksize是非类型参数，不能是浮点型
//typename == class
T func(T x,T y){
    return x*y*ksize;
}

void test(){
    int ia=3,ib=4;
    //cout<<func(ia,ib)<<endl;  
    //如果不写默认值，则不能推导出ksize
    //总之，模板的参数列表和总常见的函数参数列表一样，如果没有默认值，都要在<>中给出
    
    cout<<func<int,5>(ia,ib)<<endl;
    cout<<func(ia,ib)<<endl;
}

int main()
{   
    test();
    return 0;
}

