#include <iostream>

using std::cout;
using std::endl;

class Example{
public:
    Example(double ix)
    : _ix(ix)
    {

    }
    
    //成员函数模板
    template <typename T>
        T func(){
            return (T)_ix;
        }

private:
    double _ix;
};

void test(){
    Example ex(10.5);
    cout<<"ex.func()="<<ex.func<int>()<<endl;
}

int main()
{   
    test();
    return 0;
}

