#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test(){
    //初始化方法如下：
    //1、无参
    //vector<int> number;
    
    //2、count个value
    //vector<int> number(10,3);   //10个3
    
    //3、迭代器范围
    //int arr[10]={1,3,5,7,9,8,6,4,2,10};
    //vector<int> number(arr,arr+10);  //左闭右开，arr是第一个元素的入口地址

    //4、拷贝与移动
    //vector<int> number={1,3,4,2,6,8,7,5,9,10};
    //vector<int> number2=number;
    //vector<int> number2(number);
    
    //5、大括号形式。=可以去掉
    vector<int> number={1,3,4,2,6,8,7,5,9,10};
    //vector<int> number{1,3,4,2,6,8,7,5,9,10};
    
    int num=555;
    int &q=num;
    number.push_back(q);
    number.push_back(num);


    //遍历方法如下：
    //1、下标遍历
    //vector的下标访问运算符有越界风险，可以用at函数
    for(size_t idx=0;idx!=number.size();++idx){
        //cout<<number[idx]<<" ";
        cout<<number.at(idx)<<" ";
    }
    cout<<endl;

    //2、未初始化的迭代器
    vector<int>::iterator it;
    for(it=number.begin();it!=number.end();++it){
        cout<<*it<<" ";
        *it=*it+1;
    }
    //可以更改，it指向也变了
    cout<<endl;

    //3、初始化的迭代器
    vector<int>::iterator it2=number.begin();
    for(;it2!=number.end();++it2){
        cout<<*it2<<" ";
    }
    cout<<endl;

    //4、for循环+auto
    for(auto &elem:number){
        cout<<elem<<" ";
    }
    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

