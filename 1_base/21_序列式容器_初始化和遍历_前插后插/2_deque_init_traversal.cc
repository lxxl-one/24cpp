#include <iostream>
#include <deque>

using std::cout;
using std::endl;
using std::deque;

//和vector一模一样
void test(){
    //初始化方法如下：
    //1、无参
    //deque<int> number;
    
    //2、count个value
    //deque<int> number(10,3);   //10个3
    
    //3、迭代器范围
    //int arr[10]={1,3,5,7,9,8,6,4,2,10};
    //deque<int> number(arr,arr+10);  //左闭右开，arr是第一个元素的入口地址

    //4、拷贝与移动
    //deque<int> number={1,3,4,2,6,8,7,5,9,10};
    //deque<int> number2=number;
    //deque<int> number3(number);
    
    //5、大括号形式。=可以去掉
    deque<int> number={1,3,4,2,6,8,7,5,9,10};
    //deque<int> number{1,3,4,2,6,8,7,5,9,10};


    //遍历方法如下：
    //1、下标遍历
    for(size_t idx=0;idx!=number.size();++idx){
        cout<<number[idx]<<" ";
    }
    cout<<endl;

    //2、未初始化的迭代器
    deque<int>::iterator it;
    cout<<&it<<endl;
    for(it=number.begin();it!=number.end();++it){
        cout<<*it<<" ";
        cout<<&it<<endl;    //it自己的地址不变，因为它是指针
    }
    //当遍历完，it指向确实走了，*it得到的也变，但it自己的地址是不变的
    cout<<endl;

    //3、初始化的迭代器
    deque<int>::iterator it2=number.begin();
    for(;it2!=number.end();++it2){
        cout<<*it2<<" ";
    }
    cout<<endl;

    //4、for循环+auto
    for(auto &elem:number){
        cout<<elem<<" ";
    }
    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

