#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

//对于3种 序列式容器 vector、deque、list初始化方式一样
//遍历方式除了list不支持下标访问，其他都一样
void test(){
    //初始化方法如下：
    //1、无参
    //list<int> number;
    
    //2、count个value
    //list<int> number(10,3);   //10个3
    
    //3、迭代器范围
    int arr[10]={1,3,5,7,9,8,6,4,2,10};
    list<int> number(arr,arr+11);  //左闭右开，arr是第一个元素的入口地址，11越界，得到无关的数

    //4、拷贝与移动
    //list<int> number2(number);
    //list<int> number3=number;
    
    //5、大括号形式。=可以去掉
   // list<int> number={1,3,4,2,6,8,7,5,9,10};


    //遍历方法如下：
/*
    1、list不支持下标访问
    因为它是链表，不是数组
 
    for(size_t idx=0;idx!=number.size();++idx){
        cout<<number[idx]<<" ";
    }
    cout<<endl;
*/

    //2、未初始化的迭代器
    list<int>::iterator it;
    for(it=number.begin();it!=number.end();++it){
        cout<<*it<<" ";
    }
    cout<<endl;

    //3、初始化的迭代器
    list<int>::iterator it2=number.begin();
    for(;it2!=number.end();++it2){
        cout<<*it2<<" ";
    }
    cout<<endl;

    //4、for循环+auto
    for(auto &elem:number){
        cout<<elem<<" ";
    }
    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

