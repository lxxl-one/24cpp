#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

//vector不能在头部删除插入，因为要挪动开销太大
//其他两个可以
template <typename Container>
void display(const Container &con){
    cout<<sizeof(vector<int>)<<endl;
    cout<<sizeof(vector<long>)<<endl;
    cout<<sizeof(vector<double>)<<endl;
    //vector模板类本身并不存储元素，它只存储指向动态分配的内存的指针和一些其他控制信息。
    //因此，sizeof(vector<int>) 和 sizeof(vector<long>) 都将返回相同的大小，
    //3个指针当前头、当前使用空间的尾即插入位置、当前可用空间的后一位

    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

void test(){
    
    vector<int> number={1,2,4,6,8,9,3,6,7};
    display(number);

    cout<<endl<<"在vector尾部插入与删除"<<endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);
}

int main()
{   
    test();
    return 0;
}

