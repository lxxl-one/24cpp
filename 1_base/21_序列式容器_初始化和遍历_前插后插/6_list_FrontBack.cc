#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

template <typename Container>
void display(const Container &con){
    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

void test(){
    
    list<int> number={1,2,4,6,8,9,3,6,7};
    display(number);

    cout<<endl<<"在list尾部插入与删除"<<endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);

    cout<<endl<<"在list头部插入与删除"<<endl;
    number.push_front(100);
    number.push_front(200);
    display(number);
    number.pop_front();
    display(number);
}

int main()
{   
    test();
    return 0;
}

