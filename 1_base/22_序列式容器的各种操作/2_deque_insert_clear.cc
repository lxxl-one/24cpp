#include <iostream>
#include <deque>
#include <vector>

using std::cout;
using std::endl;
using std::deque;
using std::vector;

template <typename Container>
void display(const Container &con){
    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

void test(){
    
    deque<int> number={1,2,4,6,8,9,3,6,7};
    display(number);

    cout<<endl<<"在deque尾部插入与删除"<<endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);

    cout<<endl<<"在deque头部插入与删除"<<endl;
    number.push_front(100);
    number.push_front(200);
    display(number);
    number.pop_front();
    display(number);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cout<<endl<<"在任意位置进行插入"<<endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout<<*it<<endl;
    number.insert(it,33);  //找一个位置插入一个元素
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;

    cout<<endl;
    number.insert(it,3,666);
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;

    cout<<endl;
    vector<int> vec={200,500,600,300};
    //迭代器范围地插入
    number.insert(it,vec.begin(),vec.end());
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;
    
    cout<<endl;
    number.insert(it,{123,456,789});    //大括号插入
    display(number);
    cout<<"it指向位置不变，但是该位置上的元素发生了变化，"<<*it<<endl;
    //因为之前，包括list的insert，都是在该位置之前插入，所以该位置元素一直是2
    //而此时后面元素较少，挪动少，所以先从该位置往后挪3个，腾出3个位置给123、456、789
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cout<<endl<<"获取第一个、最后一个元素"<<endl;
    cout<<number.front()<<endl;
    cout<<number.back()<<endl;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cout<<endl<<"清空所有元素"<<endl;
    number.clear();
    number.shrink_to_fit();
    cout<<"size()="<<number.size()<<endl;
}

int main()
{   
    test();
    return 0;
}

