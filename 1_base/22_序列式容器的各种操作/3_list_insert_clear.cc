#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con){
    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

void test(){
    
    list<int> number={1,2,4,6,8,9,3,6,7};
    display(number);

    cout<<endl<<"在list尾部插入与删除"<<endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);

    cout<<endl<<"在list头部插入与删除"<<endl;
    number.push_front(100);
    number.push_front(200);
    display(number);
    number.pop_front();
    display(number);

    cout<<endl<<"在任意位置进行插入"<<endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout<<*it<<endl;
    auto it2=number.insert(it,33);  //有的插入方式，具有返回类型，接收一下
    cout<<*it2<<endl;
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;

    cout<<endl;
    number.insert(it,3,666);
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;

    cout<<endl;
    vector<int> vec={200,500,600,300};
    //迭代器范围地插入
    auto it3=number.insert(it,vec.begin(),vec.end());
    cout<<"返回的指针指向插入的第一个元素，"<<*it3<<endl;
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;
    
    cout<<endl;
    number.insert(it,{123,456,789});
    display(number);
    cout<<"此时迭代器指向位置、及其元素不变，还是"<<*it<<endl;

    cout<<endl<<"获取第一个、最后一个元素"<<endl;
    cout<<number.front()<<endl;
    cout<<number.back()<<endl;

    cout<<endl<<"清空所有元素"<<endl;
    number.clear();
    cout<<"size()="<<number.size()<<endl;
}

int main()
{   
    test();
    return 0;
}

