#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test(){
    vector<int> number={1,3,4,6,4,4,4,4,9,7};
    for(auto it=number.begin();it!=number.end();++it){
        if(4==*it){
            number.erase(it);
        //并没有把所有的4删除
        //因为删除了当前，后面位置的4挪到这里，之后不再处理这个位置
        }
    }
    for(auto it=number.begin();it!=number.end();++it){
        cout<<*it<<" ";
    }
    cout<<endl;
}

void test2(){
    vector<int> number={1,3,4,6,4,4,4,4,9,7};
    for(auto it=number.begin();it!=number.end();){  //或者从后往前走
        if(4==*it){
            number.erase(it);
        }
        else{
            ++it;
        }
    }
    for(auto it=number.begin();it!=number.end();++it){
        cout<<*it<<" ";
    }
    cout<<endl;
}

int main()
{   
    test2();
    return 0;
}

