#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class Point{
public:
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"Point(int, int)"<<endl;
    }
    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"Point(const Point &)"<<endl;
    }
    int _ix;
    int _iy;
};

void test(){
    vector<Point> vec;
    vec.reserve(100);

    Point pt(1,2);
    vec.push_back(pt);  //拷贝
    vec.push_back(Point(3,4));  //拷贝
    vec.emplace_back(5,6);      //不用拷贝，就地构建一个Point

    auto it=vec.end();
    --it;
    vec.emplace(it, Point(7,8));    //拷贝
    cout<<it->_ix<<endl;    // 7
    ++it;
    cout<<it->_ix<<endl;    // 5

    //emplace对应insert、emplace_back对应push_back
}

int main()
{   
    test();
    return 0;
}

