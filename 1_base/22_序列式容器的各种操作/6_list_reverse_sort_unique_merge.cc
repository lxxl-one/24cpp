#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con){
    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

struct CompareInt{
    //不用在意为什么是重载了()，既然是函数对象，只需要写好相应接口传进去即可
    bool operator()(const int &lhs, const int &rhs) const
    {
        //cout<<"ComparePoint"<<endl;
        return lhs > rhs;
    }
};

void test(){
    
    list<int> number={1,2,4,6,8,9,3,6,9,6,7};
    display(number);

    cout<<endl<<"反转"<<endl;
    number.reverse();
    display(number);

    cout<<endl<<"排序"<<endl;
    //number.sort();    //默认从小到大
    //number.sort(std::greater<int>());  //要求传入排序器对象，这里是临时对象
    number.sort(CompareInt());
    display(number);

    cout<<endl<<"去掉连续重复的元素"<<endl;
    number.unique();
    display(number);

    cout<<endl<<"合并两个链表"<<endl;
    list<int> number2={11,33,55,99,5,22,77};
    number.sort();
    number2.sort();
    number.merge(number2);  
    //在merge时，要先对两个list都进行从小到大排序（必须是从小到大）
    //然后merge得到的就是有序的，总体有序
    //而不是有序1+有序2=有序1有序2，不是局部有序
    //如果不排序，则和直接拼接到后面没什么区别
    display(number);
    display(number2);   //number2不存在了

}


int main()
{   
    test();
    return 0;
}

