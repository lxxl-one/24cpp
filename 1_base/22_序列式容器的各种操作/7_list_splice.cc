#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con){
    for(auto &elem : con){
        cout<<elem<<" ";
    }
    cout<<endl;
}

struct ComparePoint{    //不用在意为什么是重载了()，只需要写好相应接口传进去即可
    bool operator()(const int &lhs, const int &rhs) const
    {
        //cout<<"ComparePoint"<<endl;
        return lhs > rhs;
    }
};

void test(){
    list<int> number={1,2,4,6,8,9,6,3,6,7,6};
    display(number);

    list<int> number2={11,33,55,88};
    display(number2);

    cout<<endl;
    auto it=number.begin();
    ++it;
    ++it;
    cout<<"*it="<<*it<<endl;
    number.splice(it,number2);  //所有元素都移动到it前
    cout<<"*it="<<*it<<endl;
    display(number);
    display(number2);   //不存在了

    list<int> number3={111,333,555,888,999,777,222};
    display(number3);
    it=number.begin();
    ++it;
    ++it;
    cout<<"*it="<<*it<<endl;
    auto it3=number3.end();
    --it3;
    --it3;
    cout<<"*it3="<<*it3<<endl;
    number.splice(it,number3,it3);  //某个元素移动到it前
    display(number);
    display(number3);   //只是移动的那个元素没了

    cout<<endl;
    auto it4=number3.begin();
    auto it5=number3.end();
    --it5;
    --it5;
    --it5;
    cout<<"*it="<<*it<<endl;
    cout<<"*it4="<<*it4<<endl;
    cout<<"*it5="<<*it5<<endl;
    number.splice(it,number3,it4,it5);  //移动迭代器范围，[it4,it5）移动到it前
    display(number);
    display(number3);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cout<<endl<<"splice同一个list，可用于LRU算法"<<endl;
    auto it6=number.begin();
    cout<<"*it6="<<*it6<<endl;
    auto it7=number.end();
    --it7;
    --it7;
    cout<<"*it7="<<*it7<<endl;
    number.splice(it6,number,it7);
    display(number);
    //1 2 777 111 333 555 11 33 55 88 4 6 8 9 6 3 6 7 6 
    //7 1 2 777 111 333 555 11 33 55 88 4 6 8 9 6 3 6 6
    // 7移到1前面

}

int main()
{   
    test();
    return 0;
}

