#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void printCapacity(const vector<int> &con)
{
    cout << "size() = " << con.size() << endl;
    cout << "capacity() = " << con.capacity() << endl;
}

void test()
{
    vector<int> number = {1, 2, 4, 6, 8, 9, 3, 6, 7};
    display(number);
    printCapacity(number);

    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(11);
    display(number);
    printCapacity(number);

    cout << endl << "在vector的任意位置进行插入" << endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 33);
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    //size() = m = 11, capacity() =n=18,待插入元素的个数就是t
    //1、当t < n - m,底层不会发生扩容
    //2、当 n  - m < t < m,按照2 * size()进行扩容
    //3、当 n - m < t < n, t > m,会按照t + m进行扩容
    //4、当 n - m < t, t > n,会按照 t + m进行扩容
    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 30, 666);
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    cout << endl;
    vector<int> vec = {200, 500, 600, 300};
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


