#include <iostream>
#include <unordered_set>

using std::cout;
using std::endl;
using std::unordered_set;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //unordered_set的特征
    //1、存放的是key类型，key值是唯一的，不能重复
    //2、key值是没有顺序的
    //3、底层使用的是哈希
    //4、构造函数和set一样
    //count、find、insert、clear、erase和set一样
    unordered_set<int> number = {1, 4, 7, 9, 8, 6, 3, 6, 7};
    display(number);

    //number[2];//哈希表已经改变了插入位置
    auto it=number.begin();
    //*it=5; 不能修改，因为已经根据插入时的值按哈希表插入了
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


