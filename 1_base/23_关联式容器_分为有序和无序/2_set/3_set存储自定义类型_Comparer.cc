#include <math.h>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

using std::cout;
using std::endl;
using std::set;
using std::pair;
using std::vector;

//未解决的问题：
//在判断时，如果是横纵坐标都相等的情况，无论是 < 还是 > 都返回false
//但是怎么知道是踢出呢？

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        operator<<(cout, elem)<<"   ";
        //cout << elem << "  ";
    }
    cout << endl;
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        /* cout << "Point(int = 0, int = 0)" << endl; */
    }

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
        /* cout << "~Point()" << endl; */
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
    friend bool operator<(const Point &lhs, const Point &rhs);
    friend struct ComparePoint; //友元结构体
    friend struct Greater; //友元结构体
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
       << ", " << rhs._iy
       << ")";

    return os;
}

//1、默认比较器里的运算符重载，优先级最低
bool operator<(const Point &lhs, const Point &rhs)
{
    cout << "bool operator<(const Point &, const Point &)"<<endl;
    /*
        重载的 < ，用于比较两个点的大小。
        因为set默认的比较器中，
    template<
            class key
            class Compare = std::less<key>
        > class set;
    std::less是一个结构体，它里面重载了 operator()
    bool operator()(const T &lhs, const T &rhs) ，
    在这个重载函数里，利用了 <  符号比较lhs和rhs
    正是因为这个 < 不能比较两个点大小，所以重载<即可

    至于std::less在里面是怎么进行比较的，或者说为什么重载了()，不管我们的事
    */

    if(lhs.getDistance() < rhs.getDistance())
    {
        return true;    //说明左边确实小于右边，满足<
    }
    else if(lhs.getDistance() == rhs.getDistance())
    {
        if(lhs._ix < rhs._ix)
        {
            return true;
        }
        else if(lhs._ix == rhs._ix)
        {
            if(lhs._iy < rhs._iy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else 
    {
        return false;
    }
}

//函数对象
//2、不用std::less，自己写一个用于Point的比较器，
//先看原先的默认比较器是怎么写的
struct ComparePoint
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        cout << "struct ComparePoint" << endl;
        if(lhs.getDistance() < rhs.getDistance())
        {
            return true;
        }
        else if(lhs.getDistance() == rhs.getDistance())
        {
            if(lhs._ix < rhs._ix)
            {
                return true;
            }
            else if(lhs._ix == rhs._ix)
            {
                if(lhs._iy < rhs._iy)
                {
                    return true;
                }
                else
                {
                    return false;   //横纵坐标都相等，踢出
                }
            }
            else
            {
                return false;
            }
        }
        else 
        {
            return false;
        }
    }
};
struct Greater{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        if(lhs.getDistance() < rhs.getDistance())   // 左<右，false
            return false;
        
        else if(lhs.getDistance() > rhs.getDistance())  // 左>右，true
            return true;
        
        else{
            if(lhs._ix < rhs._ix)   //距离相等，横坐标小的，false
                return false;
            
            else if(lhs._ix == rhs._ix){
                if(lhs._iy > rhs._iy)   //距离相等横坐标相等，纵坐标大的，true
                    return true;
                /*
                else if(lhs.getY() == rhs.getY())
                {
                    return true;
                    //这么写，就不能去掉重复的元素了
                }
                */
                else
                    return false;
                    //距离相等，横坐标相等，只剩下纵坐标相等，应该踢出
                    //但是不懂是怎么踢出的
            }
            else    //距离相等，横坐标大的，true
                return true;
        }
    
    }
};

//命名空间是可以进行扩展的
namespace  std
{
//3、模板的特化
//因为less结构体在std里，现在将其特化针对Point
//相当于默认使用的std::less里，有针对比较Point的方法了
template <>
struct less<Point>
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        cout << "template <> struct less<Point>" << endl;
        if(lhs.getDistance() < rhs.getDistance())
        {
            return true;
        }
        else if(lhs.getDistance() == rhs.getDistance())
        {
            if(lhs.getX() < rhs.getX())     //这里写成友元不太方便
            {
                return true;
            }
            else if(lhs.getX() == rhs.getX())
            {
                if(lhs.getY() < rhs.getY())
                {
                    return true;
                }
                /*
                else if(lhs.getY() == rhs.getY())
                {
                    return true;
                    这么写，就不能去掉重复的元素了
                }
                */
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else 
        {
            return false;
        }
    }
};//end of namespace less

}//end of namespace std


void test()
{
    set<Point> number = {
    //set<Point,Greater> number = {
    /* set<Point, ComparePoint> number = {  用自己写的比较器 */
        Point(1, 2),
        Point(1, -2),
        Point(-1, 2),
        Point(3, 2),
        Point(1, 2),
        Point(-2, 2),
    };
    display(number);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


