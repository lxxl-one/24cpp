#include <math.h>
#include <iostream>
#include <unordered_map>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::unordered_map;
using std::pair;
using std::make_pair;
using std::string;

//对于unordered_map，只要它的key能够hash，就不用重写
//这里key是string类型，c++中已经特化了它的hash函数
//value想是啥是啥
//key依然不能重复
template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
       << ", " << rhs._iy
       << ")";

    return os;
}

void test()
{
    unordered_map<string, Point> number = {
        {"wangdao", Point(1, 2)},
        {"wd", Point(2, 2)},
        {"wuhan", Point(3, 2)},
        {"wd", Point(2, 2)},
        pair<string, Point>("nice", Point(1, 2)),
        pair<string, Point>("beijing", Point(1, 2)),
        make_pair("nanjing", Point(3, 4)),
        make_pair("dongjing", Point(3, 4)),
    };
    display(number);
}

int main()
{
    test();
    return 0;
}


