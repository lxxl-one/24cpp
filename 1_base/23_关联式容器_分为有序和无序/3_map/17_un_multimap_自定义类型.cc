#include <math.h>
#include <iostream>
#include <unordered_map>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::unordered_multimap;
using std::pair;
using std::make_pair;
using std::string;

//key可以重复
template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
       << ", " << rhs._iy
       << ")";

    return os;
}

void test()
{
    unordered_multimap<string, Point> number = {
        {"wangdao", Point(1, 2)},
        {"wd", Point(2, 2)},
        {"wuhan", Point(3, 2)},
        {"wd", Point(2, 2)},
        {"wd", Point(2, 5)},
        pair<string, Point>("nice", Point(1, 2)),
        pair<string, Point>("beijing", Point(1, 2)),
        make_pair("nanjing", Point(3, 4)),
        make_pair("dongjing", Point(3, 4)),
        {"wd", Point(3, 5)},
    };
    display(number);
}

int main()
{
    test();
    return 0;
}


