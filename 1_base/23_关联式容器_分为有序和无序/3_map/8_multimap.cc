#include <iostream>
#include <map>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::multimap;
using std::pair;
using std::make_pair;
using std::string;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

void test()
{
    //multimap的特征
    //1、存放的是key-value类型，key值是不唯一的，可以重复，
    //value既可以重复也可以不重复
    //2、默认情况下，会按照key值进行升序排列
    //3、底层使用的也是红黑树
    //4、不支持下标访问，因为会有二义性
    multimap<int, string> number = {
    /* multimap<int, string, std::greater<int>> number = { */
        pair<int, string>(1, "wuhan"),
        pair<int, string>(9, "beijing"),
        pair<int, string>(9, "jing"),
        {3, "wangdao"},
        {5, "hubei"},
        {8, "wangdao"},
        make_pair(4, "shanghai"),
        make_pair(7, "shanghai"),
    };
    display(number);

    cout << endl << "multimap的查找" << endl;
    size_t cnt = number.count(9);
    cout << "cnt = " << cnt << endl;

    multimap<int, string>::iterator it = number.find(4);
    if(it != number.end())
    {
        cout << "该元素在set中 : " 
             << it->first << "  "
             << it->second << endl;
    }
    else
    {
        cout << "该元素不在multimap中" << endl;
    }

    cout <<endl << "insert，都会成功" << endl;
    number.insert(pair<int, string>(6, "nanjing"));
    /* number.insert(make_pair(6, "nanjing")); */
    /* number.insert({6, "nanjing"}); */
    display(number);

    cout << endl << "不支持下标访问" << endl;
#if 0
    cout << "number[5] = " << number[5] << endl;//查找
    cout << "number[2] = " << number[2] << endl;//插入
    display(number);

#endif
    cout << endl;
    //T &operator[](const Key &)
    it=number.begin();
    it->second = "wangdao";//支持修改，但不支持通过下标访问来修改
    /* number.operator[](2).operator=("wangdao"); */
    /* const multimap<int, string> tmp = {{1, "hello"}, {2, "wuhan"}}; */
    /* cout << tmp[2] << endl; */
    display(number);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


