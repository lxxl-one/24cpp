#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::copy;

void test()
{
    //可以看成是对vector进行遍历了
    vector<int> vec{1, 4, 7, 9};
    ostream_iterator<int> osi(cout, "\n");
    copy(vec.begin(), vec.end(), osi);
/*
    std::copy函数是C++标准库中的一个算法，用于将一个范围内的元素复制到另一个范围。
    
template<class InputIt, class OutputIt>
OutputIt copy(InputIt first, InputIt last, OutputIt d_first)
{
    while (first != last) {
        *d_first++ = *first++;
    }
    return d_first;
}

    起始迭代器（InputIterator first）：指向要复制的范围的起始位置的迭代器。
    结束迭代器（InputIterator last）：指向要复制的范围的结束位置的迭代器（不包括该位置的元素）。
    目标位置迭代器（OutputIterator d_first）：指向目标范围的起始位置的迭代器，复制的元素将被写入到这个位置开始的上述规定范围中。
*/
    //所以，如果不使用流迭代器，则cout根本无法参与copy函数
    //相当于遍历了 vector
}

int main()
{
    test();
    return 0;
}


