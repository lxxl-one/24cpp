#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::cin;
using std::istream_iterator;
using std::ostream_iterator;
using std::vector;
using std::copy;

void test()
{
    vector<int> vec;
    /* vec.reserve(10); */
    /* vec[0] = 10;//error */
    
    istream_iterator<int> isi(cin);
    //1、对于vector而言，插入元素一般使用的是push_back
    //copy(isi, istream_iterator<int>(), vec.begin());
    //段错误，因为vec.begin()是一个迭代器，不是vector，没有push_back，写不进数据
    //但是copy的第三个参数是迭代器，不能直接传入vec，怎么办？
    //其次，因为一开始vec没有指定容量，所以没有空间来写入数据
    //如果一开始就指定了空间，则这行不会报错，但是写不进vec，因为没有push_back
    
    copy(isi, istream_iterator<int>(), std::back_inserter(vec));
/*
    插入迭代器的构造函数
    explicit back_insert_iterator(_Container& __x) : container(&__x) {}

    类体外的返回插入迭代器对象的函数
    template <class _Container>
    inline back_insert_iterator<_Container> back_inserter(_Container& __x) {
        return back_insert_iterator<_Container>(__x);
    }
*/

    //接1、copy里的 *d_first++ = *first++，对于插入迭代器，是调用了拷贝构造函数，
    //而它的拷贝构造函数是执行了插入迭代器存储的vec的push_back
    //所以才成功将元素插入到vec
    
    //2、既然isi永远不可能等于第二个参数（临时对象），那么copy要怎么退出循环？
/*
private:
    istream_type* _M_stream;    //指向输入流对象的指针
    _Tp _M_value;               // 暂存从输出流迭代器中读出的数据
    bool _M_ok;                 //流状态

   istream_iterator() : _M_stream(0), _M_ok(false) {}
   istream_iterator(istream_type& __s) : _M_stream(&__s) { _M_read(); }

   void _M_read() {
    _M_ok = (_M_stream && *_M_stream) ? true : false;
    //指向输入流对象的指针、输入流对象本身都不为空
    if (_M_ok) {
        *_M_stream >> _M_value;     // 相当于 cin >> value
        M_ok = *_M_stream ? true : false;  
        //再次检查输入流对象是否正常
        //比如输入了ctrl+d或者hello，和value的类型不符合，
        //那么流对象不正常，(cin)为false，mok变为false，从而影响了后面的M_equal
        //
        //参考之前的打开文件
        // ifstream ifs("wd.txt");
        // if(!ifs)
        //   cerr<<"ifs is not good";
    }
  }


//__x = isi;
// __y = istream_iterator<int>();
inline bool operator!=(const istream_iterator& __x, const istream_iterator&__y)
{
    return !__x._M_equal(__y);
}
bool _M_equal(const istream_iterator& __y) const
{
    return (_M_ok == __y._M_ok) && (!_M_ok || _M_stream == __y._M_stream);
    //输出流迭代器的默认构造函数中，M_ok（流状态）为false，即y的M_ok是false
    //那么当x的M_ok为false时（比如输入ctrl+d、hello），
    //返回结果是x、y相等，取反后！=不成立，copy退出while循环
}

*/
    
    
    //遍历
    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "\n"));
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


