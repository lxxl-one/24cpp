#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

using std::cout;
using std::endl;
using std::vector;
using std::copy;
using std::list;
using std::set;
using std::ostream_iterator;
using std::back_inserter;
using std::back_insert_iterator;
using std::front_inserter;
using std::front_insert_iterator;
using std::inserter;
using std::insert_iterator;

//copy函数(来自algorithm) 和 3个inserter，实现容器互插
//其中包含的3个：inserter、front_inserter、back_inserter
//分别实现了 插入(insert)、头插(push_front)、尾插(push_back)
//inserter是函数模板，返回类型是相应的insert_iterator类模板
//容器仍然保留自身特性，比如set依然是从小到大，去重
void test()
{
    vector<int> vecNumber = {1, 3, 5, 7};
    list<int> listNumber = {11, 33, 77, 55, 44};

    //1、将list中的元素插入到vector尾部
    //copy(listNumber.begin(), listNumber.end(),back_inserter(vecNumber));
    copy(listNumber.begin(), listNumber.end(), 
         back_insert_iterator<vector<int>>(vecNumber));
    copy(vecNumber.begin(), vecNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;
    
    //2、将vector中的元素插入到了list头部，注意是头插法
    cout << endl;
    //copy(vecNumber.begin(), vecNumber.end(), front_inserter(listNumber));
    copy(vecNumber.begin(), vecNumber.end(),
         front_insert_iterator<list<int>>(listNumber));
    copy(listNumber.begin(), listNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;

    //3、将vector中的元素插入到set的任意位置
    //inserter比另外两个多一个参数，即insert函数的插入位置it
    cout << endl;
    set<int> setNumber = {1, 3, 8, 11, 100, 77, 55, 34};
    auto it = setNumber.begin();
    //copy(vecNumber.begin(),vecNumber.end(),insert_iterator<set<int>>(setNumber, it));
    copy(vecNumber.begin(), vecNumber.end(),
         inserter(setNumber, it));
    copy(setNumber.begin(), setNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;
         
}

int main()
{
    test();
    return 0;
}


