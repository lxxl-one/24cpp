#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

//占位符占据的是函数的形参的位置
//占位符中的数字代表的是函数的实参的位置。

int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

void test()
{
    //占位符
    auto f4 = bind(add, 1, std::placeholders::_1);
    cout << "f4(9) = " << f4(9) << endl;
}

void func2(int x1, int x2, int x3, const int &x4, int x5)
{
    cout << "x1 = " << x1 << endl
         << "x2 = " << x2 << endl
         << "x3 = " << x3 << endl
         << "x4 = " << x4 << endl
         << "x5 = " << x5 << endl;
}

void test2()
{
    using namespace std::placeholders;
    
    //bind进行传参的时候，使用的是值传递，而不是 引用传递
    //std::cref,引用的包装器 const reference
    //std::ref,引用的包装器 reference
    int number = 100;
    
    //auto f = bind(func2, 10, _3, _1, number, number);
    //f(40, 50, 60, 70, 80);
    //在传参时，传入参数的个数必须大于占位符中的最大数字，比如这里是3
    //10、60、40、100、100
    
    auto f = bind(func2, 10, _3, _1, std::cref(number), number);
    &f;
    number = 500;
    f(40, 50, 60, 70, 80);
    //10、60、40、500、100
    
    number = 800;
    f(40, 50, 60, 70, 80);
    //10、60、40、800、100
    //多余的参数凑人数罢了，直接扔掉
}

int main()
{
    test2();
    return 0;
}


