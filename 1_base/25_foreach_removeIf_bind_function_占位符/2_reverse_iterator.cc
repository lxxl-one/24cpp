#include <iostream>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::vector;
using std::reverse_iterator;

//迭代器的++总是begin走向end
void test()
{
    vector<int> vec = {1, 3, 5, 7, 9};
    vector<int>::iterator it = vec.begin();
    for(;it != vec.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

    vector<int>::reverse_iterator rit = vec.rbegin();
    for(;rit != vec.rend(); ++rit)  //重载了++运算符
    {
        cout << *rit << "  ";
    }
    cout << endl;
}
int main()
{
    test();
    return 0;
}


