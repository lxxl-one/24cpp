#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::ostream_iterator;
using std::copy;

void func(int &value)
{
    ++value;
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 3, 5, 9, 7, 8, 6, 5};
    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    for_each(vec.begin(), vec.end(), func); //要传入一元函数
    //指定范围内的每个元素都经历一遍func，func(*it)
    cout << endl;

    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main()
{
    test();
    return 0;
}


