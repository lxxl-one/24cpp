#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;

void func(int &value)
{
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 3, 5, 9, 7, 8, 6, 5};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl << endl;
    size_t cnt = count(vec.begin(), vec.end(), 15);
    cout << "cnt = " << cnt << endl;    // 0，如果是5则cnt=2

    cout << endl << endl;
    auto it = find(vec.begin(), vec.end(), 7);
    if(it != vec.end())     //可以发现，find的过程就是++it，所以最后以end跳出循环
    {
        cout << "查找成功 " << *it << "  ";
    }
    else
    {
        cout << "查找失败" << endl;
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


