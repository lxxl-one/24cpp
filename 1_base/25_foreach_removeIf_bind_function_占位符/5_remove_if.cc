#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;
using std::remove_if;

//remove_if + erase：删除指定范围内所以满足一元断言的元素
//整个过程可以看成：
//首先调用find_if，返回第一个>5的元素的迭代器，记为1
//然后从1的后一个位置2开始往后遍历，
//如果是<5的，则这个元素复制一份到1，然后1、2都++
//如果是>5的，则忽略，只让2++
//最终停下来的1，它以及它之后的所有元素都是多余的
//整个过程有点像筛选

void func(int &value)
{
    cout << value << "  ";
}

//一元断言\谓词
bool func3(int value)
{
    return value > 5;
}

void test()
{
    vector<int> vec = {1, 3, 5, 7, 8, 9, 4, 3, 2, 6, 2};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl;
    //remove_if不能一次将满足func3条件的元素删除，
    //但是可以返回待删除元素的迭代器，从这个位置开始以后的元素都是多余的
    //然后配合erase函数进行使用，就可以达到效果
    
    //为什么不一步到位？
    //因为remove_if要满足所有容器，对list可以边遍历边删，但是vector却可能跳过(erase那一节)
    //所以remove_if只是拿后面的，不会被删除的元素往前填补删除元素的空缺，保证不漏掉每一个元素
    
    auto it = remove_if(vec.begin(), vec.end(), func3); //要传入一元断言
    
    cout<<"remove_if完结果如下："<<endl;
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout<<"erase一遍："<<endl;
    vec.erase(it, vec.end());
    
    for_each(vec.begin(), vec.end(), func);
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


