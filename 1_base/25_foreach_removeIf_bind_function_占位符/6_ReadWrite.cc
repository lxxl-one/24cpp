#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

//读写时没有对共享资源进行保护，引起迭代器失效

void printCapacity(const vector<int> &vec)
{
    cout << "size() = " << vec.size() << endl;
    cout << "capacity() = " << vec.capacity() << endl;
}

void test()
{
    vector<int> vec;
    //vec.reserve(10);
    vec.push_back(1);
    //printCapacity(vec);

    bool flag = true;

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
        if(flag)
        {
            vec.push_back(2);
            flag = false;
            //printCapacity(vec);
            it=vec.begin(); //如果不重置，底层发生了扩容，导致迭代器失效了
        }
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


