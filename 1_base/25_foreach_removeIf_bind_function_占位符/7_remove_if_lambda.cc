#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;
using std::remove_if;

void func(int &value)
{
    cout << value << "  ";
}

/* 
 bool func3(int value)
{ 
    return value > 5;
} 
*/


void test()
{
    vector<int> vec = {1, 3, 5, 7, 8, 9, 4, 3, 2};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl;
    int a = 10;
    //lambda表达式，也叫匿名函数
    //在需要函数对象的地方替代传统的函数对象或函数指针。
    //Lambda表达式允许你在需要时定义一个简单的函数，而无需显式地定义一个命名函数
    //[],叫做捕获列表
    auto it = remove_if(vec.begin(), vec.end(), 
                        //[a](int value)mutable->bool{  可以修改，但不影响外部
                        //[&a](int value)->bool{ 可以修改并影响外部
                        [&a](int value){
                        ++a;
                        cout << "a = " << a << endl;
                        return value > 5;
                        });
    cout<<a<<endl;
    vec.erase(it, vec.end());

    for_each(vec.begin(), vec.end(), func);
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


