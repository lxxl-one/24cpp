#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;
using std::remove_if;
using std::bind1st;
using std::bind2nd;

//bind1st、bind2nd用于绑定二元函数
//作用是固定原本函数的参数
//返回一个函数对象

void func(int &value)
{
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 3, 5, 7, 8, 9, 4, 3, 2};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl;
    //一元断言/谓词
    //auto it = remove_if(vec.begin(), vec.end(),  
    //                   bind2nd(std::greater<int>(), 5));
    //将greater的第二个参数固定为5，那么元素＞5时为true，删掉

    auto it = remove_if(vec.begin(), vec.end(), 
                        bind1st(std::less<int>(), 5));
    //将less的第一个参数固定为5，那么 5<元素 为true，删掉
    //如果这里把less的第二个参数固定为5，则 元素<5 为true，即小于5的都删掉
    
    vec.erase(it, vec.end());

    for_each(vec.begin(), vec.end(), func);
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


