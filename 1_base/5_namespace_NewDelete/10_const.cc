#include <iostream>

using std::cout;
using std::endl;

//宏定义只有在运行时才能发现错误
#define MULTIPLY(x,y) x*y

void test(){

    int a;
    a=4;
    const int number = 10;
    //c++中定义常量的语法，常量在定义时必须初始化
    //编译时进行检查
    // number =20; 不能再赋值
    cout<<MULTIPLY(1+2,3+4)<<endl;
    //MULTIPLY(1+2,3+4) 在宏替换时会被替换成 1+2*3+4
}

int main()
{
    test();
    return 0;
}

