#include <iostream>

using std::cout;
using std::endl;

//总结：const修饰谁，谁就不能改变
//双const则都不能改变

void test0(){
    int value =10;
    int *a = &value;
    //a =0x7ffe90c7ecfc;
    //这种操作在C++中通常被视为危险操作，因此编译器会报错
    cout << *a <<endl;
}

void test1(){
    int number = 1;
    int value =10;
    const int *p = &number;
    cout << *p <<endl;
    p = &value;
    cout << *p <<endl;
    //const位于 星号的左边，称为 常量指针 (int const哪个在前都一样)
    //可以改变指针的指向，从而改变*p=10。但不能直接改变*p=10。
}

void test2(){
    int number = 1;
    int value =10;
    int * const p = &number;
    cout << *p <<endl;

    //p = &value;
    //cout << *p <<endl;
    //常量指针报错
    
    *p = 20;
    cout << *p <<endl;
    cout << number<<endl;
    //const位于 星号右边，称为 指针常量。
    //可以改变指针所指变量的内容，但不能改变指向。
}

int main()
{
    test2();
    return 0;
}

