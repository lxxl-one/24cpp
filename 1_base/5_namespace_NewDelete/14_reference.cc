#include <iostream>

using std::cout;
using std::endl;

void test(){
    int number =10;
    int &ref =number;
    printf("%p\n",&number);
    printf("%p\n",&ref);
    cout << ref << endl;    
    ref=20;
    cout << number <<endl;

    cout<<endl;
    int a=number;
    cout<<a<<endl;
    a=30;
    cout<<number<<endl;
    //可以发现，改变ref和number都一样
    //但是改变a，不会同时改变number
    //
    
    cout<<endl;
    ref=a;
    cout<<ref<<endl;
    cout<<number<<endl;


    cout<<endl;
    printf("%p\n",&a);
    printf("%p\n",&number);
    printf("%p\n",&ref);
    //引用第一次和谁绑定，就固定不变了，不会再去和a绑定
    //本质上是一个指针常量，int * const ref



}


int main()
{
    test();
    return 0;
}

