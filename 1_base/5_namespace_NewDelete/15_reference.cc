#include <iostream>

using std::cout;
using std::endl;

#if 0
void swap(int x, int y){
    int tmp = x;
    x=y;
    y=tmp;
    cout<<x<<y<<endl;
}
#endif

#if 0
//地址传递
void swap(int *x, int *y){
    int tmp = *x;
    *x=*y;
    *y=tmp;
    cout<<*x<<*y<<endl;
}
#endif

//引用传递
void swap(int &x, int &y){
    int tmp = x;
    x=y;
    y=tmp;
    cout<<x<<y<<endl;
}

void test(){
    int a =3,b=4;
    swap(a,b);
    cout<< a<<b<<endl;

}

int main()
{
    test();
    return 0;
}

