#include <iostream>

using std::cout;
using std::endl;

int arr[10]={1,3,5,8};
int a=10;

int &func(size_t idx){//size_t是无符号整型
    return arr[idx];
}

int func1(size_t idx){//size_t是无符号整型
    return arr[idx];
}

#if 0
int &func3(){
    //不能返回局部变量的引用
    //因为它的生命周期只在这个函数中，返回后就不存在了
    int number =10;
    return number;
}
#endif

int &func3(){
    //int *pint=new int[100];
    int *pint = new int(100);
    return *pint;
    //这个局部变量的生命周期比函数大，因为没有释放掉
    //但会出现内存泄漏
    //除非存在内存自动回收机制，否则不建议返回堆空间的引用
}


int &func4(){
    return a;
}
//如果是int func4，则返回的是一个整数，那么此时test里的int &b=func4就会报错
//因为声明了一个整型的引用 b，并尝试将函数 func4 的返回值赋给它。
//但是，函数 func4 的返回类型是整型，而不是整型的引用，
//因此无法将其返回值赋给一个整型的引用变量。


void test2(){
    int c=1,d=2;
    cout<<c+func3()+d+func4()<<endl;

    int &ref=func3();
    delete &ref;
    //注意delete后面跟的是一个指针，
    //那么我们可以通过取地址，把引用变成指针
    //但是上面的func3仍然没有释放
}


void test(){
    cout<<func1(0)<<endl;
    //func1(0)=100;  返回的是一个值，不能修改
    func(0)=1000;//返回的是引用，可以修改
    cout<<func(0)<<endl;
    cout<<arr[0]<<endl;

    int &b=func4();
    cout<<b<<endl;
    b=250;
    cout<<a<<endl;
    func4()=251;
    cout<<a<<endl;
}

int main()
{
    test();
    return 0;
}

