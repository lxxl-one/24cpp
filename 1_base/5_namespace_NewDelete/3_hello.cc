#include <iostream>

using namespace std;//命名空间

void test();//函数的声明，可以有多次

void test(){//函数的定义(实现)，只有一次

}

#if 0
void test(){

}
#endif

int main(int argc, char *argv[])
{
    //cout是标准输出,输出流的对象
    /* cout << "Hello world" << endl; */
    //cout 是C++标准库中的标准输出流对象，它允许我们向标准输出设备写入数据。我们可以使用 cout 对象来将数据输出到屏幕上。
    
    // 运算符重载
    operator<<(cout, "Hello world");
    //operator<<(cout, endl); 报错
    cout.operator<<(endl);

    operator<<(cout, "Hello world").operator<<(endl);
    
    //cin标准输入，输入流对象。
    //cin 是C++标准库中的标准输入流对象，它允许我们从标准输入设备读取数据。我们可以使用 cin 对象来读取用户输入并将其赋值给变量。
    
    //输入流是用于从输入设备读取数据的数据流，而输出流是用于向输出设备写入数据的数据流。
    //输入流运算符 >> 用于从输入流中提取数据，而输出流运算符 << 用于向输出流中插入数据。
    //这些运算符可以被重载，以便我们可以使用它们来进行自定义类型的输入和输出操作。
    int number =10;
    cout << number << endl;
    cin >> number;
    cout << "number = " << number << endl;

    return 0;
}

