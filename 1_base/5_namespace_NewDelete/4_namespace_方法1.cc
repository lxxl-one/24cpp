#include <iostream>


using namespace std;
//std是标准命名空间的名字，是 <iostream> 头文件中定义的一个命名空间。
//在 C++ 标准库中，很多标准库中定义的函数、类等都放在 std 命名空间中，以避免命名冲突和提供更好的代码组织结构。
//
//using namespace <namespace_name>; 可以用来引入整个命名空间，
//使得该命名空间中的所有成员在当前作用域中可见，从而无需每次都使用命名空间限定符来访问。
//比如使用 using namespace std; ，就可以直接使用 std 命名空间中的所有成员，而无需每次都写上 std:: 前缀。
//1、我们不能完全知道，std里面有哪些实体，
//自己再去定义时，比如cout、变量、结构体等的名字与std中已有的相同，造成冲突
//2、可以一次将std中的实体全部引出来，方便，
//比如下面可以直接使用cout，不用using std::cout; std::cout<<number<<std::endl;
//即使在wd的命名空间里也能直接使用cout

//双冒号 :: 是作用域解析运算符，用于指定特定作用域中的变量或函数。
//当使用 ::number 时，它表示正在引用全局命名空间中的 number 变量，而不是当前命名空间中的变量

//=======================================================
int number = 100;

void print()
{
    cout << "void print()" << endl;
}
//=======================================================

namespace wd
{
int number =300;    //把number放入wd，和外面的number不是同一个了
int number2 =400;

void print()
{
    cout << "void wd::print()" << endl;
    cout << ::number << endl;
    cout << number << endl;
}
}//end of namespace wd

//using wd::number; //报错，和int number = 100 冲突
//using namespace wd; //如果在这里用，则test2和test3的第一句、test2的最后一句报错

void test(){
    using namespace wd;
    //using wd::number;
    //cout << "number=" << number << endl; //报错，如果写了上一句则为300
    cout << "number=" << ::number << endl; //100
    cout << "wd::number=" << wd::number << endl; //300
    cout << "wd::number2=" << number2 << endl; //400
    //因为有了using namespace wd，所以可以直接看见number2，
    //而number、print和全局命名空间有冲突，所以得用::指明是哪个
    
    //print();    //call to 'print' is ambiguous
    ::print();
    wd::print();
}

void test2(){
    cout << "number=" << number << endl; //100
    cout << "wd::number=" << wd::number << endl; //300
    //cout << "wd::number2=" << number2 << endl;
    //报错，因为没有引入wd，看不见number2
    //using wd::number2;如果加了这句则ok
    cout << "wd::number2=" << wd::number2 << endl; //400
    print();
}

void test3(){
    cout<<"number=" << number <<endl; //100，此时是全局的number
    using wd::number;
    cout<<"number=" << number <<endl; //300，此时全局的number已经被wd的number覆盖了
    //编译器首先查找当前作用域中是否有number，如果没有，则会向外层作用域查找，
    //直到找到或者到达全局作用域。
    //在这种情况下，因为using wd::number;引入了wd::number，
    //所以第二个cout语句中的number指向的是wd::number，其值为300。
}

int main()
{
    test();
    cout<<endl;
    test2();
    cout<<endl;
    test3();
    return 0;
}

