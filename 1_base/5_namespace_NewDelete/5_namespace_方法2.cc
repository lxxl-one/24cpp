#include <iostream>


//使用作用域限定符的好处
//哪怕自己定义的实体与std中的冲突，也不会产生歧义
//
//缺点：
//每次使用都要使用std::

int cout(int x, int y){
    std::cout << "x=" << x << ",y="<<y<<std::endl;
}

int main()
{   
    int a=3,b=4;
    cout(a,b);
    return 0;
}

