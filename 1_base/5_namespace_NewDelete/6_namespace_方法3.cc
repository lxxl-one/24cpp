#include <iostream>

//using <namespace_name>::<member_name>; 
//引入命名空间中的特定成员，使得特定成员在当前作用域中可见，而不是引入整个命名空间。
//一个一个地引出
//以后想方便使用哪个就引出哪个
using std::cout;

#if 0
int cout(int x, int y){
    std::cout << "x=" << x << ",y="<<y<<std::endl;
}
报错：
declaration conflicts with target of using declaration already in scope
#endif

int main()
{   
    int a=3,b=4;
    cout<<a<<","<<b<<std::endl;
   // cout(a,b);
    return 0;
}

