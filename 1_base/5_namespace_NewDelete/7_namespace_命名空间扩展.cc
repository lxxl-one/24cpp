#include <iostream>

using std::cout;
using std::endl;

//命名空间是可以扩展的,std也可以
namespace wd1{
//带命名空间的函数声明
void print();
}


namespace wd2{
void display(){
    cout << "void wd2::display()"<<endl;
    //现在要调用wd1中的print
    wd1::print();//后面进行扩展了，所以是有输出的
}
void func(){
    cout << "void wuhan::func()"<<endl;
}
}

//wd1的扩展
namespace wd1{
void print(){
    cout << "void wd1::print()"<<endl;
    wd2::func();
}
}//end of wd1


int main()
{
    wd2::display();
    return 0;
}

