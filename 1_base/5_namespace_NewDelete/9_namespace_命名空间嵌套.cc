#include <iostream>

using std::cout;
using std::endl;

int number = 100;

namespace wd{

int number =200;

void show(int value){
    cout << value <<endl;
    cout << number<< endl;
}
void print(){
    cout<<"wd::print()"<<endl;
}

namespace wh{

int number = 300;

void print(){
    cout<<"void wd::wh::print()"<<endl;
    
    cout<<number<<endl; //300
    //这里有个问题，如果wd2没有300，则看见的是wd的200，没有200则看见外面的100
    
    //cout<<wd::number<<endl; //200
    //cout<<::number<<endl; //100
    cout<<"命名空间里可以再定义命名空间"<<endl;
}

}//end of wh

}//end of wd

int main()
{   
    int a=300;
    wd::show(a);
    wd::print();
    cout<<endl;
    
    wd::wh::print();
    return 0;
}

