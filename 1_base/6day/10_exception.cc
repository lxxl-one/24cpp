#include <iostream>

using std::cout;
using std::endl;
using std::cin;

void test(){
    int x,y;
    cin >> x >> y;

    try
    {
        if(0==y)
        {
            throw y;
        }
        else
        {
            cout <<x/y<<endl;
        }
    }
    catch(double e)
    {
        cout<<"catch(double)"<<endl;
    }
    catch(int e)
    {   
        cout<<"catch(int)"<<endl;
    }
}

int main()
{
    test();
    return 0;
}

