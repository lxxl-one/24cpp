#include <iostream>
#include <string.h>

using std::cout;
using std::endl;

void test(){
    //字符数组
    //c的字符串是以\0结尾的
    char str1[]="hello";
    char str2[]={'w','o','r','l','d'};
    char *pstr="hello,world";
    
    //字符指针
    //strlen碰到\0就结束，长度不包含\0
    cout<<sizeof(str1)<<endl;
    cout<<sizeof(str2)<<endl;
    cout<<strlen(pstr)<<endl;
    
    cout<<endl;
    char str3[]="hel\0lo";
    cout<<str3<<endl;

    cout<<endl;
    cout<<sizeof(pstr)<<endl;
    //对于32位系统而言，一个系统是4字节，64位8字节
    
    cout<<endl;
    cout<<*pstr<<endl;
}

int main()
{
    test();
    return 0;
}

