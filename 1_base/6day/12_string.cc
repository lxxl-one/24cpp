#include <iostream>
#include <string.h>

using std::cout;
using std::endl;

void test(){
    //字符数组
    //c的字符串是以\0结尾的
    char str1[]="hello";//自带\0
    char str2[]={'w','o','r','l','d','\0'};//需要认为添加\0，不然容易出错
    str1[0]='H';
    cout<<str1<<endl;
    //不能改变指向 str1=nullptr

   const char *pstr="hello,world";
    //字符指针
    //strlen碰到\0就结束，长度不包含\0
    cout<<pstr<<endl;
   // pstr[0]='H';
   // 字符串时常量，所以应该用常量指针，即不能改变指针内容
   pstr="wuhan";
    cout<<pstr<<endl;
    cout<<endl;

    //拼接字符串
    //strcat(str1,str2); 容易发生内存踩踏
    
    size_t len=sizeof(str1)+sizeof(str2);
    char *ptmp=new char[len]();
    strcpy(ptmp,str1);
    strcat(ptmp,str2);
    cout<<ptmp<<endl;

    delete []ptmp;
    ptmp=nullptr;

}

int main()
{
    test();
    return 0;
}

