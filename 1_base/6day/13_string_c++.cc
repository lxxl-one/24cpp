#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void test(){
    string s1="hello";
    string s2="world";
    cout<<s1<<endl;
    cout<<s2<<endl;

    cout<<endl;
    cout<<s1.length()<<endl;
    cout<<s2.size()<<endl;

    cout<<endl;
    string s3 = "wuhan"+s1+s2+"wangdao"+'K';
    cout<<s3<<endl;
    s3[0]='H';
    cout<<s3<<endl;

    cout<<endl;
    for(size_t idx=0;idx!=s3.size();idx++){
        cout<<s3[idx]<<"  ";
    }
    cout<<endl;

    
    //c++风格字符串转为c风格
    cout<<endl;
    const char *pstr = s1.c_str();
    cout<<pstr<<endl;
}

int main()
{
    test();
    return 0;
}

