#include <iostream>
#include <stdlib.h>

using std::cout;
using std::endl;

void test(){
    int inum=10;
    float fnum=12.34;
    fnum=static_cast<float>(inum);

    void *pret=malloc(sizeof(int));
    int *pint=static_cast<int *>(pret);
    
    #if 0
    int num=1;
    int *error=&num;
    float *error1=static_cast<float *>(error);
    #endif
}

int main()
{
    test();
    return 0;
}

