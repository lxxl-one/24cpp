#include <iostream>

using std::cout;
using std::endl;

void test(){
    
    const int number=10;
    //int *pint= &number;
    //*pint=20;
    //如果第二行成立，那么第三行也成立，这是不允许的
   int *pint=const_cast<int *>(&number);
    //脱掉常量属性。但这样是危险的
    
   *pint=30;//未定义的行为
   cout<<number<<endl;
   cout<<*pint<<endl;
   cout<<pint<<endl;
   cout<<&number<<endl;
   cout<<&pint<<endl;
   

    
    cout<<endl;
    //int &a=number;
    int &a=const_cast<int &>(number);
    cout<<number<<endl;
    a=20;//同上，不被
    cout << a<<endl;
    cout<<number<<endl;
    //number仍然是10
    
    cout<<endl;
    cout<<&number<<endl;
    cout<<&a<<endl;

    //这种写法不被c++承认
}

int main()
{
    test();
    return 0;
}

