#include <iostream>
#include <stdlib.h>
#include  <string.h>

using std::cout;
using std::endl;

//所有add都是函数重载
//c++会把名字改编，将原名字和参数列表合在一起取名
int add(int x, int y){
    return x+y;
}

float add(float x, float y){
    return x+y;
}

int add(int x, long y){
    return x+y;

}
int add(long x, int y){
    return x+y;
}

int add(int x, int y, int z){
    return x+y;
}

void test(){
    int *pret=static_cast<int *>(malloc(sizeof(int)));
    memset(pret,0,sizeof(int));

    free(pret);
    pret=nullptr;

}

int main()
{
    test();
    return 0;
}

