#include <iostream>

using std::cout;
using std::endl;

//给参数设置默认值
//要从右向左连续地赋初值，和python不一样
int add(int x, int y, int z=1){
    return x+y+z;
}

int add1(int x=2, int y=4){
    return x+y;
}

void test(){

}

int main()
{   
    int a=3,b=4,c=5;
    cout<<add(a,b,c)<<endl;
    cout<<add1(b)<<endl;
    return 0;
}

