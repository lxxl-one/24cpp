#include <iostream>

using std::cout;
using std::endl;

#define MULTIPLY(x,y) ((x)*(y))

//函数的调用是有开销的，比如参数入栈出栈，保存现场
//采用内联函数，则可以像宏定义一样，直接拿函数体去替换，减少开销
//内联函数是小函数，不调用，不循环
inline int add(int x, int y){
    return x+y;
}


void test(){
    int a=3,b=4;
    cout<<add(a,b)<<endl;
}


int main()
{
    test();
    return 0;
}

