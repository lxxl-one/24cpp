#ifndef __ADD_H_    //防止在同一个文件中被多次编译_
#define __ADD_H__

//内联函数，不能分成头文件和实现文件，即声明和实现不能分开
//否则在链接的时候，链接不到函数的实现
//如果非要这么做，就得在此#include实现文件
inline
int add(int, int);
//#include"add.cc"
#endif
