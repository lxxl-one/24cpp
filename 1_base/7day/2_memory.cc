#include <iostream>

using std::cout;
using std::endl;

int gvalue;//全局变量，使用默认值进行初始化
char *p1;//全局变量
const int gnumber=10;

int main()
{
    int a;//局部变量，位于栈区，随机值
    char *p2;//栈区
    int *pint = new int(10);

    cout<<&a<<endl;
    cout<<&p2<<endl;
    cout<<&pint<<endl;
    printf("栈区\n");

    cout<<endl;
    cout<<pint<<endl;
    printf("堆区\n");

    cout<<endl;
    static int cnt=10;
    cout<<&cnt<<endl;
    printf("读写段，静态变量\n");
    cout<<&gvalue<<endl;
    cout<<&p1<<endl;
    printf("读写段，全局变量\n");
   
    
    cout<<endl;
    const char *pstr="hello,world";
    cout<<pstr<<endl;
    cout<<*pstr<<endl;
    printf("%p\n",pstr);
    cout<<&pstr<<endl;
    printf("只读段,文字常量区\n");

    cout<<endl;
    cout<<&main<<endl;
    printf("%p\n",main);
    printf("%p\n",&main);
    printf("只读段，程序代码段\n");
    
    cout<<endl;
    const int number=10;
    printf("%p\n",&gnumber);
    printf("%p\n",&number);
    //虽然都是常量，但是不在一个区
    //gnumber位于文字常量区
    //number位于栈区

    delete pint;

    return 0;
}

