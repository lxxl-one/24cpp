#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class Computer
{

public:
    //成员函数
    void setbrand(const char *brand){
        strcpy(_brand,brand);//有越界风险
    }

    void setprice(float price){
        _price =price;
    }

    void print(){
        cout<<_brand<<endl;
        cout<<_price<<endl;
    }

private:
    //数据成员
    //char *_brand;  //段错误
    char _brand[20];
    float _price;

};

int main()
{   
    Computer com;
    com.setbrand("mac");
    com.setprice(5400);
    com.print();
    return 0;
}

