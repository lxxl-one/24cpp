#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

struct Computer
{
//c++中的结构体做了提升，可以定义函数了
//class默认都是private
//struct默认都是public

    void setbrand(const char *brand){
        strcpy(_brand,brand);//有越界风险
    }

    void setprice(float price){
        _price =price;
    }

    void print(){
        cout<<_brand<<endl;
        cout<<_price<<endl;
    }

    //数据成员
    //char *_brand;  //段错误
    char _brand[20];
    float _price;

};

int main()
{   
    Computer com;
    com._price=5500;
    strcpy(com._brand,"mac");
    com.print();
    com.setbrand("huawei");
    com.print();
    return 0;
}

