#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    
    //拷贝构造函数
    Point(const Point &rhs)//不能去掉&，否则会在此无限拷贝，最终栈溢出
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"拷贝"<<endl;
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

    cout<<endl;
    Point pt2=pt1;//这里可以看见，没有初始化，说明有其他的默认函数,1
    cout<<"pt2=";
    pt2.print();

}

Point func(){
    Point pt2(4,5);
    cout<<"pt2=";
    pt2.print();

    return pt2;
}

void test2(){
    int number=10;
    int &ref=number;
    cout<< &number<<endl;
    //int &ref2 = 10; 不行，因为10是右值
    const int &ref2 =10;


    //拷贝函数Point(Point &rhs) 未加const的左值不能绑定右值
    //此时Point pt =func(); 是错的
    Point pt=func();
    //&func() 临时对象是右值，不能取地址
    cout<<"pt=";
    pt.print();
}

int main()
{   
    test2();
    return 0;
}

