#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    
    //拷贝构造函数
    Point(const Point &rhs)//不能去掉&，否则会在此无限拷贝，最终栈溢出
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"拷贝"<<endl;
    }


    //this指针隐藏在(非静态)成员函数的第一个位置
    //this指针是一个指针常量，不能改变指向，但可以this->_ix=100
    void print(){
        //cout<<'('<<_ix<<','<<_iy<<')'<<endl;
        cout<<'('<<this->_ix<<','<<this->_iy<<')'<<endl;
    }

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

    cout<<endl;
    Point pt2(3,4);//这里可以看见，没有初始化，说明有其他的默认函数,1
    cout<<"pt2=";
    pt2.print();

}

int main()
{   
    test();
    return 0;
}

