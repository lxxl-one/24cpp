#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"拷贝"<<endl;
    }

    void print(){
        cout<<'('<<this->_ix<<','<<this->_iy<<')'<<endl;
    }


    Point &operator=(const Point &rhs){//operator=是函数名，也可以理解为运算符重载
        cout<<"赋值运算符函数"<<endl;
        _ix=rhs._ix; //同样存在两次释放的问题，所以要改成深拷贝，见9文件夹
        _iy=rhs._iy;

        return *this;
    }
    

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

    cout<<endl;
    Point pt2(3,4);//这里可以看见，没有初始化，说明有其他的默认函数,1
    cout<<"pt2=";
    pt2.print();

    cout<<endl;
    //pt2=pt1;
    pt2.operator=(pt1);//赋值运算符函数，效果等价于上一行
    cout<<"pt2=";
    pt2.print();
}

int main()
{   
    test();
    return 0;
}

