#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class Computer
{

public:
    //成员函数
    void setbrand(const char *brand);
    void setprice(float price);
    void print();

private:
    //数据成员
    char _brand[20];
    float _price;

};

//类中的成员函数可以在外面实现
void Computer:: setbrand(const char *brand){
     strcpy(_brand,brand);//有越界风险
}

void Computer::setprice(float price){
    _price =price;
}

void Computer:: print(){
    cout<<_brand<<endl;
    cout<<_price<<endl;
}

int main()
{   
    Computer com;
    com.setbrand("mac");
    com.setprice(5400);
    com.print();
    return 0;
}

