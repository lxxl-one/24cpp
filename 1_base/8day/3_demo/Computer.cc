#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//类中的成员函数可以在外面实现
void Computer:: setbrand(const char *brand){
     strcpy(_brand,brand);//有越界风险
}

void Computer::setprice(float price){
    _price =price;
}

void Computer:: print(){
    cout<<_brand<<endl;
    cout<<_price<<endl;
}

