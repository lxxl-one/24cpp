#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Computer
{

public:
    //成员函数
    void setbrand(const char *brand);
    void setprice(float price);
    void print();

private:
    //数据成员
    char _brand[20];
    float _price;

};

#endif
