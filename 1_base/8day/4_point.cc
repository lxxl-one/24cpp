#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    //构造函数，初始化数据成员
    //如果自己没有写，编译器会默认提供 无参构造函数
    //如果都写，则可以构造有参和无参,即构造函数重载
    Point()
    : _ix(0)
    , _iy(0)
    {
        cout<<"无参数"<<endl;
    }

    Point(int ix, int iy)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        int a=10;
        int b(a);
        cout<<b<<endl;
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }
private:
    int _ix;
    int _iy;
};

void test(){
    //创建对象时，会自动调用构造函数
    Point pt(2,3);
    Point pt2;
    pt.print();
    pt2.print();
}

int main()
{   
    test();
    return 0;
}

