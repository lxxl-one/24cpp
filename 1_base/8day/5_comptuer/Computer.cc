#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

Computer::Computer(const char *brand, float price)
:_brand(new char[strlen(brand)+1]())//申请空间，避免越界
,_price(price)
{   
    strcpy(_brand,brand);//赋值
    cout<<"调用了构造函数，避开了set"<<endl;
}

void Computer:: print(){
    cout<<_brand<<endl;
    cout<<_price<<endl;
}

Computer::~Computer(){
    if(_brand){
        cout<<"默认提供的析构函数没啥用，要自己写释放,尤其是堆对象"<<endl;
        delete []_brand;
        _brand=nullptr;
    }
    else{
        cout<<"没有要释放的堆空间上的变量"<<endl;
    }
    cout<<"已经销毁"<<endl;
}
