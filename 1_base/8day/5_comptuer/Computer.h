#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Computer
{

public:
    Computer(const char *brand, float price);
    void print();
    ~Computer();
private:
    //数据成员
    char *_brand;
    float _price;

};

#endif
