#include "Computer.h"

void test(){
    Computer com("mac",5500);
    com.print();
    //com.~Computer();//不建议自己手动显式调用析构函数
    //com.print();//这个并非不打印，而是碰到空指针要打印，自己终止了
    

    Computer *pc=new Computer("xiaomi",6000);
    pc->print();
    delete pc;
    pc = nullptr;
    //对于堆对象，析构函数不会自动执行，需要delete，才相当于析构
}

int main()
{   
    test();
    return 0;
}

