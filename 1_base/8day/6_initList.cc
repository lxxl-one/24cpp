#include <iostream>

using std::cout;
using std::endl;

class Example{

public:
    Example(int value)
    : _ix(value)
    , _iy(_ix)
    //不管有没有交叉，都按照21行，声明顺序进行初始化
    {
        cout<<"初始化"<<endl;
    }

    void print(){
        cout<<"("<<_ix<<","<<_iy<<")"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Example ex(10);
    ex.print();
}

int main()
{   
    test();
    return 0;
}

