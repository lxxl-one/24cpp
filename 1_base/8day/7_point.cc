#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    //构造函数，初始化数据成员
    //如果自己没有写，编译器会默认提供 无参构造函数
    //如果都写，则可以构造有参和无参,即构造函数重载

    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }

    //栈对象pt，在进行销毁时，会调用 析构函数
    //清理数据成员
    //也是编译器默认提供
    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt(2,3);//pt在栈区
    pt.print();
    //但不能 pt.Point();
    pt.~Point();
    pt.print();//说明默认提供的析构函数并没有什么用

    cout<<"~~~~~~~~~~~~~~~~~~~"<<endl;
    Point().print();
    //临时对象(匿名对象)，用完之后立马就被销毁了
    //生命周期只在本行，而不会到test结束
    cout<<"~~~~~~~~~~~~~~~~~~~"<<endl;
}

int main()
{   
    test();
    return 0;
}

