#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    
    //拷贝构造函数
    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"拷贝"<<endl;
    }


    void print(){
        cout<<'('<<_ix<<','<<_iy<<')'<<endl;
    }

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

    cout<<endl;
    Point pt2=pt1;//这里可以看见，没有初始化，说明有其他的默认函数,1
    cout<<"pt2=";
    pt2.print();

}

void func(Point pt){//当两个形参实参结合时，若两者都是类，调用拷贝函数,2
    cout<<"pt=";
    pt.print();
}

void test2(){
    Point pt2(3,4);
    func(pt2);

}

Point func2(){
    Point pt2(4,5);
    cout<<"pt2=";
    pt2.print();

    return pt2;//这里会拷贝一次,3
    //g++ 8_point.cc -fno-elide-constructors

}

void test3(){
    Point pt =func2();//这里用pt接受返回的类时，拷贝1，返回的类是临时对象，用完立马销毁
    cout<<"pt=";
    pt.print();
}

int main()
{   
    test();
    return 0;
}

