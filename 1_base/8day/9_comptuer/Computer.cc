#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

Computer::Computer(const char *brand, float price)
:_brand(new char[strlen(brand)+1]())//申请空间，避免越界
,_price(price)
{   
    strcpy(_brand,brand);//赋值
    cout<<"调用了构造函数，避开了set"<<endl;
}

Computer::Computer(const Computer &rhs)
//: _brand(rhs._brand) 浅拷贝
: _brand(new char[strlen(rhs._brand) +1 ]()) //深拷贝
, _price(rhs._price)
{
    strcpy(_brand,rhs._brand);
    cout<<"拷贝"<<endl;
}

#if 0
//方法一
Computer &Computer::operator=(const Computer &rhs){
    cout<<"赋值运算符函数"<<endl;
    
    //_brand=rhs._brand 浅拷贝不行

    //注意这里不同于拷贝构造函数，这里的_brand已经有指向，
    //所以不能乱改指向,不然原来指向的地方内存泄漏
    
    strcpy(_brand,rhs._brand);
    //这样表面上看起来没有问题，但是会发生内存踩踏，比如用huawei覆盖mac
    
    _price=rhs._price;

    return *this;
}
#endif

//方法二
Computer &Computer::operator=(const Computer &rhs){
    cout<<"赋值运算符函数"<<endl;
    if(this !=&rhs){
        //delete []this->_brand;
        delete []_brand;
        _brand = nullptr;
        _brand= new char[strlen(rhs._brand) + 1]();

        strcpy(_brand,rhs._brand);
        _price=rhs._price;
    }

    return *this;
}


void Computer:: print(){
    cout<<_brand<<endl;
    cout<<_price<<endl;
}

Computer::~Computer(){
    if(_brand){
        cout<<"默认提供的析构函数没啥用，要自己写释放,尤其是堆对象"<<endl;
        delete []_brand;
        _brand=nullptr;
    }
    else{
        cout<<"没有要释放的堆空间上的变量,走到这里说明二次释放"<<endl;
    }
    cout<<"已经销毁"<<endl;
}
