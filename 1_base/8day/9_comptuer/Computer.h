#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Computer
{

public:
    Computer(const char *brand, float price);
    Computer(const Computer &rhs);
    void print();
    Computer &operator=(const Computer &rhs);
    ~Computer();

private:
    //数据成员
    char *_brand;
    float _price;

};

#endif
