#include "Computer.h"
#include <iostream>

using std::cout;
using std::endl
;
void test(){
    Computer com("mac",5500);
    cout<<"com="<<endl;
    com.print();

    cout<<endl;
    Computer com2=com;
    cout<<"com2="<<endl;
    com2.print();
    //这里会两次释放同一个空间，所以要重写拷贝构造函数

    cout<<endl;
    Computer com3("huawei",4000);
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
    com3 = com;
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
    com3 = com3;//这很傻逼，自己赋给自己，但是说明赋值运算符函数还是不够健全，改！
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

