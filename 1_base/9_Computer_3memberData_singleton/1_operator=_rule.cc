#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout<<"拷贝"<<endl;
    }

    void print(){
        cout<<'('<<this->_ix<<','<<this->_iy<<')'<<endl;
    }


    //1、&最好不去掉，不然会rhs=pt1多一次拷贝构造函数
    //2、const不能去掉，不然pt2=Point(1,2)，左值不能赋给右值
    //3、&operator的&不能删掉，不然返回时会调用拷贝构造函数
    //4、返回类型不能是void，不然不支持 pt3=pt2=pt1
    Point &operator=(const Point &rhs){
        _ix=rhs._ix; 
        _iy=rhs._iy;

        return *this;
    }
    

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

    cout<<endl;
    Point pt2(3,4);
    pt2.print();

    cout<<endl;
    //pt2=pt1;
    pt2.operator=(pt1);//赋值运算符函数，效果等价于上一行
    cout<<"pt2=";
    pt2.print();
}

int main()
{   
    test();
    return 0;
}

