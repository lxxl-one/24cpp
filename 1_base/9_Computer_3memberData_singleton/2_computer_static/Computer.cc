#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

float Computer::_totalPrice=0;
//静态数据成员不能再初始化列表中初始化，必须在实现文件中初始化
//也不能在类的声明中初始化

Computer::Computer(const char *brand, float price)
:_brand(new char[strlen(brand)+1]())//申请空间，避免越界
,_price(price)
//, _totalPrice(0)
{   
    strcpy(_brand,brand);//赋值
    _totalPrice += _price;
    cout<<"调用了构造函数，避开了set"<<endl;
}

Computer::Computer(const Computer &rhs)
//: _brand(rhs._brand) 浅拷贝
: _brand(new char[strlen(rhs._brand) +1 ]()) //深拷贝
, _price(rhs._price)
{
    strcpy(_brand,rhs._brand);
    cout<<"拷贝"<<endl;
}

#if 0
//方法一
Computer &Computer::operator=(const Computer &rhs){
    cout<<"赋值运算符函数"<<endl;
    
    //_brand=rhs._brand 浅拷贝不行

    //注意这里不同于拷贝构造函数，这里的_brand已经有指向，
    //所以不能乱改指向,不然原来指向的地方内存泄漏
    
    strcpy(_brand,rhs._brand);
    //这样表面上看起来没有问题，但是会发生内存踩踏，比如用huawei覆盖mac
    
    _price=rhs._price;

    return *this;
}
#endif
//方法二
Computer &Computer::operator=(const Computer &rhs){
    cout<<"赋值运算符函数"<<endl;
    if(this !=&rhs){
        //delete []this->_brand;
        delete []_brand;
        _brand = nullptr;

        _brand= new char[strlen(rhs._brand) + 1]();
        strcpy(_brand,rhs._brand);
        _price=rhs._price;
    }

    return *this;
}

void Computer:: print(){
    cout<<"非const"<<endl;
    cout<<_brand<<endl;
    cout<<_price<<endl;
    
    //cout<<_totalPrice<<endl;
    //printTotalPrice();
    //非静态成员函数，可以访问静态成员
}

void Computer::print() const{
    cout<<"const"<<endl;
    cout<<_brand<<endl;
    cout<<_price<<endl;
    //它与print函数是函数重载，因为它的this指针是const Computer * const this
    //如果只能留一个，最好留const版本
}

Computer::~Computer(){
    if(_brand){
        cout<<"默认提供的析构函数没啥用，要自己写释放,尤其是堆对象"<<endl;
        delete []_brand;
        _brand=nullptr;
    }
    else{
        cout<<"没有要释放的堆空间上的变量,走到这里说明二次释放"<<endl;
    }
    cout<<"已经销毁"<<endl;
}

void Computer::printTotalPrice(){
    cout<<"_totalPrice="<<_totalPrice<<endl;
    //cout<<_price<<endl;
    //静态成员函数没有隐含的this指针，所以这里的_price不知道是谁的，除非this->_price
    //不能访问非静态数据成员
    //print(); 同理，不能访问非静态成员函数
    

    //如果非要访问非静态成员，可以printTotalPrice(传参)
}




