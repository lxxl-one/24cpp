#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Computer
{

public:
    Computer(const char *brand, float price);
    Computer(const Computer &rhs);
    void print();
    void print() const;
    Computer &operator=(const Computer &rhs);
    ~Computer();

    static void printTotalPrice();

   // static float _totalPrice;
   // 也可以写在public下，这样在外面可以 类名:: 的形式直接调用
private:
    //数据成员
    char *_brand;
    float _price;
    static float _totalPrice;
    //static float _totalPrice=5; 
    //静态数据成员的初始化不能在类的声明中进行,需要在类的声明外部进行初始化

    //静态数据成员，位于全局静态区，它的初始化不受private影响
    //不占用类中对象的大小(有点像成员函数)，但被所有这个类所创建的对象共享。

};

//float Computer::_totalPrice=0;//写在这里会被test.cc、Computer.cc定义两次
#endif
