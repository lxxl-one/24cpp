#include "Computer.h"
#include <iostream>

using std::cout;
using std::endl
;
void test(){
    Computer com("mac",5500);
    cout<<"com="<<endl;
    com.print();

    cout<<endl;
    Computer com2=com;
    cout<<"com2="<<endl;
    com2.print();
    //这里会两次释放同一个空间，所以要重写拷贝构造函数

    cout<<endl;
    Computer com3("huawei",4000);
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
    com3 = com;
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
    com3 = com3;//这很傻逼，自己赋给自己，但是说明赋值运算符函数还是不够健全，改！
    cout<<"com3="<<endl;
    com3.print();

    cout<<endl;
}

void test2(){
    //cout<<sizeof(Computer)<<endl;
    cout<<"购买之前的总价：";
    Computer::printTotalPrice();//类名+作用域，创建对象之前在外面调用静态函数
    cout<<endl;
    
    Computer com("mac",5500);
    cout<<"com="<<endl;
    com.print();
    cout<<"买第一台电脑时的总价:";
    com.printTotalPrice();

    cout<<endl;
    Computer com2("huawei",4000);
    cout<<"com2="<<endl;
    com2.print();
    cout<<"买第二台电脑时的总价:";
    com.printTotalPrice();
    //Computer::printTotalPrice()、com2.printTotalPrice()都可以
    cout<<endl;
    //cout<<Computer::_totalPrice<<endl;

}

void test3(){
    
    Computer com("mac",5500);
    cout<<"com="<<endl;
    com.print();
    //非const对象调用非const版本的成员函数
    //有谁就调用谁

    cout<<endl;
    const Computer com2("mac",5500);
    cout<<"com2="<<endl;
    com2.print();
    //const对象调用const版本成员函数
    //且在没有const函数情况下，不能调用非const函数


}

int main()
{   
    test2();
    return 0;
}

