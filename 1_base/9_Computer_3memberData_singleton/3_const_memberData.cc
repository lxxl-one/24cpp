#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    //常量数据成员，必须在构造函数的初始化列表中进行，之后不能赋值
    {
        cout<<"成功初始化"<<endl;
    }

    void print(){
        cout<<'('<<this->_ix<<','<<this->_iy<<')'<<endl;
    }

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    const int _ix;//常量数据成员，必须在构造函数的初始化列表中进行，之后不能赋值
    const int _iy;
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();

}

int main()
{   
    test();
    return 0;
}

