#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    , _iz(_ix) //引用数据成员也必须在创建时就初始化
    {
        cout<<"成功初始化"<<endl;
    }

    void setZ(int iz){
        _iz=iz;
    }

    void print(){
        cout<<'('<<this->_ix<<','<<this->_iy<<','<<_iz<<')'<<endl;
    }

    ~Point()
    {
        cout<<"已被销毁"<<endl;
    }

private:
    int _ix;
    const int _iy;//const数据成员
    int &_iz;//引用数据成员,本质是指针，占用8字节
};

void test(){
    Point pt1(1,2);
    cout<<"pt1=";
    pt1.print();
    cout<<endl;
    pt1.setZ(100);
    cout<<"pt1=";
    pt1.print();

    cout<<sizeof(Point)<<endl;//4+4+8=16
}

int main()
{   
    test();
    return 0;
}

