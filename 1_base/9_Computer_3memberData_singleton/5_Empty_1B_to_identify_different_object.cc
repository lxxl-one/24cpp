#include <iostream>

using std::cout;
using std::endl;

class Empty{
    //自动生成四个函数
    //无参构造函数
    //拷贝构造函数
    //赋值运算符函数
    //析构函数
};

void test(){
    cout<<sizeof(Empty)<<endl;
    Empty em1;
    Empty em2;
    printf("%p\n",&em1);
    printf("%p\n",&em2);
    //为了区分不同对象，有一字节的大小
}

int main()
{   
    test();
    return 0;
}

