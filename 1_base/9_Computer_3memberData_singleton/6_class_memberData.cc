#include <iostream>

using std::cout;
using std::endl;

class Point{

public:
    Point(int ix=0, int iy=0)
    : _ix(ix)
    , _iy(iy)
    {
        cout<<"成功初始化"<<endl;
    }

    void print(){
        cout<<'('<<this->_ix<<','<<this->_iy<<')';
    }

    ~Point()
    {
        cout<<"point已被销毁"<<endl;
    }

private:
    int _ix;
    int _iy;
};

class Line{
public:
    Line(int x1,int y1, int x2, int y2)
    : _pt1(x1,y1)   //类对象数据成员在此初始化
    , _pt2(x2,y2)   //哪怕不写这一行，也会调用无参构造函数
    {

    }

    ~Line(){
        cout<<"line已经销毁"<<endl;
    }

    void printLine(){
        _pt1.print();
        cout<<">>>>>>>>>";
        _pt2.print();
    }


    Point _pt1;//类对象数据成员
    Point _pt2;
};

void test(){

    Line line(1,2,3,4);
    //line._pt2=Point(5,6);
    cout<<"line:";
    line.printLine();
    cout<<endl;
}

int main()
{   
    test();
    return 0;
}

