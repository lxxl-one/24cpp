#include <iostream>
#include <string>
#include <stack>
#include <map>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::stack;
using std::map;

class Leetcode20{
public:
    bool isValid(const string &mystr){
        stack<char> st;
        map<char,char> match={ {'(',')'}, {'[',']'}, {'{', '}'} };
        
        for(size_t idx=0; idx<mystr.size(); ++idx){
            if( mystr[idx]=='(' || mystr[idx]=='[' || mystr[idx]=='{')
                st.push(mystr[idx]);
            else
            {
                if(st.empty()){
                //如果左括号入栈和判断同时进行，则判空必须先写
                //否则碰到右括号打头，则 match[st.top()]直接报错了
                    return false;
                }
                char mytop = st.top();
                st.pop();
                if( match[mytop] != mystr[idx] )
                    return false;
            }
        }

        return st.empty();
    }
};

int main()
{   
    //string mystr = "]({})";
    string mystr;
    cin >> mystr;
    cout<<Leetcode20().isValid(mystr)<<endl;
    return 0;
}

