#include "trans.h"
#include <sstream>

const string & transform(const unordered_map<string, string> &Map, const string & word){
    auto mapIt = Map.find(word);

    if(mapIt != Map.end())
    {
        return Map.find(word)->second;
    }
    else
        return word;
}


void trans(ifstream &readfile, ifstream &readmap){

    auto dict = buildMap(readmap);
    string line;
    string word;
    while(getline(readfile, line)){
        bool firstword = true;

        cout<<line<<endl;
        
        istringstream iss(line);
        while(iss >> word){
            if(firstword)
                firstword = false;
            else
                cout<<" ";
            cout<<transform(dict,word);
        }
        cout<<endl;
        cout<<"========================================="<<endl;
    }
}

const unordered_map<string,string> buildMap(ifstream &readmap){
    unordered_map<string, string> Map;
    string key;
    string value;

    while(readmap >> key && getline(readmap, value)){
        if(value.size() > 1)
            Map[key] = value.substr(1);
    }

    return Map;

}

