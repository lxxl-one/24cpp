#ifndef __TRANS_H__
#define __TRANS_H__

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

using namespace std;

void trans(ifstream &, ifstream &);
const unordered_map<string,string> buildMap(ifstream &);
const string & transform(const unordered_map<string,string> &, const string &);

#endif

