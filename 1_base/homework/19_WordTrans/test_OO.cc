#include <iostream>
#include <unordered_map>
#include <string>
#include <fstream>
#include <sstream>

using std::cout;
using std::endl;
using std::unordered_map;
using std::string;
using std::ifstream;
using std::istringstream;

class WordTrans
{
public:
    void builsMap(const string &filename)
    {
        ifstream ifs(filename);
        if(!ifs)
        {
            std::cerr << "open file " << filename << " error!" << endl;
            return;
        }

        string key;
        string word;
        while(ifs >> key && getline(ifs, word))
        {
            if(word.size() > 1)//判断空格
            {
                _um[key] = word.substr(1);//substr取子串
            }
        }

        ifs.close();
    }

    void trans(const string &filename)
    {
        ifstream ifs(filename);
        if(!ifs)
        {
            std::cerr << "open file " << filename << " error!" << endl;
            return;
        }

        string line;//接收一行的内容
        while(getline(ifs, line))
        {
            cout<<line<<endl;

            string word;
            istringstream iss(line);

            while(iss >> word)
            {
                //此处是不能直接使用下标的
                auto it = _um.find(word);
                if(it != _um.end())
                {
                    cout << it->second;
                }
                else
                {
                    cout << word;
                }
                cout << " ";
            }
            cout << endl;
            cout <<"======================================" <<endl;
        }

        ifs.close();

    }
private:
    unordered_map<string, string> _um;//存放map.txt中的内容
};

int main(int argc, char *argv[])
{
    WordTrans wt;
    wt.builsMap("map.txt");
    wt.trans("file.txt");
    return 0;
}
