#include <iostream>

using std::cout;
using std::endl;

class Stack
{
public:
    Stack(size_t size = 10);
    ~Stack();
    bool empty() const;
    bool full() const;
    void push(const int &value);
    void pop();
    int top();

private:
    int _top;//栈顶指针
    size_t _size;//栈中元素的个数
    int *_pdata;//存放栈中元素的指针
};

Stack::Stack(size_t size)
: _top(-1)
, _size(size)
, _pdata(new int[_size]())
{
    cout << "Stack(size_t)" << endl;
}

Stack::~Stack()
{
    cout << "~Stack()" << endl;
    if(_pdata)
    {
        delete [] _pdata;
        _pdata = nullptr;
    }
}

bool Stack::empty() const
{
    return (-1 == _top);
}

bool Stack::full() const
{
    return (_top == (int )(_size - 1));
}

void Stack::push(const int &value)
{
    //如果是满的，那就不能push
    if(!full())
    {
        _pdata[++_top] = value;
    }
    else
    {
        cout << "该栈是满的，不能进行push" << endl;
        return;
    }
}

void Stack::pop()
{
    if(!empty())
    {
        --_top;
    }
    else
    {
        cout << "该栈是空的，不能进行pop" << endl;
        return;
    }
}

int Stack::top()
{
    return _pdata[_top];
}

void test()
{
    Stack st(10);
    cout << "栈是不是空的 " << st.empty() << endl;
    st.push(1);
    cout << "栈是不是满的 " << st.full() << endl;

    cout << endl;
    for(size_t idx = 2; idx != 13; ++idx)
    {
        st.push(idx);
    }
    cout << "栈是不是满的 " << st.full() << endl;

    //可以用下标访问
    //cout<<st._pdata[5]<<endl;

    while(!st.empty())
    {
        cout << st.top() << "  ";
        st.pop();
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


