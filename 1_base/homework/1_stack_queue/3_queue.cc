#include <iostream>

using std::cout;
using std::endl;

//循环队列
//初始化两个都指向0
//front为当前队头，rear为当前插入位置（队尾后一个）
//容量为sz-1
class Queue
{
public:
    Queue(int size = 10)
    : _size(size)
    , _front(0)
    , _rear(0)
    , _data(new int[_size]())
    {
        cout << "Queue(int  = 10)" << endl;
    }

    bool full() const
    {
        return (_front == (_rear + 1) % _size);
    }

    bool empty() const
    {
        return _front == _rear;
    }

    void push(const int &value)
    {
        if(!full())
        {
            _data[_rear++] = value;
            _rear %= _size;
        }
        else
        {
            cout << "The queue is full" <<endl;
            return;
        }
    }

    void pop()
    {
        if(!empty())
        {
            ++_front;
            _front %= _size;
        }
        else
        {
            cout << "The queue is empty" << endl;
            return;
        }
    }

    int front()
    {
        return _data[_front];
    }

    int back()
    {
        return _data[(_rear - 1 + _size) % _size];
        //不用担心报错，因为已经初始化，不会是乱值
    }

    ~Queue()
    {
        cout << "~Queue()" <<endl;
        if(_data)
        {
            delete [] _data;
            _data = nullptr;
        }
    }
private:
    int _size;
    int _front;
    int _rear;
    int *_data;
};

int main(int argc, char **argv)
{
    Queue que;
    cout << "队列是不是空的？" << que.empty() << endl;
    que.push(1);
    cout << "队列是不是满的？" << que.full() << endl;

    for(int idx = 2; idx != 15; ++idx)
    {
        que.push(idx);
    }

    while(!que.empty())
    {
        cout << que.front() << endl;
        que.pop();
    }
    cout << "栈是不是空的？" << que.empty() << endl;
    return 0;
}


