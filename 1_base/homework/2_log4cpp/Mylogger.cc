#include "Mylogger.h"
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Priority.hh>
#include <iostream>

using std::cout;
using std::endl;
using namespace log4cpp;

Mylogger *Mylogger::_pInstance=nullptr;

Mylogger *Mylogger::getInstance(){
    if(nullptr== _pInstance){
        _pInstance = new Mylogger();
    }
    return _pInstance;
}

void Mylogger::destory(){
    if(_pInstance){
        delete _pInstance;
        _pInstance=nullptr;
    }
}

void Mylogger::warn(const char *msg){
    _root.warn(msg);
}
void Mylogger::error(const char *msg){
    _root.error(msg);
}
void Mylogger::debug(const char *msg){
    _root.debug(msg);
}
void Mylogger::info(const char *msg){
    _root.info(msg);
}
	
Mylogger::Mylogger()
: _root(Category::getRoot().getInstance("cat")) //获得名为cat的子记录器对象
{
    cout<<"Mylogger::Mylogger()"<<endl;
    
    //格式
    PatternLayout *ppl1=new PatternLayout();
    ppl1->setConversionPattern("%d [%p] %c %m %n");
    PatternLayout *ppl2=new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c %m %n");
    
    //目的地
    OstreamAppender *poa=new OstreamAppender("OstreamAppender",&cout);
    poa->setLayout(ppl1);
    FileAppender *pfl=new FileAppender("FileAppender","msg.txt");
    pfl->setLayout(ppl2);

    _root.addAppender(poa);
    _root.addAppender(pfl);
    
    //优先级
    _root.setPriority(Priority::DEBUG);
    
}

Mylogger::~Mylogger(){
    cout<<"Mylogger::~Mylogger()"<<endl;
    Category::shutdown();
}
