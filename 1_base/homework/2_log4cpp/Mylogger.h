#ifndef __MYLOGGER_H__
#define __MYLOGGER_H__

#include <log4cpp/Category.hh>

using namespace log4cpp;

//
class Mylogger
{
public:
    static Mylogger *getInstance();
    static void destory();
	void warn(const char *msg);
	void error(const char *msg);
	void debug(const char *msg);
	void info(const char *msg);
	
private:
	Mylogger();
	~Mylogger();
    static Mylogger *_pInstance;
    Category &_root;    //记录器是唯一的，作为单例模式的成员就能保证唯一
};
#endif
