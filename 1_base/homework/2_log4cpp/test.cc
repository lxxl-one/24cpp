#include <iostream>
#include "Mylogger.h"
#include <string>
#include <sstream>

using std::cout;
using std::endl;
using std::string;
using std::ostringstream;
using std::to_string;

string intToString(int value){
    ostringstream oss;
    oss<<value;
    return oss.str();
    //可以用这个把 int 转成 string
}

#define prefix(msg) (string(__FILE__)+string(": ") \
                + string(__FUNCTION__)+string(": ") \
                + string(to_string(__LINE__))+string(": ") \
                + msg).c_str()

string func(const string &msg){
    string tmp=string(__FILE__)+string(": ")
                + string(__FUNCTION__)+string(": ")
                + string(to_string(__LINE__))+string(": ")
                + msg;
    return tmp;
    //这里记录的3个宏都有问题，_
    //function_应该是test2，但是被记成了func，_line_是55，但被记成了27
    //使用inline也不能解决这个问题
    //只能使用宏定义
}

#define LogError(msg) Mylogger::getInstance()->error(prefix(msg))
#define LogInfo(msg) Mylogger::getInstance()->info(prefix(msg))
#define LogWarn(msg) Mylogger::getInstance()->warn(prefix(msg))
#define LogDebug(msg) Mylogger::getInstance()->debug(prefix(msg))

void test(){
    //只是最简单地记录，没有来自哪个文件、哪个文件的哪个函数、发生记录的行号
    Mylogger *log = Mylogger::getInstance();
    log->error("this is an error message");
    log->destory();
}

void test1(){
    //输出 文件、函数、行号的宏定义
    cout<<__FILE__<<"   "<<__FUNCTION__<<"  "<<__LINE__<<endl;
}

void test2(){
    Mylogger *log = Mylogger::getInstance();
    //log->error(func("this is an error message").c_str());   
    //调用func会导致函数、行号不对
    
    log->error(prefix("this is an error message"));   
    //实现文件里，error的参数msg是c风格
    log->destory();
}

void test3(){
    LogError("This is wangdao error");
    LogInfo("This is wangdao info");
    LogWarn("This is wangdao warn");
    LogDebug("This is wangdao debug");
    //但这里有一个问题：单例模式没有释放
}

int main()
{   
    test3();
    return 0;
}

