#include <iostream>
#include "tinyxml2.h"
#include <regex>
#include <string>

using namespace tinyxml2;
using std::cout;
using std::endl;
using std::cerr;
using std::regex;
using std::string;

void test(){

    XMLDocument doc;
    doc.LoadFile("coolshell.xml");
    if(doc.ErrorID()){
        cerr<<"error"<<endl;
        return;
    }

    XMLElement *pNode=doc.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("item");
    if(pNode){
        XMLElement *title = pNode->FirstChildElement("title");
        XMLElement *link = pNode->FirstChildElement("link");
        XMLElement *description = pNode->FirstChildElement("description");
        XMLElement *con = pNode->FirstChildElement("content:encoded");

        if(title){
            const char *ptitle=title->GetText();
            cout<<"title="<<ptitle<<endl;
        }
        else
            cout<<"nullptr == "<<endl;
    
        if(link){
            const char *plink=link->GetText();
            cout<<"link="<<plink<<endl;
        }
        else
            cout<<"nullptr == "<<endl;
    
        const char *pdescription=nullptr;
        if(description){
            pdescription=description->GetText();
            //cout<<"description="<<pdescription<<endl;
        }
        else
            cout<<"nullptr == "<<endl;
    
        if(con){
            const char *pcon=con->GetText();
            //cout<<"content:encoded="<<pcon<<endl;
        }
        else
            cout<<"nullptr == "<<endl;

        regex reg("<[^>]+>");
        string msg=regex_replace(string(pdescription),reg," ");
        cout<<msg<<endl;
    }
}

int main()
{   
    test();
    return 0;
}

