#include <iostream>
#include "tinyxml2.h"
#include <regex>
#include <string>

using namespace tinyxml2;
using std::cout;
using std::endl;
using std::cerr;
using std::regex;
using std::string;

void test(){

    XMLDocument doc;
    doc.LoadFile("coolshell.xml");
    if(doc.ErrorID()){
        cerr<<"error"<<endl;
        return;
    }

    XMLElement *pNode=doc.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("item");
    while(pNode){
        string title = pNode->FirstChildElement("title")->GetText();
        string link = pNode->FirstChildElement("link")->GetText();
        string description = pNode->FirstChildElement("description")->GetText();
        string con = pNode->FirstChildElement("content:encoded")->GetText();

        regex reg("<[^>]+>");
        title = regex_replace(title,reg," ");
        link = regex_replace(link,reg," ");
        description = regex_replace(description,reg," ");
        con = regex_replace(con,reg," ");
    
        cout<<"title = "<<title<<endl;
        cout<<"link = "<<link<<endl;
        cout<<"description = "<<description<<endl;
        cout<<"content = "<<con<<endl;

        //处理下一篇
        cout<<endl<<endl;
        pNode = pNode->NextSiblingElement("item");
    }
}

int main()
{   
    test();
    return 0;
}

