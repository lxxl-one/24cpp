#include "TcpServer.h"
#include "TcpConnection.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;

//连接建立做的事件
void onConnection(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has connected2!!" << endl; 
}

//消息到达做的事件
void onMessage(const TcpConnectionPtr &con)
{
    //接收客户端的数据
    string msg = con->receive();
    cout << ">>recv msg from client : " << msg << endl;

    //将数据发送给客户端
    con->send(msg);
}

//连接断开做的事件
void onClose(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has closed!!!" << endl; 
}

void test()
{
    TcpServer server("127.0.0.1", 8888);
    server.setAllCallback(std::move(onConnection), 
                          std::move(onMessage), 
                          std::move(onClose));
    server.start();

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

