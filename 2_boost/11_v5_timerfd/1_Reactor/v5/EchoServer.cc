#include "EchoServer.h"
#include "TcpConnection.h"
#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

MyTask::MyTask(const string &msg, const TcpConnectionPtr &con)
: _msg(msg)
, _con(con)
{

}

void MyTask::process()
{
    _con->sendInLoop(_msg);
}

EchoServer::EchoServer(size_t threadNum, size_t queSize
                       , const string &ip, unsigned short port)
: _pool(threadNum, queSize)
, _server(ip, port)
{
}

EchoServer::~EchoServer()
{

}

//服务器的启动与停止
void EchoServer::start()
{
    _pool.start();//让线程池启动

    using namespace std::placeholders;
    //void(EchoServer *, const TcpConnectionPtr &)
    //void(const TcpConnectionPtr &)
    _server.setAllCallback(bind(&EchoServer::onConnection, this, _1)
                           , bind(&EchoServer::onMessage, this, _1)
                           , bind(&EchoServer::onClose, this, _1));

    _server.start();//让EventLoop启动
}

void EchoServer::stop()
{
    _pool.stop();
    _server.stop();
}

//三个回调函数
void EchoServer::onConnection(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has connected!!" << endl; 
}

void EchoServer::onMessage(const TcpConnectionPtr &con)
{
    //接收客户端的数据
    string msg = con->receive();//读网络数据,Read,Input
    cout << ">>recv msg from client : " << msg << endl;

    MyTask task(msg, con);
    _pool.addTask(std::bind(&MyTask::process, task));
}

void EchoServer::onClose(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has closed!!!" << endl; 
}
