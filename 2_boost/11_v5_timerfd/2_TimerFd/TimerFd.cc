#include "TimerFd.h"
#include <sys/timerfd.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <iostream>

using std::cout;
using std::endl;

TimerFd::TimerFd(TimerFdCallback &&cb, int initSec, int peridocSec)
: _timerfd(createTimerFd())
, _cb(std::move(cb))//回调函数的注册
, _isStarted(false)
, _initSec(initSec)
, _peridocSec(peridocSec)
{

}

TimerFd::~TimerFd()
{
    if(_isStarted)
    {
        //需要将定时器关掉
        setTimerFd(0, 0);
        close(_timerfd);
    }
}

//启动与停止
void TimerFd::start()
{
    struct pollfd pfd;
    pfd.fd = _timerfd;//监听的文件描述符
    pfd.events = POLLIN;//监听的读事件

    //设置定时器
    setTimerFd(_initSec, _peridocSec);

    _isStarted = true;
    while(_isStarted)
    {
        //使用poll进行监听
        int nready = poll(&pfd, 1, 3000);
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            perror("poll error");
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll time_out" << endl;
        }
        else
        {
            //查看一下是不是读事件
            if(pfd.revents & POLLIN)
            {
                //阻塞等待被唤醒
                handleRead();
                if(_cb)
                {
                    _cb();//回调函数的执行
                }
            }
        }
    }
}

void TimerFd::stop()
{
    if(_isStarted)
    {
        _isStarted = false;
        setTimerFd(0, 0);
    }
}

//创建用于通信的文件描述符
int TimerFd::createTimerFd()
{
    int fd = timerfd_create(CLOCK_REALTIME, 0);
    if(fd < 0)
    {
        perror("timerfd_create");
        return -1;
    }
    return fd;
}

//阻塞等待（也就是执行read）
void TimerFd::handleRead()
{
    uint64_t two = 0;
    ssize_t ret = read(_timerfd, &two, sizeof(uint64_t));
    if(ret != sizeof(uint64_t))
    {
        perror("read");
        return;
    }
}

//设置定时器
void TimerFd::setTimerFd(int initSec, int peridocSec)
{
    struct itimerspec newValue;
    newValue.it_value.tv_sec = initSec;//初始的秒数
    newValue.it_value.tv_nsec = 0;

    newValue.it_interval.tv_sec = peridocSec;//周期性的时间
    newValue.it_interval.tv_nsec = 0;

    int ret = timerfd_settime(_timerfd, 0, &newValue, nullptr);
    if(ret < 0)
    {
        perror("timerfd_settime");
        return;
    }
}
