#include "TimerFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;
using std::bind;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask process is running!!!" << endl;
    }
};

void test()
{
    MyTask task;
    TimerFd tfd(bind(&MyTask::process, &task), 4, 2);
    Thread th(bind(&TimerFd::start, &tfd));//就让handleRead放在了子线程中
    th.start();//将子线程启动起来,也就是会执行TimerFd中的start函数

    //主线程在此处睡眠
    sleep(30);

    tfd.stop();
    th.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

