#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include <iostream>
using std::cout;
using std::endl;

int gNumber = 1000;
__thread int number = 1;//线程局部存储的变量
/* int number = 1; */
 
void * threadFunc1(void * arg)
{
	//结论: errno并不是一个全局变量
	number = 10;
	printf("thread1     thread_id: %lu, &errno: %p, &number: %p, number: %d, &gNumber: %p, gNumber: %d\n\n", 
		   pthread_self(), &errno, &number, number, &gNumber, gNumber);

	return nullptr;
}
 
void * threadFunc2(void * arg)
{
	number = 20;
	printf("thread2     thread_id: %lu, &errno: %p, &number: %p, number: %d, &gNumber: %p, gNumber: %d\n\n", 
		   pthread_self(), &errno, &number, number, &gNumber, gNumber);

	return nullptr;
}

int main(void)
{
	printf("main_thread thread_id: %lu, &errno: %p, &number: %p, number: %d, &gNumber: %p, gNumber: %d\n\n", 
			pthread_self(), &errno, &number, number, &gNumber, gNumber);

	pthread_t thid1, thid2;
	pthread_create(&thid1, nullptr, threadFunc1, nullptr);
	pthread_create(&thid2, nullptr, threadFunc2, nullptr);

	pthread_join(thid1, nullptr);
	pthread_join(thid2, nullptr);

	return 0;
} 
