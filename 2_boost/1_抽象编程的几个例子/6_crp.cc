#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

//关联 替代 继承，
//降低了个人与车之间的耦合关系

class Vehicle
{
public:
    virtual void run() = 0;
    virtual ~Vehicle() {}
};

class Tesla
: public Vehicle
{
public:
    void run() override
    {
        cout << "Model Y start..." << endl;
    }
};

class BYD
: public Vehicle
{
public:
    void run() override
    {
        cout << "汉EV start..." << endl;
    }
};

class Geely
: public Vehicle
{
public:
    void run() override
    {
        cout << "Geely LYNK03 start..." << endl;
    }
};

class Person
{
public:
    //这个人想要什么车
    void getVehicle(Vehicle *vehicle)//基类指针指向派生类对象。
    {
        _vehicle = vehicle;
    }

    //拿到车之后开走
    void drive()
    {
        _vehicle->run();
    }
private:
    Vehicle *_vehicle;
};

void test()
{
    Person person;
    unique_ptr<Vehicle> tesla(new Tesla());
    unique_ptr<Vehicle> han(new BYD());
    unique_ptr<Vehicle> geely(new Geely());

    person.getVehicle(tesla.get());
    person.drive();

    person.getVehicle(han.get());
    person.drive();

    person.getVehicle(geely.get());
    person.drive();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}


