#include <iostream>

using std::cout;
using std::endl;

class Point{
public:
    Point(int x, int y)
    : _ix(x)
    , _iy(y)
    {

    }

    void print(){
        cout<<"("<<_ix<<","<<_iy<<")";
    }
private:
    int _ix;
    int _iy;
};

class Line{
public:
    Line(int x1,int y1,int x2, int y2)
    : _pt1(x1,y1)
    , _pt2(x2,y2)
    {

    }

    void print(){
        _pt1.print();
        cout<<"->";
        _pt2.print();
        cout<<endl;
    }

private:
    Point _pt1;
    Point _pt2;
};

void test(){
    Line line(1,2,3,4);
    line.print();
}

int main()
{   
    test();
    return 0;
}

