#include "line.h"
#include <iostream>

using std::cout;
using std::endl;

/*
设计模式之 Pimpl
PIMPL （ Private Implementation 或 Pointer to Implementation ）
是通过一个私有的成员指针，将指针所指向的类的内部实现数据进行隐藏。
PIMPL 又称作“编译防火墙”，它的实现中就用到了嵌套类。
PIMPL 设计模式有如下优点：
1. 提高编译速度；
2. 实现信息隐藏；
3. 减小编译依赖，可以用最小的代价平滑的升级库文件；
4. 接口与实现进行解耦；
5. 移动语义友好。
6. 降低Line和Point的耦合程度，解耦
*/

//把前向声明的类实现出来
class Line::LineImpl{
public:
    LineImpl(int x1, int y1, int x2, int y2);
    ~LineImpl();
    void printLine() const;
private:
    class Point
    {
    public:
        Point(int x = 0, int y = 0)
        : _x(x), _y(y)
        {

        }
        void print() const;
    private:
        int _x;
        int _y;
    };
private:
    Point _pt1;
    Point _pt2;
};

Line::LineImpl::LineImpl(int x1, int y1, int x2, int y2)
: _pt1(x1, y1)
, _pt2(x2, y2)
{

}

Line::LineImpl::~LineImpl()
{
    cout<<"~LineImpl()"<<endl;
}

void Line::LineImpl::printLine() const
{
    _pt1.print();
    cout << " ---> ";
    _pt2.print();
    cout << endl;
}

Line::Line(int x1, int y1, int x2, int y2)
: _pimpl(new LineImpl(x1, y1, x2, y2))
{

}

Line::~Line() 
{ 
    delete _pimpl; 
    cout<<"~Line()"<<endl;
}

void Line::printLine() const
{
    _pimpl->printLine();
}

void Line::LineImpl::Point::print() const
{
    cout<<"("<<_x<<","<<_y<<")";
}


