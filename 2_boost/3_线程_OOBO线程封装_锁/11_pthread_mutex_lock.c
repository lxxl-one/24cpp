#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
        exit(1); \
    } \
}while(0);

int main(int argc, char *argv[])
{
    pthread_mutex_t mutex;//创建互斥锁变量
    int ret = pthread_mutex_init(&mutex, NULL);
    ERROR_CHECK(ret, "pthread_mutex_init");
    
    //上锁
    ret = pthread_mutex_lock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_lock");
    
    //如果锁处于上锁状态，那么是不能进行销毁的
    ret = pthread_mutex_destroy(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_destroy");
    return 0;
}

