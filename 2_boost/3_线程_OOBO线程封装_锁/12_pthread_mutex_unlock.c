#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
        exit(1); \
    } \
}while(0);

int main(int argc, char *argv[])
{
    pthread_mutex_t mutex;//创建互斥锁变量
    int ret = pthread_mutex_init(&mutex, NULL);
    ERROR_CHECK(ret, "pthread_mutex_init");

    //上锁
    ret = pthread_mutex_lock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_lock");

    //解锁
    ret = pthread_mutex_unlock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_unlock");
    
    //销毁锁的时候，锁肯定是没有上锁的
    ret = pthread_mutex_destroy(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_destroy");
    return 0;
}

