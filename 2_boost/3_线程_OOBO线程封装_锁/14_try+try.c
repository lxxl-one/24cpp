#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
        exit(1); \
    } \
}while(0);

int main(int argc, char *argv[])
{
    pthread_mutex_t mutex;//创建互斥锁变量
    int ret = pthread_mutex_init(&mutex, NULL);
    ERROR_CHECK(ret, "pthread_mutex_init");
    
    //尝试上锁
    ret = pthread_mutex_trylock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock1");

    //连续两次尝试上锁，也就是执行pthread_mutex_trylock两次
    //会导致第二次上锁不成功，直接报错，退出
    //不会和lock一样出现阻塞
    printf("begin\n");
    ret = pthread_mutex_trylock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock2");
    printf("end\n");

    //解锁
    ret = pthread_mutex_unlock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_unlock");
    
    //进行销毁锁的时候，锁肯定是没有上锁的
    ret = pthread_mutex_destroy(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_destroy");
    return 0;
}

