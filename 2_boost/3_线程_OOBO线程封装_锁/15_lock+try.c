#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

//上锁和解锁一定要成对出现
//多次上锁时，后面可以采用尝试上锁，但也要注意解锁
//锁只有上锁和解锁状态，只有解锁状态才能销毁
//init、lock、unlock、destroy、trylock返回类型都是int，0是成功，其他是失败码

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
    } \
}while(0);

int main(int argc, char *argv[])
{
    pthread_mutex_t mutex;//创建互斥锁变量
    int ret = pthread_mutex_init(&mutex, NULL);
    ERROR_CHECK(ret, "pthread_mutex_init");
    
    //上锁
    ret = pthread_mutex_lock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock1");

    //直接上锁与尝试上锁，即pthread_mutex_lock与pthread_mutex_trylock不一样，
    //两次执行lock会让程序处于阻塞状态，函数调用并不会有返回结果；
    //但是使用尝试上锁函数trylock的时候，如果上锁不成功会立马返回，
    //不会有任何的影响，相当于此次尝试上锁没有执行
    printf("begin\n");
    ret = pthread_mutex_trylock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock2");
    printf("end\n");

    //解锁
    ret = pthread_mutex_unlock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_unlock");
    printf("end222\n");
    
    //进行销毁锁的时候，锁肯定是没有上锁的
    ret = pthread_mutex_destroy(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_destroy");
    printf("end333\n");
    return 0;
}

