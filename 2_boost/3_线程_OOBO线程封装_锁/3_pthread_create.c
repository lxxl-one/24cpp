#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

//用宏来省略下面的代码
//do while保证执行一次，看起来不突兀
//宏的语句后面加上 \
//编译时最后要加上 -lpthread

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret!=0) \
    {\
        printf("%s : %s\n", funcName, strerror(ret)); \
        exit(1); \
    }\
}while(0);

void *threadFunc(void *arg)
{
    //arg接受传进来的number，所以这个参数列表一定为void*，而不为空
    //如果传进来的参数很多，则可以写成struct打包传进来，再解包
    
    printf("threadFunc thread\n");
    int *pInt=(int *)arg;
    printf("*pInt = %d\n",*pInt);

    return NULL;
}

int main()
{
    pthread_t thid;
    int number=100;
    //int ret=pthread_create(&thid, NULL, threadFunc, NULL);
    int ret=pthread_create(&thid, NULL, threadFunc, &number);
/*
    int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg);

    pthread_t 是 POSIX 线程库中用于表示线程的数据类型，存储线程的标识符。
    可以将 pthread_t 视为一个抽象的线程标识符类型，
   
    在 POSIX 线程库中，pthread_t 被定义为一个结构体或整数类型，
    在一些系统中，pthread_t 可能被定义为一个结构体，它包含了线程的一些属性和标识信息。
    而在其他系统中，pthread_t 可能只是一个整数类型，用来表示线程的标识符。
    具体取决于不同的系统和编译器实现。
    无论是结构体还是整数类型，pthread_t 都是用来唯一标识一个线程的数据类型，
    开发者通常不需要了解其具体实现细节，只需要将其用作线程的标识符即可。
    
    通过 pthread_create 函数创建线程时，会将新线程的标识符存储在相应的 pthread_t 变量中，
    然后可以使用该变量来操作和控制线程，比如等待线程结束、终止线程等。

*/
    ERROR_CHECK(ret, "pthread_create");

/*
    if(ret){
        strerror(ret);
        return -1;
    }
*/

    usleep(100);   //让主线程睡觉，将cpu交给子线程
    //线程的执行是随机的，可能主线程已经结束了，子线程还没执行，所以子线程那句没有打印
    printf("main thread\n");
    return 0;
}

