#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret!=0) \
    {\
        printf("%s : %s\n", funcName, strerror(ret)); \
        exit(1); \
    }\
}while(0);

void *threadFunc(void *arg)
{
    printf("threadFunc thread\n");
    int *pInt=(int *)arg;
    printf("*pInt = %d\n",*pInt);

    pthread_exit(NULL); //让子线程主动退出
}

int main()
{
    pthread_t thid;
    int number=100;
    int ret=pthread_create(&thid, NULL, threadFunc, &number);
    ERROR_CHECK(ret, "pthread_create");

    usleep(100);
    printf("main thread\n");
    return 0;
}

