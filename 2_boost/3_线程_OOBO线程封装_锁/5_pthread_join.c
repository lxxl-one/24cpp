#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>


#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret!=0) \
    {\
        printf("%s : %s\n", funcName, strerror(ret)); \
        exit(1); \
    }\
}while(0);

void *threadFunc(void *arg)
{
    printf("threadFunc thread\n");
    int *pInt=(int *)arg;
    printf("*pInt = %d\n",*pInt);

    pthread_exit(NULL); //让子线程主动退出
}

int main()
{
    pthread_t thid;
    int number=100;
    int ret=pthread_create(&thid, NULL, threadFunc, &number);
    ERROR_CHECK(ret, "pthread_create");

    printf("main thread\n");    //主线程先抢到cpu
    
    ret = pthread_join(thid,NULL);  //让主线程阻塞等待thid这个线程执行结束

    //int pthread_join(pthread_t thread, void **retval);
    //retval 是一个指向指针的指针，用于获取被等待线程的返回值。
    //被等待线程的返回值会通过这个指针来传递给调用线程。
    //如果不需要获取被等待线程的返回值，可以传入 NULL。
    
    ERROR_CHECK(ret,"pthread_join");
    return 0;
}

