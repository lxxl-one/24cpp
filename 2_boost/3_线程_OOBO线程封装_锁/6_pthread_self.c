#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>


#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret!=0) \
    {\
        printf("%s : %s\n", funcName, strerror(ret)); \
        exit(1); \
    }\
}while(0);

void *threadFunc(void *arg)
{
    printf("threadFunc thread\n");
    int *pInt=(int *)arg;
    printf("*pInt = %d\n",*pInt);

    printf("threadFunc pthread id: %ld\n",pthread_self());
    //调用的都是self函数，但是主线程子线程得到的id不同

    pthread_exit(NULL); //让子线程主动退出
}

int main()
{
    pthread_t thid;
    int number=100;
    int ret=pthread_create(&thid, NULL, threadFunc, &number);
    ERROR_CHECK(ret, "pthread_create");

    printf("threadFunc pthread id: %ld\n", thid);   //这个就一样了

    printf("main thread\n");    //主线程先抢到cpu
    printf("main thread id: %ld\n",pthread_self());
    
    ret = pthread_join(thid,NULL);  //让主线程阻塞等待thid这个线程执行结束
    ERROR_CHECK(ret,"pthread_join");
    
    return 0;
}

