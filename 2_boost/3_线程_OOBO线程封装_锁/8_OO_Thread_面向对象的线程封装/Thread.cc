#include "Thread.h"
#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;

Thread::Thread()
: _thid(0)
, _isRunning(false)
{
    //构造函数的参数列表中不能create，因为此时还没有_thid
    
    //在这里为什么不create呢？
    //注意看test文件：
    //MyThread mth;//栈对象
    //mth.start();
    
    //相当于此时刚刚开辟了一个新线程，但我并不需要马上把它跑起来
    //当需要它跑时，才start，进而才create，且之后抢到cpu了才执行入口函数Func
}

Thread::~Thread()
{

}

//线程的开始
void Thread::start()
{
    //threadFunc必须是返回类型是void *,参数也是void *
    //现在隐含了一个this指针，那么就把Func设为静态
    //设为静态之后，因为没有this，Func就不能执行类体内的函数
    //需要从外面即create时就把this传给入口函数
    
    //int ret = pthread_create(&_thid, nullptr, threadFunc, nullptr);
    int ret = pthread_create(&_thid, nullptr, threadFunc, this);
    if(ret)
    {
        perror("pthread_create 出错");   //也是打印错误信息的
        return;
    }

    _isRunning = true;//线程已经开始运行了
}

//线程的结束
void Thread::stop()
{
    if(_isRunning)
    {
        int ret = pthread_join(_thid, nullptr);
        if(ret)
        {
            perror("pthread_join");
            return;
        }

        _isRunning = false;//能走到此处，表明线程已经结束了
    }
}

//线程的入口函数
void *Thread::threadFunc(void *arg)
{
    //run();  //报错，静态的Func没有this，不能this->run()

    Thread *pth = static_cast<Thread *>(arg);
    //Thread *pth = arg = this = &mth
    //this是start的this，mth调用的start，所以基类指针pth指向派生类对象this(即mth)
    
    if(pth)
    {
        //让线程执行任务
        pth->run();//体现了多态
    }
    else
    {
        cout << "nullptr == pth，不能调用run函数" << endl;
    }

    /* return nullptr; */
    pthread_exit(nullptr);
}

