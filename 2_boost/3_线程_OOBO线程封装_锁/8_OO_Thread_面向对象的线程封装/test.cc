#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyThread
: public Thread
{
public:
    void run() override
    {
        //派生了的run方法具体做什么？
        while(1)
        {
            cout << "MyThread is running!!!" << endl;
            sleep(1);
            //pthread_exit(nullptr);
        }
    }
};

void test()
{
    MyThread mth;//开辟新线程，栈对象
    mth.start();
    mth.stop(); 
    //注意，不是mth调用了join，是main调用了mth.stop()里的join，相当于main调用了join
    //如果不加stop这句，则和前面一样，主线程执行完直接结束了，不会执行子线程
}

void test2()
{
    //MyThread *ptr = new MyThread();
    unique_ptr<Thread> pth(new MyThread()); //这里直接用基类指针指向派生对象
    pth->start();
    pth->stop();

    //delete ptr;
    //ptr = nullptr;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

