#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::bind;

class MyTask
{
public:
    void process()//实现回调函数 
    {
        while(1)
        {
            cout << "MyTask is processning!!!" << endl;
            sleep(1);
        }
    }
};

void test()
{
    // unique_ptr<MyTask> pth(new MyTask()); 
    //Thread th(bind(&MyTask::process, pth));
    MyTask task;
    
    Thread th(bind(&MyTask::process, &task));

    th.start();//让线程运行起来
    th.stop();//让主线程等待子线程的退出
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}


