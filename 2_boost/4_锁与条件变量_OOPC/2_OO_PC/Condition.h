#ifndef __CONDITION_H__
#define __CONDITION_H__

//a.h(包含了b.h) a.cc   
//b.h(包含了a.h) b.cc  
//则会造成死循环
// #include "MutexLock.h"
#include <pthread.h>

class MutexLock;//建议前向声明

class Condition
{
public:
    Condition(MutexLock &mutex);    //传入的是锁对象，不是真正的锁
    ~Condition();

    //等待函数
    void wait();
    //发通知
    void notify();
    void notifyAll();

private:
    MutexLock &_mutex;//互斥锁对象的引用
    //注意这个不是pthread_mutex_t的互斥锁，真正的互斥锁在对象里面
    pthread_cond_t _cond;//条件变量

};

#endif
