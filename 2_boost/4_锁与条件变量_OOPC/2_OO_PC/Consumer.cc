#include "Consumer.h"
#include "TaskQueue.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Consumer::Consumer(TaskQueue &taskQue)
: _taskQue(taskQue)
{

}

Consumer::~Consumer()
{

}

void Consumer::run() 
{
    //和生产者保持一致，我也拿20个。最终生产一个消费一个，生产一个消费一个
    size_t cnt = 20;
    while(cnt--)
    {
        //消费数据
        int number = _taskQue.pop();
        cout << ">>Consumer number = " << number << endl;
        sleep(1);
    }
}
