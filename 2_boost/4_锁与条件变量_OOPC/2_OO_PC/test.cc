#include "Producer.h"
#include "Consumer.h"
#include "TaskQueue.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

//3个线程：main、生产者、消费者

void test()
{
    TaskQueue  taskQue(10);//创建了唯一的TaskQueue对象，仓库大小为10
    unique_ptr<Thread> pro(new Producer(taskQue));//把唯一的TaskQueue对象传入
    unique_ptr<Thread> con(new Consumer(taskQue));//同时有基类指针指向派生类对象

    //生产者与消费者启动
    //这里有个问题，如果是生产一个消费一个，那么push、pop中的wait、唤醒是否起作用？
    pro->start();
    con->start();

    //让主线程等待子线程的退出
    //这个stop和前面一样，是主线程等待子线程，不是让子线程停下
    pro->stop();
    con->stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

