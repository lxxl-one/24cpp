#include "Thread.h"
#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;

Thread::Thread()
: _thid(0)
, _isRunning(false)
{

}

Thread::~Thread()
{

}

//线程的开始
void Thread::start()
{
    //threadFunc必须是返回类型是void *,参数也是void *
    //现在隐含了一个this指针
    /* int ret = pthread_create(&_thid, nullptr, threadFunc, nullptr); */
    int ret = pthread_create(&_thid, nullptr, threadFunc, this);
    if(ret)
    {
        perror("pthread_create");
        return;
    }

    _isRunning = true;//标识线程已经开始运行了
}

//线程的结束
void Thread::stop()
{
    if(_isRunning)
    {
        int ret = pthread_join(_thid, nullptr);
        if(ret)
        {
            perror("pthread_join");
            return;
        }

        _isRunning = false;//能走到此处，表明线程已经结束了
    }
}

//线程的入口函数
void *Thread::threadFunc(void *arg)
{
    //Thread *pth = arg = this = &mth
    Thread *pth = static_cast<Thread *>(arg);
    if(pth)
    {
        //让线程执行任务
        pth->run();//体现了多态
    }
    else
    {
        cout << "nullptr == pth" << endl;
    }

    /* return nullptr; */
    pthread_exit(nullptr);
}

