#include "Producer.h"
#include "Consumer.h"
#include "TaskQueue.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue  taskQue(10);//创建了唯一的TaskQueue对象
    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    //生产者与消费者启动
    pro->start();
    con->start();

    //让主线程等待子线程的退出
    pro->stop();
    con->stop();
}

void test2()
{
    //锁、条件变量、线程，不能进行复制与赋值
    //但是不能设为单例模式，因为可能A和B有锁，C和D也有锁
    MutexLock mutex1;

    /* MutexLock mutex2 = mutex1;//复制,error */

    MutexLock mutex3;
    /* mutex3 = mutex1;//赋值,error */
}

int main()
{
    test();
    return 0;
}

