#include "TaskQueue.h"
    
TaskQueue::TaskQueue(size_t queSize)
: _queSize(queSize)
, _que()
, _mutex()
, _notEmpty(_mutex)
, _notFull(_mutex)
, _flag(true)
{

}

TaskQueue::~TaskQueue()
{

}

//任务队列空与满
bool TaskQueue::empty() const
{
    return _que.size() == 0;
}

bool TaskQueue::full() const
{
    return _que.size() == _queSize;
}

//向任务队列中存数据
void TaskQueue::push(ElemType ptask)
{
    MutexLockGuard autoLock(_mutex);//栈对象

    while(full())
    {
        _notFull.wait();
    }

    _que.push(ptask);

    _notEmpty.notify();
}

//从任务队列中取数据
/* TaskQueue::ElemType TaskQueue::pop() */
ElemType TaskQueue::pop()
{
    MutexLockGuard autoLock(_mutex);//栈对象

    //bool flag = true; 要跨函数使用，所以设为数据成员，而不是局部变量
    while(empty() && _flag)
    //这个flag不能影响以前的所有操作，只有当确定要退出线程池时，才设为false不再让睡觉
    {
        _notEmpty.wait();
    }

    if(_flag)
    {
        ElemType tmp = _que.front();
        _que.pop();

        _notFull.notify();

        return tmp;
    }
    else
    {
        //不再让睡觉说明队列已空，所以不返回不唤醒
        return nullptr;
    }
}

void TaskQueue::wakeup()
{
    _flag = false;
    _notEmpty.notifyAll();
}


