#include "WorkThread.h"
#include "ThreadPool.h"

WorkThread::WorkThread(ThreadPool &pool)
: _pool(pool)
{
}

WorkThread::~WorkThread()
{

}

void WorkThread::run() 
{
    //这就是线程池交给工作线程做的任务
    //工作线程调用_pool的doTask，所以在Pool.h中，把WorkThread设为友元
    _pool.doTask();
}
