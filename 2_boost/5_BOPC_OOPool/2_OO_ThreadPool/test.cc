#include "Task.h"
#include "ThreadPool.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
: public Task
{
public:
    void process() override
    {
        //实现MyTask具体逻辑即可
        ::srand(::clock());
        int number = ::rand() % 100;
        cout << "number = " << number << endl;
    }
};
//这里其实可以写很多个Mytask，作为不同的任务传入任务队列中
//比如生产者、消费者就是两个不同的任务，这里为了简化，只写了这一个任务

//1、
//Q：任务addTask可以添加20次，但是任务并没有执行20次，因为number的打印没有20次
//A：任务刚添加进去还没有执行完，就调用pool.stop()，而stop函数中将_isExit设置为了true，
//  导致在doTask函数中的while循环进不去，也就是任务还没有全拿走执行完，就强制回收了工作线程
//R：在stop函数中，如果任务队列上的任务没有被拿完，就先睡觉等待（是主线程main睡觉）

//2、现在已经可以确保任务全部执行完，但是线程池无法退出了
void test()
{
    //Task *ptask = new MyTask();
    unique_ptr<Task> ptask(new MyTask());   //基类指针指向派生对象
    ThreadPool pool(4, 10);

    //应该是先启动线程池中的工作线程，让工作线程等任务
    pool.start();

    size_t cnt = 20;
    //准确来说，并没有20个任务，
    //而是只有一个任务（对象），添加了20次
    //它的process当做任务执行20次
    while(cnt--)
    {
        pool.addTask(ptask.get());//智能指针向裸指针转换
        cout << "cnt = " << cnt << endl;
    }

    pool.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

