#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/epoll.h>

#define SERV_PORT 8888
#define OPEN_MAX 5000

int main()
{
    int listenfd, connfd, sockfd, epfd;
    struct sockaddr_in serv_addr, clie_addr;
    socklen_t clie_addr_len;
    int ret, i, j, nready, nByte;
    char buf[BUFSIZ], str[BUFSIZ];
    struct epoll_event evt, ep[OPEN_MAX];
    //ep就是当前要监听的口的数组，ep数组的每个元素就是一个evt
    //evt可以看成是一个标签，标签上有 某个口和这个口要做的事
    //wait函数会自动更新ep

/*
typedef union epoll_data
{
    void *ptr;
    int fd;
    uint32_t u32;
    uint64_t u64;
} epoll_data_t;

struct epoll_event
{
    uint32_t events; // Epoll events
    epoll_data_t data; // User data variable 
};

events:可以是EPOLLIN(读)/EPOLLOUT(写)/EPOLLERR(异常)
*/


    //1、创建套接字
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if(-1 == listenfd)
    {
        perror("socket error");
        exit(-1);
    }

    //2、地址复用
    int opt = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    //3、端口复用
    int opt2 = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEPORT, &opt2, sizeof(opt2));

    //4、绑定ip与端口号
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERV_PORT);
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    ret = bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if(-1 == ret)
    {
        perror("bind error");
        close(listenfd);
        exit(-1);
    }

    //5、服务器监听
    ret = listen(listenfd, 128);
    if(-1 == ret)
    {
        perror("listen error");
        close(listenfd);
        exit(-1);
    }
    printf("server is listening...\n");

    //6、epoll类型IO多路复用
    //创建红黑树（使用数据结构：红黑树 + 就绪链表）
    epfd = epoll_create(OPEN_MAX);
    if(-1 == epfd)
    {
        perror("epoll_create error");
        close(listenfd);
        exit(-1);
    }
/*
    epfd是epoll实例的文件描述符，用于管理和监听多个文件描述符的状态变化。
    epfd并不是一个红黑树，而是一个由操作系统维护的数据结构，用于高效地管理大量的文件描述符。
    实际上，epoll内部可能使用了红黑树和就绪链表等数据结构来实现高效的IO多路复用。
    
    在代码中，调用epoll_create函数创建了一个epoll实例，并将其返回的文件描述符存储在epfd中。
    然后，调用epoll_ctl函数将要监听的文件描述符listenfd添加到epoll实例中进行监听。
    还通过设置evt.events和evt.data.fd来指定要监听的事件类型和相关的文件描述符。
    因此，epfd并不是直接负责监听的红黑树，而是用于操作和管理整个epoll实例的文件描述符。在内部，epoll可能使用了红黑树等数据结构来实现高效的IO多路复用。
*/

    //对应结构体evt赋值，目的是放在红黑树上进行监听
    evt.events = EPOLLIN;
    evt.data.fd = listenfd;
    /* evt.data.ptr = ; */

    //将文件描述符listenfd放在红黑树上进行监听
    //evt 是一个 struct epoll_event 结构体，
    //其中 events 成员指定要监听的事件类型（比如 EPOLLIN 表示可读事件），
    //data 成员是一个联合体，可以是 fd ，表示文件描述符
    //相当于给红黑树的这个节点贴上一张便条，event就是要做什么事（监听什么类型，读还是写）
    ret = epoll_ctl(epfd, EPOLL_CTL_ADD, listenfd, &evt);
    if(-1 == ret)
    {
        perror("epoll_ctl error");
        close(listenfd);
        exit(-1);
    }

    while(1)
    {
        //6.1、使用epoll_wait负责监听，如果返回值大于0，表明有满足条件的连接被监听到
        
        //每次调用 epoll_wait 后，ep 数组都会根据当前就绪的事件进行更新。
        //当有事件就绪时，epoll_wait 函数会填充 ep 数组，其中每个元素对应一个就绪的文件描述符和该文件描述符对应的事件信息。
        //程序可以通过遍历 ep 数组来逐个处理每个就绪的文件描述符。
        //这种机制使得程序能够高效地处理多个并发事件，而不需要像传统的轮询方式那样消耗大量的系统资源。
        //
        //ep数组和epoll_ctl函数之间的联系在于，
        //通过epoll_ctl函数添加文件描述符到epfd后，
        //epoll_wait函数会等待这些文件描述符上的事件就绪，并将相关信息存储到ep数组中，
        //程序可以通过遍历ep数组来处理这些就绪事件。
        nready = epoll_wait(epfd, ep, OPEN_MAX, -1);
        if(nready < 0)
        {
            perror("epoll_wait error");
            close(listenfd);
            exit(-1);
        }

        //7、遍历这些nready
        for(i = 0; i < nready; ++i)
        {
            //7.1、异常处理
            if(!(ep[i].events & EPOLLIN))
            {
                continue;
            }

            //7.2、如果有监听到的是listenfd，表示有新的请求进来
            if(ep[i].data.fd == listenfd)//如果是连接事件
            {
                clie_addr_len = sizeof(clie_addr);
                //8、有新的连接，那么accept肯定会有返回值
                connfd = accept(listenfd, (struct sockaddr *)&clie_addr, &clie_addr_len);
                if(-1 == connfd)
                {
                    perror("accept error");
                    close(listenfd);
                    exit(-1);
                }
                printf("receive from %s from port %d\n", 
                       inet_ntop(AF_INET, &clie_addr.sin_addr, str, sizeof(str)),
                       ntohs(clie_addr.sin_port));

                //将connfd放在红黑树上继续监听（类似于select中，放在client数组中进行继续监听一样）
                evt.events = EPOLLIN;
                evt.data.fd = connfd;
                epoll_ctl(epfd, EPOLL_CTL_ADD, connfd, &evt);
            }
            //7.3、不是连接建立事件，而是读写事件(信息传递事件)
            //那就表明是老的连接上有数据可读，
            //那肯定是客户端有数据发送过来，所以就可以进行接收,执行read/write
            else 
            {
                sockfd = ep[i].data.fd;
                nByte = read(sockfd, buf, sizeof(buf));
                //表明数据读完了（对应的缓冲区中没有数据了）
                //也就是表明连接马上要断开了
                if(nByte == 0)
                {
                    ret = epoll_ctl(epfd, EPOLL_CTL_DEL, sockfd, NULL);
                    if(-1 == ret)
                    {
                        perror("epoll_ctl error");
                    }
                    close(sockfd);
                    printf("client[%d] closed connection\n", sockfd);
                }
                //连接异常
                else if(nByte < 0)
                {
                    perror("epoll_ctl error");
                    ret = epoll_ctl(epfd, EPOLL_CTL_DEL, sockfd, NULL);
                    if(-1 == ret)
                    {
                        perror("epoll_ctl error");
                    }
                    close(sockfd);

                }
                //正常的操作
                else 
                {
                    for(j = 0; j < nByte; ++j)
                    {
                        buf[j] = toupper(buf[j]);
                    }
                    write(sockfd, buf, nByte);
                    write(STDOUT_FILENO, buf, nByte);
                }
            }
        }
    }

    close(listenfd);
    close(connfd);

    return 0;
}
