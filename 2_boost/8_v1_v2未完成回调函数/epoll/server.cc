#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

/*
1、
    //将新连接添加到epoll的监听实例中去
    struct epoll_event ev;
    ev.events = EPOLLIN | EPOLLOUT | EPOLLERR;
    ev.data.fd = peerfd;
    ret = ::epoll_ctl(efd, EPOLL_CTL_ADD, peerfd, &ev);
    这句话是否会将当前ev加入evtlist？

    ::epoll_ctl(efd, EPOLL_CTL_ADD, peerfd, &ev);的作用是
    将peerfd描述符添加到epoll实例efd中，并设置对应的事件类型和事件数据。让efd开始监听peerfd上的事件。
    当peerfd上有对应的事件发生时，epoll_wait会检测到并将事件存储在evtList中，然后在事件循环中进行处理。
    因此，::epoll_ctl(efd, EPOLL_CTL_ADD, peerfd, &ev);只是将peerfd的相关信息添加到epoll实例中，而不是将ev加入evtList。
    
2、
    让epoll实例去监听ev，如果后续这个ev上有消息，才会在evtlist上显示它？

    并不是。
    当peerfd上的事件发生时，比如有数据可读或可写等，epoll_wait会检测到该事件，并将事件信息存储在传入的evtList数组中。
    evtList是用来存储epoll_wait返回的就绪事件列表的，其中包含了发生事件的描述符、事件类型等信息。
    所以说，evtList并不是存储ev的地方，而是存储epoll_wait检测到的就绪事件信息的地方。
    ev是用来注册事件的数据结构，而evtList是用来存储实际发生事件的描述符和事件类型的数组。
    当监听到ev时，它的fd、事件类型信息会传入evtlist，作为它的数组元素中的一部分，但不等同于就是ev。
*/


void test()
{
	//1. 创建监听服务器的套接字
	int listenfd = ::socket(AF_INET, SOCK_STREAM, 0);	
	if(listenfd < 0) 
    {
		perror("socket");
		return;
	}
    
    //地址复用
    int opt = 1;
    int ret = setsockopt(listenfd,  SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    if(-1 == ret)
    {
        perror("setsockopt ip error");
        close(listenfd);
        return;
    }

    //端口重用
    ret = setsockopt(listenfd, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt));
    if(-1 == ret)
    {
        perror("setsockopt port error");
        close(listenfd);
        return;
    }

	//网络地址需要采用网络字节序存储(大端模式)
	struct sockaddr_in serveraddr;
	memset(&serveraddr, 0, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(8888);
	serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	socklen_t length = sizeof(serveraddr);


	//2. 绑定服务器的网络地址
	if(::bind(listenfd, (struct sockaddr*)&serveraddr, length) < 0)
    {
		perror("bind");
		//文件描述符是比较稀缺的，所以不用的时候要回收
		close(listenfd);
		return;
	}

	//3. 让服务器开始监听
	// listenfd跟所有的新连接打交道
	if(::listen(listenfd, 128) < 0)
    {
		perror("listen");
		close(listenfd);
		return;
	} 
    cout << "server is listening..." << endl;

    //4、创建epoll实例
	int efd = ::epoll_create1(0);// 红黑树 + 就绪链表

	struct epoll_event ev;
	ev.events = EPOLLIN | EPOLLOUT;
	ev.data.fd = listenfd;
	//epoll要进行监听操作: 对listenfd的读事件进行监听
    //
	//Reactor: 注册读就绪事件
	ret = ::epoll_ctl(efd, EPOLL_CTL_ADD, listenfd, &ev);
	if(ret < 0) 
    {
		perror("epoll_ctl");
		close(listenfd);
		close(efd);
		return;
	}

	struct epoll_event *evtList = 
        (struct epoll_event *)malloc(1024 * sizeof(struct epoll_event));

	//事件循环
	while(1)
    {
		//Reactor: 事件分离器
		int nready = ::epoll_wait(efd, evtList, 1024, 3000);
		if(-1 == nready && errno == EINTR)  //有中断请求，先去处理
        {
			continue;
        }
		else if(-1 == nready) 
        {
			perror("epoll_wait");
			return;
		} 
        else if(0 == nready) //3000毫秒，即3秒钟没有响应，就打印超时
        {
			printf(">> epoll_wait timeout!\n");
		} 
        else
        {
			//遍历struct epoll_event数组 evtList，
            //check每一个epoll_event到底发生了什么事件
			for(int idx = 0; idx < nready; ++idx)
            {
				// 必须要使用按位&操作来判断事件，不能使用==,&&
				if((evtList[idx].data.fd == listenfd)  &&
				   (evtList[idx].events & EPOLLIN)) 
				{   
					//意味着有新连接来了,所以要调用accept函数,获取新连接
					//写事件什么情况会触发? 只要内核发送缓冲区还有空间，就可以触发写事件
					int peerfd = ::accept(listenfd, nullptr, nullptr);	
					/* TcpConnection conn(peerfd); */

					//将新连接添加到epoll的监听实例中去
					struct epoll_event ev;
					ev.events = EPOLLIN | EPOLLOUT | EPOLLERR;
					ev.data.fd = peerfd;
					ret = ::epoll_ctl(efd, EPOLL_CTL_ADD, peerfd, &ev);
					if(ret < 0) 
                    {
						perror("epoll_ctl");
						continue;
					}

					//新连接到来之后的处理
					printf(">> conn has connected, fd: %d\n", peerfd);
					//记录日志, 使用Log4cpp完成
					//个性定制化 ==> 事件处理器
					/* onConnection();//考虑扩展性，挖一个坑 */
				} 
                else
                {
					// 已经建立好的连接发送数据过来了
					// 如果发生了读事件
					char buff[128] = {0};
					if(evtList[idx].events & EPOLLIN)
                    {
						int fd = evtList[idx].data.fd;
						ret = ::recv(fd, buff, sizeof(buff), 0);

						if(ret > 0) 
                        {
							printf(">>> recv msg %d bytes,content:%s\n",
									ret, buff);
							
							//1. 对应用层数据进行解析
							//2. 拿到最终要处理的数据之后，进行业务逻辑处理
							//(假设第2步执行的时间很长1S, 是否合适)
							//3. 得到要返回给客户端的数据之后，进行发送操作
							//

							ret = send(fd, buff, strlen(buff), 0);
                            //这里是strlen，表示buff里面的数据有多长
                            //而recv函数是sizeof(buff)，默认读取缓冲区大小的字节
							printf(">>> send %d bytes\n", ret);
							/* onMessage();//, 考虑扩展性，挖一个坑 */
						}
                        else if(ret == 0)
                        {
							printf("conn has closed!\n");
							
							//需要从epoll的监听实例删除掉, 因为连接断开，不需要再监听了
							struct epoll_event ev;
							ev.data.fd = fd;
							ret = ::epoll_ctl(efd, EPOLL_CTL_DEL, fd, &ev);
							if(ret < 0) 
                            {
								perror("epoll_ctl");
							}
							//记录日志, log4cpp
							/* onClose();//考虑扩展性，挖一个坑 */
						}// end of ret if
					}//end of event if
					//else if()  //处理其他事件，比如写事件
				}
			}
		}
    }
	
	close(listenfd);// 关闭连接
}
 
int main(int argc, char *argv[])
{
	test();
	return 0;
}
