#ifndef __EVENTLOOP_H__
#define __EVENTLOOP_H__

#include <sys/epoll.h>
#include <vector>
#include <functional>
#include <map>
#include <memory>

using std::vector;
using std::map;
using std::shared_ptr;
using std::function;

class Acceptor;//前向声明
class TcpConnection;//前向声明

class EventLoop
{
    using TcpConnectionPtr = shared_ptr<TcpConnection>;
    using TcpConnectionCallback = function<void(const TcpConnectionPtr &con)>;
public:
    EventLoop(Acceptor &acceptor);
    ~EventLoop();

    //事件循环与否对应的两个函数
    void loop();
    void unloop();
private:
    //该函数里面封装类epoll_wait函数
    void waitEpollFd();

    //处理新的连接
    void handleNewConnection();

    //处理老的连接上的数据收发
    void handleMessage(int fd);

    //红黑树根结点文件描述符的创建
    int createEpollFd();

    //将文件描述符放在红黑树上进行监听
    void addEpollReadFd(int fd);

    //将文件描述符从红黑树上删除
    void delEpollReadFd(int fd);

private:
    int _epfd;//epoll_create创建的红黑树关联结点
    bool _isLooping;//标识循环是否在运行的标志
    vector<struct epoll_event> _evtList;//存放满足条件文件描述符的结构体
    Acceptor &_acceptor;//需要调用该类的成员函数accept
    map<int, TcpConnectionPtr> _conns;//存放文件描述符与TcpConnection的键值对

    TcpConnectionCallback _onConnection;//连接建立
    TcpConnectionCallback _onMesage;//文件描述符可读，也就是数据的收发
    TcpConnectionCallback _onClose;//连接断开
};

#endif
