#include "EventFd.h"
#include <sys/eventfd.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <iostream>

using std::cout;
using std::endl;

EventFd::EventFd(EventFdCallback &&cb)
: _evfd(createEventFd())
, _cb(std::move(cb))//回调函数的注册
, _isStarted(false)
{

}

EventFd::~EventFd()
{
    close(_evfd);
}

//启动与停止
void EventFd::start()
{
    struct pollfd pfd;
    pfd.fd = _evfd;//监听的文件描述符
    pfd.events = POLLIN;//监听的读事件

    _isStarted = true;

    while(_isStarted)
    {
        //使用poll进行监听
        int nready = poll(&pfd, 1, 3000);
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            perror("poll error");
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll time_out" << endl;
        }
        else
        {
            //查看一下是不是读事件
            if(pfd.revents & POLLIN)
            {
                //阻塞等待被唤醒
                handleRead();   //清空_evfd，表明我已经收到通知
                if(_cb)
                {
                    _cb();//回调函数的执行
                }
            }
        }
    }
}

void EventFd::stop()
{
    _isStarted = false;
}

//唤醒(执行write)
void EventFd::wakeup()
{
    uint64_t one = 1;
    ssize_t ret = write(_evfd, &one, sizeof(uint64_t));
    if(ret != sizeof(uint64_t))
    {
        perror("wakeup");
        return;
    }
}

//创建用于通信的文件描述符
int EventFd::createEventFd()
{
    int fd = eventfd(10, 0);
    if(fd < 0)
    {
        perror("eventfd");
        return -1;
    }
    return fd;
}

//阻塞等待（也就是执行read）
void EventFd::handleRead()
{
    uint64_t two = 0;
    ssize_t ret = read(_evfd, &two, sizeof(uint64_t));
    if(ret != sizeof(uint64_t))
    {
        perror("read");
        return;
    }
}
