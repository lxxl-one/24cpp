#include "EventFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;
using std::bind;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask process is running!!!" << endl;
    }
};

void test()
{
    MyTask task;
    EventFd efd(bind(&MyTask::process, &task));
    Thread th(bind(&EventFd::start, &efd));//就让handleRead放在了子线程中
    th.start();//将子线程启动起来,也就是会执行EventFd中的start函数

    size_t cnt = 20;
    while(cnt--)
    {
        efd.wakeup();//主线程中执行，表示write在主线程中执行
        cout << "cnt = " << cnt << endl;
        sleep(1);
    }

    efd.stop();
    th.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

